<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the fist needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientbase class each generated ServiceType class extends this class
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = array(
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'main.wsdl',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * );
 * etc....
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'main.wsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => ClassMap::get(),
);
/**
 * Samples for SSKCHECKCONTACTEXISTS ServiceType
 */
$sSKCHECKCONTACTEXISTS = new \ServiceType\SSKCHECKCONTACTEXISTS($options);
/**
 * Sample call for SSK_CHECK_CONTACT_EXISTS operation/method
 */
if ($sSKCHECKCONTACTEXISTS->SSK_CHECK_CONTACT_EXISTS(new \StructType\TDoubleCheck()) !== false) {
    print_r($sSKCHECKCONTACTEXISTS->getResult());
} else {
    print_r($sSKCHECKCONTACTEXISTS->getLastError());
}
/**
 * Samples for SSKDELETECONTACTCOMSTAT ServiceType
 */
$sSKDELETECONTACTCOMSTAT = new \ServiceType\SSKDELETECONTACTCOMSTAT($options);
/**
 * Sample call for SSK_DELETE_CONTACT_COMSTAT operation/method
 */
if ($sSKDELETECONTACTCOMSTAT->SSK_DELETE_CONTACT_COMSTAT($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_COMSTAT_ID) !== false) {
    print_r($sSKDELETECONTACTCOMSTAT->getResult());
} else {
    print_r($sSKDELETECONTACTCOMSTAT->getLastError());
}
/**
 * Samples for SSKDELETECONXATTR ServiceType
 */
$sSKDELETECONXATTR = new \ServiceType\SSKDELETECONXATTR($options);
/**
 * Sample call for SSK_DELETE_CONXATTR operation/method
 */
if ($sSKDELETECONXATTR->SSK_DELETE_CONXATTR($Check_Only, new \StructType\TConxAttr()) !== false) {
    print_r($sSKDELETECONXATTR->getResult());
} else {
    print_r($sSKDELETECONXATTR->getLastError());
}
/**
 * Samples for SSKDELETEEVENT ServiceType
 */
$sSKDELETEEVENT = new \ServiceType\SSKDELETEEVENT($options);
/**
 * Sample call for SSK_DELETE_EVENT operation/method
 */
if ($sSKDELETEEVENT->SSK_DELETE_EVENT($IN_CHECK_ONLY, $EVCONXROLE_EVENT_ID, $EVCONXROLE_CONTACT_ID) !== false) {
    print_r($sSKDELETEEVENT->getResult());
} else {
    print_r($sSKDELETEEVENT->getLastError());
}
/**
 * Samples for SSKDELETENEWADDRESS ServiceType
 */
$sSKDELETENEWADDRESS = new \ServiceType\SSKDELETENEWADDRESS($options);
/**
 * Sample call for SSK_DELETE_NEWADDRESS operation/method
 */
if ($sSKDELETENEWADDRESS->SSK_DELETE_NEWADDRESS($NEWADDRESS_ID) !== false) {
    print_r($sSKDELETENEWADDRESS->getResult());
} else {
    print_r($sSKDELETENEWADDRESS->getLastError());
}
/**
 * Samples for SSKDELETEPERIODICAL ServiceType
 */
$sSKDELETEPERIODICAL = new \ServiceType\SSKDELETEPERIODICAL($options);
/**
 * Sample call for SSK_DELETE_PERIODICAL operation/method
 */
if ($sSKDELETEPERIODICAL->SSK_DELETE_PERIODICAL($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_PERIODICAL_ID) !== false) {
    print_r($sSKDELETEPERIODICAL->getResult());
} else {
    print_r($sSKDELETEPERIODICAL->getLastError());
}
/**
 * Samples for SSKGETARTICLES ServiceType
 */
$sSKGETARTICLES = new \ServiceType\SSKGETARTICLES($options);
/**
 * Sample call for SSK_GET_ARTICLES operation/method
 */
if ($sSKGETARTICLES->SSK_GET_ARTICLES($ARTGROUP_ID) !== false) {
    print_r($sSKGETARTICLES->getResult());
} else {
    print_r($sSKGETARTICLES->getLastError());
}
/**
 * Samples for SSKGETCOLAREA ServiceType
 */
$sSKGETCOLAREA = new \ServiceType\SSKGETCOLAREA($options);
/**
 * Sample call for SSK_GET_COLAREA operation/method
 */
if ($sSKGETCOLAREA->SSK_GET_COLAREA($COLAREA_ID) !== false) {
    print_r($sSKGETCOLAREA->getResult());
} else {
    print_r($sSKGETCOLAREA->getLastError());
}
/**
 * Samples for SSKGETCOLAREASPROM ServiceType
 */
$sSKGETCOLAREASPROM = new \ServiceType\SSKGETCOLAREASPROM($options);
/**
 * Sample call for SSK_GET_COLAREAS_PROM operation/method
 */
if ($sSKGETCOLAREASPROM->SSK_GET_COLAREAS_PROM($CONTACT_ID) !== false) {
    print_r($sSKGETCOLAREASPROM->getResult());
} else {
    print_r($sSKGETCOLAREASPROM->getLastError());
}
/**
 * Samples for SSKGETCONTACTID ServiceType
 */
$sSKGETCONTACTID = new \ServiceType\SSKGETCONTACTID($options);
/**
 * Sample call for SSK_GET_CONTACT_ID operation/method
 */
if ($sSKGETCONTACTID->SSK_GET_CONTACT_ID($IN_TITLE_ID, $IN_INITIALS, $IN_FIRST_NAME, $IN_SURNAME_1, $IN_STREET, $IN_HOUSE_NO, $IN_HOUSE_NO_ADD, $IN_COUNTRY_ID, $IN_POSTCODE, $IN_CITY, $IN_PAYACCOUNT_ID, $IN_BIRTH_DATE, $IN_EMAIL, $IN_TELEPHONE, $IN_USERNAME, $IN_PASSWORD, $IN_FREE_X100_1, $IN_FREE_X100_2, $IN_FREE_X100_3, $IN_FREE_X100_4, $IN_FREE_X100_5) !== false) {
    print_r($sSKGETCONTACTID->getResult());
} else {
    print_r($sSKGETCONTACTID->getLastError());
}
/**
 * Samples for SSKGETCONXATTRS ServiceType
 */
$sSKGETCONXATTRS = new \ServiceType\SSKGETCONXATTRS($options);
/**
 * Sample call for SSK_GET_CONXATTRS operation/method
 */
if ($sSKGETCONXATTRS->SSK_GET_CONXATTRS($CONTACT_ID) !== false) {
    print_r($sSKGETCONXATTRS->getResult());
} else {
    print_r($sSKGETCONXATTRS->getLastError());
}
/**
 * Samples for SSKGETCONXCOLAREAS ServiceType
 */
$sSKGETCONXCOLAREAS = new \ServiceType\SSKGETCONXCOLAREAS($options);
/**
 * Sample call for SSK_GET_CONXCOLAREAS operation/method
 */
if ($sSKGETCONXCOLAREAS->SSK_GET_CONXCOLAREAS($COLAREA_ID) !== false) {
    print_r($sSKGETCONXCOLAREAS->getResult());
} else {
    print_r($sSKGETCONXCOLAREAS->getLastError());
}
/**
 * Samples for SSKGETEVENTS ServiceType
 */
$sSKGETEVENTS = new \ServiceType\SSKGETEVENTS($options);
/**
 * Sample call for SSK_GET_EVENTS operation/method
 */
if ($sSKGETEVENTS->SSK_GET_EVENTS($IN_CONTACT_ID) !== false) {
    print_r($sSKGETEVENTS->getResult());
} else {
    print_r($sSKGETEVENTS->getLastError());
}
/**
 * Samples for SSKGETFULL ServiceType
 */
$sSKGETFULL = new \ServiceType\SSKGETFULL($options);
/**
 * Sample call for SSK_GET_FULL operation/method
 */
if ($sSKGETFULL->SSK_GET_FULL($IN_CONTACT_ID) !== false) {
    print_r($sSKGETFULL->getResult());
} else {
    print_r($sSKGETFULL->getLastError());
}
/**
 * Samples for SSKGETNEWADDRESSES ServiceType
 */
$sSKGETNEWADDRESSES = new \ServiceType\SSKGETNEWADDRESSES($options);
/**
 * Sample call for SSK_GET_NEWADDRESSES operation/method
 */
if ($sSKGETNEWADDRESSES->SSK_GET_NEWADDRESSES($IN_CONTACT_ID) !== false) {
    print_r($sSKGETNEWADDRESSES->getResult());
} else {
    print_r($sSKGETNEWADDRESSES->getLastError());
}
/**
 * Samples for SSKGETNEXTORDERNO ServiceType
 */
$sSKGETNEXTORDERNO = new \ServiceType\SSKGETNEXTORDERNO($options);
/**
 * Sample call for SSK_GET_NEXT_ORDERNO operation/method
 */
if ($sSKGETNEXTORDERNO->SSK_GET_NEXT_ORDERNO() !== false) {
    print_r($sSKGETNEXTORDERNO->getResult());
} else {
    print_r($sSKGETNEXTORDERNO->getLastError());
}
/**
 * Samples for SSKGETPERIODICALS ServiceType
 */
$sSKGETPERIODICALS = new \ServiceType\SSKGETPERIODICALS($options);
/**
 * Sample call for SSK_GET_PERIODICALS operation/method
 */
if ($sSKGETPERIODICALS->SSK_GET_PERIODICALS($IN_CONTACT_ID) !== false) {
    print_r($sSKGETPERIODICALS->getResult());
} else {
    print_r($sSKGETPERIODICALS->getLastError());
}
/**
 * Samples for SSKGETPPSUBS ServiceType
 */
$sSKGETPPSUBS = new \ServiceType\SSKGETPPSUBS($options);
/**
 * Sample call for SSK_GET_PPSUBS operation/method
 */
if ($sSKGETPPSUBS->SSK_GET_PPSUBS($IN_CONTACT_ID) !== false) {
    print_r($sSKGETPPSUBS->getResult());
} else {
    print_r($sSKGETPPSUBS->getLastError());
}
/**
 * Samples for SSKGETSUBSCRIPTIONS ServiceType
 */
$sSKGETSUBSCRIPTIONS = new \ServiceType\SSKGETSUBSCRIPTIONS($options);
/**
 * Sample call for SSK_GET_SUBSCRIPTIONS operation/method
 */
if ($sSKGETSUBSCRIPTIONS->SSK_GET_SUBSCRIPTIONS($IN_CONTACT_ID) !== false) {
    print_r($sSKGETSUBSCRIPTIONS->getResult());
} else {
    print_r($sSKGETSUBSCRIPTIONS->getLastError());
}
/**
 * Samples for SSKINSERTCONTACT ServiceType
 */
$sSKINSERTCONTACT = new \ServiceType\SSKINSERTCONTACT($options);
/**
 * Sample call for SSK_INSERT_CONTACT operation/method
 */
if ($sSKINSERTCONTACT->SSK_INSERT_CONTACT($IN_CHECK_ONLY, $IN_RELATION_SOURCE_ID, $IN_PERSON_TITLE_ID, $IN_PERSON_INITIALS, $IN_PERSON_FIRST_NAME, $IN_PERSON_MIDDLE_NAME, $IN_PERSON_PREFIX, $IN_PERSON_SURNAME_1, $IN_PERSON_TELEPHONE, $IN_PERSON_TELEPHONE_GSM, $IN_PERSON_EMAIL, $IN_PERSON_BIRTH_DATE, $IN_ADDRESS_POSTCODE, $IN_ADDRESS_STREET, $IN_ADDRESS_HOUSE_NO, $IN_ADDRESS_HOUSE_NO_ADD, $IN_ADDRESS_COUNTRY_ID, $IN_ADDRESS_CITY, $IN_RELATION_PAYACCOUNT_ID, $IN_PERSON_USERNAME, $IN_PERSON_PASSWORD, $IN_RELATION_NAME_1, $IN_RELATION_NAME_2, $IN_RELATION_UDPROFILE_ID, $IN_RELATION_SUBPROFILE_ID, $IN_CONTACT_TELEPHONE, $IN_CONTACT_EMAIL, $IN_CONTACT_ENTRY_SALESMAN_ID, $IN_FREE_X100_1, $IN_FREE_X100_2, $IN_FREE_X100_3, $IN_FREE_X100_4, $IN_FREE_X100_5, $IN_FREE_X100_6, $IN_FREE_X100_7, $IN_FREE_X100_8, $IN_FREE_X100_9, $IN_FREE_X1000_10) !== false) {
    print_r($sSKINSERTCONTACT->getResult());
} else {
    print_r($sSKINSERTCONTACT->getLastError());
}
/**
 * Samples for SSKINSERTCONTACTCOMSTAT ServiceType
 */
$sSKINSERTCONTACTCOMSTAT = new \ServiceType\SSKINSERTCONTACTCOMSTAT($options);
/**
 * Sample call for SSK_INSERT_CONTACT_COMSTAT operation/method
 */
if ($sSKINSERTCONTACTCOMSTAT->SSK_INSERT_CONTACT_COMSTAT($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_COMSTAT_ID) !== false) {
    print_r($sSKINSERTCONTACTCOMSTAT->getResult());
} else {
    print_r($sSKINSERTCONTACTCOMSTAT->getLastError());
}
/**
 * Samples for SSKINSERTCONXATTR ServiceType
 */
$sSKINSERTCONXATTR = new \ServiceType\SSKINSERTCONXATTR($options);
/**
 * Sample call for SSK_INSERT_CONXATTR operation/method
 */
if ($sSKINSERTCONXATTR->SSK_INSERT_CONXATTR($Check_Only, new \StructType\TConxAttr()) !== false) {
    print_r($sSKINSERTCONXATTR->getResult());
} else {
    print_r($sSKINSERTCONXATTR->getLastError());
}
/**
 * Samples for SSKINSERTCONXCOLAREA ServiceType
 */
$sSKINSERTCONXCOLAREA = new \ServiceType\SSKINSERTCONXCOLAREA($options);
/**
 * Sample call for SSK_INSERT_CONXCOLAREA operation/method
 */
if ($sSKINSERTCONXCOLAREA->SSK_INSERT_CONXCOLAREA($IN_CHECK_ONLY, new \StructType\TConXColArea()) !== false) {
    print_r($sSKINSERTCONXCOLAREA->getResult());
} else {
    print_r($sSKINSERTCONXCOLAREA->getLastError());
}
/**
 * Samples for SSKINSERTEVENT ServiceType
 */
$sSKINSERTEVENT = new \ServiceType\SSKINSERTEVENT($options);
/**
 * Sample call for SSK_INSERT_EVENT operation/method
 */
if ($sSKINSERTEVENT->SSK_INSERT_EVENT($IN_CHECK_ONLY, $EVCONXROLE_SOURCE_ID, $EVCONXROLE_EVENT_ID, $EVCONXROLE_PAYACCOUNT_ID, $EVCONXROLE_CONTACT_ID, $EVCONXROLE_EVROLE_ID, $EVCONXROLE_EVSTAT_ID, $EVCONXROLE_EVSTAT_DATE, $EVCONXROLE_MEMO) !== false) {
    print_r($sSKINSERTEVENT->getResult());
} else {
    print_r($sSKINSERTEVENT->getLastError());
}
/**
 * Samples for SSKINSERTNEWADDRESS ServiceType
 */
$sSKINSERTNEWADDRESS = new \ServiceType\SSKINSERTNEWADDRESS($options);
/**
 * Sample call for SSK_INSERT_NEWADDRESS operation/method
 */
if ($sSKINSERTNEWADDRESS->SSK_INSERT_NEWADDRESS($NEWADDRESS_CONTACT_ID, $NEWADDRESS_START_DATE, $NEWADDRESS_NEWADDRESSTYPE_ID, $NEWADDRESS_COUNTRY_ID, $NEWADDRESS_STREET, $NEWADDRESS_HOUSE_NO, $NEWADDRESS_HOUSE_NO_ADD, $NEWADDRESS_POSTCODE, $NEWADDRESS_CITY) !== false) {
    print_r($sSKINSERTNEWADDRESS->getResult());
} else {
    print_r($sSKINSERTNEWADDRESS->getLastError());
}
/**
 * Samples for SSKINSERTORHEADER ServiceType
 */
$sSKINSERTORHEADER = new \ServiceType\SSKINSERTORHEADER($options);
/**
 * Sample call for SSK_INSERT_ORHEADER operation/method
 */
if ($sSKINSERTORHEADER->SSK_INSERT_ORHEADER($IN_CHECK_ONLY, $ORHEADER_ID, $ORHEADER_CONTACT_ID, $ORHEADER_ENTRY_SALESMAN_ID, $ORHEADER_DELIVERY_CONTACT_ID, $ORHEADER_SHIPCODE_ID, $ORHEADER_PREPAYM_REQ_FLAG, $ORHEADER_MIN_DELIVERY_DATE, $ORHEADER_MAX_DELIVERY_DATE, $ORHEADER_MAX_BO_DELAY, $ORHEADER_MEMO, $ORHEADER_VAT_INCL_FLAG, $ORHEADER_SOURCE_ID, $ORHEADER_PAYACCOUNT_ID, $ORHEADER_AUTOPAYCODE_ID, $ORHEADER_FIXEDPAYCODE_ID, $ORHEADER_COLAREA_ID, $ORHEADER_ORDERKIND_ID, $PAYMENT_AMOUNT, $PAYMENT_PAYCODE_ID, $IN_FREE_X100_1, $IN_FREE_X100_2, $IN_FREE_X100_3, $IN_FREE_X100_4, $IN_FREE_X100_5) !== false) {
    print_r($sSKINSERTORHEADER->getResult());
} else {
    print_r($sSKINSERTORHEADER->getLastError());
}
/**
 * Samples for SSKINSERTORLINE ServiceType
 */
$sSKINSERTORLINE = new \ServiceType\SSKINSERTORLINE($options);
/**
 * Sample call for SSK_INSERT_ORLINE operation/method
 */
if ($sSKINSERTORLINE->SSK_INSERT_ORLINE($IN_CHECK_ONLY, $ORHEADER_ID, $ORLINE_ARTICLE_ID, $ORLINE_ARTICLE_DESCR, $ORLINE_QTY, $ORLINE_PERC_DISCOUNT, $ORLINE_ARTICLE_PRICE, $ORLINE_VAT_ID, $ORLINE_MEMO, $X100_1, $X100_2, $X100_3, $X100_4, $X100_5) !== false) {
    print_r($sSKINSERTORLINE->getResult());
} else {
    print_r($sSKINSERTORLINE->getLastError());
}
/**
 * Samples for SSKINSERTPERIODICAL ServiceType
 */
$sSKINSERTPERIODICAL = new \ServiceType\SSKINSERTPERIODICAL($options);
/**
 * Sample call for SSK_INSERT_PERIODICAL operation/method
 */
if ($sSKINSERTPERIODICAL->SSK_INSERT_PERIODICAL($IN_CONTACT_ID, $IN_PERIODICAL_ID, $IN_START_DATE, $IN_END_DATE) !== false) {
    print_r($sSKINSERTPERIODICAL->getResult());
} else {
    print_r($sSKINSERTPERIODICAL->getLastError());
}
/**
 * Samples for SSKINSERTPERSONATRELATION ServiceType
 */
$sSKINSERTPERSONATRELATION = new \ServiceType\SSKINSERTPERSONATRELATION($options);
/**
 * Sample call for SSK_INSERT_PERSON_AT_RELATION operation/method
 */
if ($sSKINSERTPERSONATRELATION->SSK_INSERT_PERSON_AT_RELATION($Check_Only, new \StructType\TPersonContact()) !== false) {
    print_r($sSKINSERTPERSONATRELATION->getResult());
} else {
    print_r($sSKINSERTPERSONATRELATION->getLastError());
}
/**
 * Samples for SSKINSERTPPSUB ServiceType
 */
$sSKINSERTPPSUB = new \ServiceType\SSKINSERTPPSUB($options);
/**
 * Sample call for SSK_INSERT_PPSUB operation/method
 */
if ($sSKINSERTPPSUB->SSK_INSERT_PPSUB($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_SOURCE_ID, $IN_TERM_ID, $IN_AMOUNT, $IN_START_DATE, $IN_END_DATE, $IN_PAYACCOUNT_ID, $IN_PROJECT_ID, $IN_SALESMAN_ID) !== false) {
    print_r($sSKINSERTPPSUB->getResult());
} else {
    print_r($sSKINSERTPPSUB->getLastError());
}
/**
 * Samples for SSKINSERTSSLINE ServiceType
 */
$sSKINSERTSSLINE = new \ServiceType\SSKINSERTSSLINE($options);
/**
 * Sample call for SSK_INSERT_SSLINE operation/method
 */
if ($sSKINSERTSSLINE->SSK_INSERT_SSLINE($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_SSLINE_SSLETTER_ID, $IN_SSLINE_SOURCE_ID, $SSLINE_SSSUBJECT_ID, $SSLINE_SSSUBSUBJECT_ID, $SSLINE_MEMO) !== false) {
    print_r($sSKINSERTSSLINE->getResult());
} else {
    print_r($sSKINSERTSSLINE->getLastError());
}
/**
 * Samples for SSKINSERTSUBSCRIPTION ServiceType
 */
$sSKINSERTSUBSCRIPTION = new \ServiceType\SSKINSERTSUBSCRIPTION($options);
/**
 * Sample call for SSK_INSERT_SUBSCRIPTION operation/method
 */
if ($sSKINSERTSUBSCRIPTION->SSK_INSERT_SUBSCRIPTION($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_SUBSCRIPTION_SOURCE_ID, $IN_SUBSCRIPTION_PR_ARTICLE_ID, $IN_SUBCODE_ID, $IN_PAYACCOUNT_ID, $IN_MGM_CONTACT_ID, $IN_ADD_COSTS) !== false) {
    print_r($sSKINSERTSUBSCRIPTION->getResult());
} else {
    print_r($sSKINSERTSUBSCRIPTION->getLastError());
}
/**
 * Samples for SSKORHEADERCOMPLETE ServiceType
 */
$sSKORHEADERCOMPLETE = new \ServiceType\SSKORHEADERCOMPLETE($options);
/**
 * Sample call for SSK_ORHEADER_COMPLETE operation/method
 */
if ($sSKORHEADERCOMPLETE->SSK_ORHEADER_COMPLETE($ORHEADER_ID) !== false) {
    print_r($sSKORHEADERCOMPLETE->getResult());
} else {
    print_r($sSKORHEADERCOMPLETE->getLastError());
}
/**
 * Samples for SSKPPSUBEND ServiceType
 */
$sSKPPSUBEND = new \ServiceType\SSKPPSUBEND($options);
/**
 * Sample call for SSK_PPSUB_END operation/method
 */
if ($sSKPPSUBEND->SSK_PPSUB_END($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_PPSUB_PPTERM_ID, $IN_PPSUB_PPENDREASON_ID, $IN_PPSUB_END_DATE) !== false) {
    print_r($sSKPPSUBEND->getResult());
} else {
    print_r($sSKPPSUBEND->getLastError());
}
/**
 * Samples for SSKSEARCHCONTACT ServiceType
 */
$sSKSEARCHCONTACT = new \ServiceType\SSKSEARCHCONTACT($options);
/**
 * Sample call for SSK_SEARCH_CONTACT operation/method
 */
if ($sSKSEARCHCONTACT->SSK_SEARCH_CONTACT(new \StructType\TRelationSearch()) !== false) {
    print_r($sSKSEARCHCONTACT->getResult());
} else {
    print_r($sSKSEARCHCONTACT->getLastError());
}
/**
 * Samples for SSKUPDATECOLAREA ServiceType
 */
$sSKUPDATECOLAREA = new \ServiceType\SSKUPDATECOLAREA($options);
/**
 * Sample call for SSK_UPDATE_COLAREA operation/method
 */
if ($sSKUPDATECOLAREA->SSK_UPDATE_COLAREA($IN_CHECK_ONLY, new \StructType\TColArea()) !== false) {
    print_r($sSKUPDATECOLAREA->getResult());
} else {
    print_r($sSKUPDATECOLAREA->getLastError());
}
/**
 * Samples for SSKUPDATECONTACT ServiceType
 */
$sSKUPDATECONTACT = new \ServiceType\SSKUPDATECONTACT($options);
/**
 * Sample call for SSK_UPDATE_CONTACT operation/method
 */
if ($sSKUPDATECONTACT->SSK_UPDATE_CONTACT($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_RELATION_SOURCE_ID, $IN_PERSON_TITLE_ID, $IN_PERSON_INITIALS, $IN_PERSON_PREFIX, $IN_PERSON_FIRST_NAME, $IN_PERSON_MIDDLE_NAME, $IN_PERSON_SURNAME_1, $IN_PERSON_TELEPHONE, $IN_PERSON_TELEPHONE_GSM, $IN_PERSON_EMAIL, $IN_PERSON_BIRTH_DATE, $IN_ADDRESS_POSTCODE, $IN_ADDRESS_STREET, $IN_ADDRESS_HOUSE_NO, $IN_ADDRESS_HOUSE_NO_ADD, $IN_ADDRESS_COUNTRY_ID, $IN_ADDRESS_CITY, $IN_RELATION_PAYACCOUNT_ID, $IN_CONTACT_USERNAME, $IN_CONTACT_PASSWORD, $IN_CONTACT_TELEPHONE, $IN_CONTACT_EMAIL, $IN_RELATION_NAME_1, $IN_RELATION_NAME_2, $IN_RELATION_UDPROFILE_ID, $IN_RELATION_SUBPROFILE_ID, $IN_FREE_X100_1, $IN_FREE_X100_2, $IN_FREE_X100_3, $IN_FREE_X100_4, $IN_FREE_X100_5, $IN_FREE_X100_6, $IN_FREE_X100_7, $IN_FREE_X100_8, $IN_FREE_X100_9, $IN_FREE_X1000_10) !== false) {
    print_r($sSKUPDATECONTACT->getResult());
} else {
    print_r($sSKUPDATECONTACT->getLastError());
}
/**
 * Samples for SSKUPDATECONXCOLAREA ServiceType
 */
$sSKUPDATECONXCOLAREA = new \ServiceType\SSKUPDATECONXCOLAREA($options);
/**
 * Sample call for SSK_UPDATE_CONXCOLAREA operation/method
 */
if ($sSKUPDATECONXCOLAREA->SSK_UPDATE_CONXCOLAREA($IN_CHECK_ONLY, new \StructType\TConXColArea()) !== false) {
    print_r($sSKUPDATECONXCOLAREA->getResult());
} else {
    print_r($sSKUPDATECONXCOLAREA->getLastError());
}
/**
 * Samples for SSKUPDATESUBSCRIPTION ServiceType
 */
$sSKUPDATESUBSCRIPTION = new \ServiceType\SSKUPDATESUBSCRIPTION($options);
/**
 * Sample call for SSK_UPDATE_SUBSCRIPTION operation/method
 */
if ($sSKUPDATESUBSCRIPTION->SSK_UPDATE_SUBSCRIPTION($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_SUBSCRIPTION_SOURCE_ID, $IN_SUBSCRIPTION_PR_ARTICLE_ID, $IN_SUBCODE_ID, $IN_PAYACCOUNT_ID, $IN_SUBSCRIPTION_AUTOPAYCODE_ID, $IN_ADD_COSTS) !== false) {
    print_r($sSKUPDATESUBSCRIPTION->getResult());
} else {
    print_r($sSKUPDATESUBSCRIPTION->getLastError());
}
/**
 * Samples for SSKUPDATESUBSCRIPTIONEND ServiceType
 */
$sSKUPDATESUBSCRIPTIONEND = new \ServiceType\SSKUPDATESUBSCRIPTIONEND($options);
/**
 * Sample call for SSK_UPDATE_SUBSCRIPTION_END operation/method
 */
if ($sSKUPDATESUBSCRIPTIONEND->SSK_UPDATE_SUBSCRIPTION_END($IN_CHECK_ONLY, $IN_CONTACT_ID, $IN_SUBSCRIPTION_SUBCODE_ID, $IN_SUBSCRIPTION_SUBQUITREASON_ID) !== false) {
    print_r($sSKUPDATESUBSCRIPTIONEND->getResult());
} else {
    print_r($sSKUPDATESUBSCRIPTIONEND->getLastError());
}
