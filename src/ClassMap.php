<?php
/**
 * Class which returns the class map definition
 * @package
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get()
    {
        return array(
            'TDoubleCheck' => '\\StructType\\TDoubleCheck',
            'TCONTACTID' => '\\StructType\\TCONTACTID',
            'TCALLRESULT' => '\\StructType\\TCALLRESULT',
            'TConxAttr' => '\\StructType\\TConxAttr',
            'TArticlesCR' => '\\StructType\\TArticlesCR',
            'TArticle' => '\\StructType\\TArticle',
            'TColAreaCR' => '\\StructType\\TColAreaCR',
            'TColArea' => '\\StructType\\TColArea',
            'TColAreasCR' => '\\StructType\\TColAreasCR',
            'TConxAttrsCR' => '\\StructType\\TConxAttrsCR',
            'TConXColAreasCR' => '\\StructType\\TConXColAreasCR',
            'TConXColArea' => '\\StructType\\TConXColArea',
            'TEVENTS' => '\\StructType\\TEVENTS',
            'TEVENT' => '\\StructType\\TEVENT',
            'TCONTACTFULL' => '\\StructType\\TCONTACTFULL',
            'TNEWADDRESSES' => '\\StructType\\TNEWADDRESSES',
            'TNEWADDRESS' => '\\StructType\\TNEWADDRESS',
            'TNEXTORDERNO' => '\\StructType\\TNEXTORDERNO',
            'TPERIODICALS' => '\\StructType\\TPERIODICALS',
            'TPERIODICAL' => '\\StructType\\TPERIODICAL',
            'TPPSUBS' => '\\StructType\\TPPSUBS',
            'TPPSUB' => '\\StructType\\TPPSUB',
            'TSubscriptions' => '\\StructType\\TSubscriptions',
            'TSubscription' => '\\StructType\\TSubscription',
            'TPersonContact' => '\\StructType\\TPersonContact',
            'TPersonContactCR' => '\\StructType\\TPersonContactCR',
            'TRelationSearch' => '\\StructType\\TRelationSearch',
            'TRelationSearchCR' => '\\StructType\\TRelationSearchCR',
            'TRelationSearchResult' => '\\StructType\\TRelationSearchResult',
        );
    }
}
