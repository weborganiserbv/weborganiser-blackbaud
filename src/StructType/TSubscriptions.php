<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TSubscriptions StructType
 * @subpackage Structs
 */
class TSubscriptions extends AbstractStructBase
{
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The SUBSCRIPTIONS
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TSubscription[]
     * - ref: soapenc:arrayType
     * @var \StructType\TSubscription[]
     */
    public $SUBSCRIPTIONS;
    /**
     * Constructor method for TSubscriptions
     * @uses TSubscriptions::setERROR_ID()
     * @uses TSubscriptions::setSUBSCRIPTIONS()
     * @param int $eRROR_ID
     * @param \StructType\TSubscription[] $sUBSCRIPTIONS
     */
    public function __construct($eRROR_ID = null, array $sUBSCRIPTIONS = array())
    {
        $this
            ->setERROR_ID($eRROR_ID)
            ->setSUBSCRIPTIONS($sUBSCRIPTIONS);
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TSubscriptions
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTIONS value
     * @return \StructType\TSubscription[]|null
     */
    public function getSUBSCRIPTIONS()
    {
        return $this->SUBSCRIPTIONS;
    }
    /**
     * Set SUBSCRIPTIONS value
     * @throws \InvalidArgumentException
     * @param \StructType\TSubscription[] $sUBSCRIPTIONS
     * @return \StructType\TSubscriptions
     */
    public function setSUBSCRIPTIONS(array $sUBSCRIPTIONS = array())
    {
        foreach ($sUBSCRIPTIONS as $tSubscriptionsSUBSCRIPTIONSItem) {
            // validation for constraint: itemType
            if (!$tSubscriptionsSUBSCRIPTIONSItem instanceof \StructType\TSubscription) {
                throw new \InvalidArgumentException(sprintf('The SUBSCRIPTIONS property can only contain items of \StructType\TSubscription, "%s" given', is_object($tSubscriptionsSUBSCRIPTIONSItem) ? get_class($tSubscriptionsSUBSCRIPTIONSItem) : gettype($tSubscriptionsSUBSCRIPTIONSItem)), __LINE__);
            }
        }
        $this->SUBSCRIPTIONS = $sUBSCRIPTIONS;
        return $this;
    }
    /**
     * Add item to SUBSCRIPTIONS value
     * @throws \InvalidArgumentException
     * @param \StructType\TSubscription $item
     * @return \StructType\TSubscriptions
     */
    public function addToSUBSCRIPTIONS(\StructType\TSubscription $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TSubscription) {
            throw new \InvalidArgumentException(sprintf('The SUBSCRIPTIONS property can only contain items of \StructType\TSubscription, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->SUBSCRIPTIONS[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TSubscriptions
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
