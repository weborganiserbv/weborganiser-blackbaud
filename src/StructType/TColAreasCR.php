<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TColAreasCR StructType
 * @subpackage Structs
 */
class TColAreasCR extends AbstractStructBase
{
    /**
     * The ColAreas
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TColArea[]
     * - ref: soapenc:arrayType
     * @var \StructType\TColArea[]
     */
    public $ColAreas;
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * Constructor method for TColAreasCR
     * @uses TColAreasCR::setColAreas()
     * @uses TColAreasCR::setERROR_ID()
     * @param \StructType\TColArea[] $colAreas
     * @param int $eRROR_ID
     */
    public function __construct(array $colAreas = array(), $eRROR_ID = null)
    {
        $this
            ->setColAreas($colAreas)
            ->setERROR_ID($eRROR_ID);
    }
    /**
     * Get ColAreas value
     * @return \StructType\TColArea[]|null
     */
    public function getColAreas()
    {
        return $this->ColAreas;
    }
    /**
     * Set ColAreas value
     * @throws \InvalidArgumentException
     * @param \StructType\TColArea[] $colAreas
     * @return \StructType\TColAreasCR
     */
    public function setColAreas(array $colAreas = array())
    {
        foreach ($colAreas as $tColAreasCRColAreasItem) {
            // validation for constraint: itemType
            if (!$tColAreasCRColAreasItem instanceof \StructType\TColArea) {
                throw new \InvalidArgumentException(sprintf('The ColAreas property can only contain items of \StructType\TColArea, "%s" given', is_object($tColAreasCRColAreasItem) ? get_class($tColAreasCRColAreasItem) : gettype($tColAreasCRColAreasItem)), __LINE__);
            }
        }
        $this->ColAreas = $colAreas;
        return $this;
    }
    /**
     * Add item to ColAreas value
     * @throws \InvalidArgumentException
     * @param \StructType\TColArea $item
     * @return \StructType\TColAreasCR
     */
    public function addToColAreas(\StructType\TColArea $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TColArea) {
            throw new \InvalidArgumentException(sprintf('The ColAreas property can only contain items of \StructType\TColArea, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->ColAreas[] = $item;
        return $this;
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TColAreasCR
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TColAreasCR
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
