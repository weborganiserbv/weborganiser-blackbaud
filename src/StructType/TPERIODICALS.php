<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TPERIODICALS StructType
 * @subpackage Structs
 */
class TPERIODICALS extends AbstractStructBase
{
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The PERIODICALS
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TPERIODICAL[]
     * - ref: soapenc:arrayType
     * @var \StructType\TPERIODICAL[]
     */
    public $PERIODICALS;
    /**
     * Constructor method for TPERIODICALS
     * @uses TPERIODICALS::setERROR_ID()
     * @uses TPERIODICALS::setPERIODICALS()
     * @param int $eRROR_ID
     * @param \StructType\TPERIODICAL[] $pERIODICALS
     */
    public function __construct($eRROR_ID = null, array $pERIODICALS = array())
    {
        $this
            ->setERROR_ID($eRROR_ID)
            ->setPERIODICALS($pERIODICALS);
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TPERIODICALS
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get PERIODICALS value
     * @return \StructType\TPERIODICAL[]|null
     */
    public function getPERIODICALS()
    {
        return $this->PERIODICALS;
    }
    /**
     * Set PERIODICALS value
     * @throws \InvalidArgumentException
     * @param \StructType\TPERIODICAL[] $pERIODICALS
     * @return \StructType\TPERIODICALS
     */
    public function setPERIODICALS(array $pERIODICALS = array())
    {
        foreach ($pERIODICALS as $tPERIODICALSPERIODICALSItem) {
            // validation for constraint: itemType
            if (!$tPERIODICALSPERIODICALSItem instanceof \StructType\TPERIODICAL) {
                throw new \InvalidArgumentException(sprintf('The PERIODICALS property can only contain items of \StructType\TPERIODICAL, "%s" given', is_object($tPERIODICALSPERIODICALSItem) ? get_class($tPERIODICALSPERIODICALSItem) : gettype($tPERIODICALSPERIODICALSItem)), __LINE__);
            }
        }
        $this->PERIODICALS = $pERIODICALS;
        return $this;
    }
    /**
     * Add item to PERIODICALS value
     * @throws \InvalidArgumentException
     * @param \StructType\TPERIODICAL $item
     * @return \StructType\TPERIODICALS
     */
    public function addToPERIODICALS(\StructType\TPERIODICAL $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TPERIODICAL) {
            throw new \InvalidArgumentException(sprintf('The PERIODICALS property can only contain items of \StructType\TPERIODICAL, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->PERIODICALS[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TPERIODICALS
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
