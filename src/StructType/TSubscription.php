<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TSubscription StructType
 * @subpackage Structs
 */
class TSubscription extends AbstractStructBase
{
    /**
     * The SUBSCRIPTION_ACTIVE_FLAG
     * @var string
     */
    public $SUBSCRIPTION_ACTIVE_FLAG;
    /**
     * The SUBSCRIPTION_ADD_COSTS
     * @var float
     */
    public $SUBSCRIPTION_ADD_COSTS;
    /**
     * The SUBSCRIPTION_AUTOPAYCODE_ID
     * @var string
     */
    public $SUBSCRIPTION_AUTOPAYCODE_ID;
    /**
     * The SUBSCRIPTION_CONTACT_ID
     * @var int
     */
    public $SUBSCRIPTION_CONTACT_ID;
    /**
     * The SUBSCRIPTION_DATE
     * @var string
     */
    public $SUBSCRIPTION_DATE;
    /**
     * The SUBSCRIPTION_DATE_CANCELLATION
     * @var string
     */
    public $SUBSCRIPTION_DATE_CANCELLATION;
    /**
     * The SUBSCRIPTION_DATE_LATEST
     * @var string
     */
    public $SUBSCRIPTION_DATE_LATEST;
    /**
     * The SUBSCRIPTION_DEL_BEGIN
     * @var int
     */
    public $SUBSCRIPTION_DEL_BEGIN;
    /**
     * The SUBSCRIPTION_DEL_CONTACT_ID
     * @var int
     */
    public $SUBSCRIPTION_DEL_CONTACT_ID;
    /**
     * The SUBSCRIPTION_DEL_END
     * @var int
     */
    public $SUBSCRIPTION_DEL_END;
    /**
     * The SUBSCRIPTION_DEL_INVOICED
     * @var int
     */
    public $SUBSCRIPTION_DEL_INVOICED;
    /**
     * The SUBSCRIPTION_DEL_LATEST
     * @var int
     */
    public $SUBSCRIPTION_DEL_LATEST;
    /**
     * The SUBSCRIPTION_DESCR
     * @var string
     */
    public $SUBSCRIPTION_DESCR;
    /**
     * The SUBSCRIPTION_INFO
     * @var string
     */
    public $SUBSCRIPTION_INFO;
    /**
     * The SUBSCRIPTION_INV_CONTACT_ID
     * @var int
     */
    public $SUBSCRIPTION_INV_CONTACT_ID;
    /**
     * The SUBSCRIPTION_MGM_CONTACT_ID
     * @var int
     */
    public $SUBSCRIPTION_MGM_CONTACT_ID;
    /**
     * The SUBSCRIPTION_PAYACCOUNT_ID
     * @var string
     */
    public $SUBSCRIPTION_PAYACCOUNT_ID;
    /**
     * The SUBSCRIPTION_PERC_DISCOUNT
     * @var int
     */
    public $SUBSCRIPTION_PERC_DISCOUNT;
    /**
     * The SUBSCRIPTION_PR_ARTICLE_ID
     * @var string
     */
    public $SUBSCRIPTION_PR_ARTICLE_ID;
    /**
     * The SUBSCRIPTION_PR_SEND_DATE
     * @var string
     */
    public $SUBSCRIPTION_PR_SEND_DATE;
    /**
     * The SUBSCRIPTION_QUANTITY
     * @var int
     */
    public $SUBSCRIPTION_QUANTITY;
    /**
     * The SUBSCRIPTION_RELATION_ID
     * @var int
     */
    public $SUBSCRIPTION_RELATION_ID;
    /**
     * The SUBSCRIPTION_SEQNO
     * @var int
     */
    public $SUBSCRIPTION_SEQNO;
    /**
     * The SUBSCRIPTION_SHIPCODE_ID
     * @var string
     */
    public $SUBSCRIPTION_SHIPCODE_ID;
    /**
     * The SUBSCRIPTION_SOURCE_ID
     * @var string
     */
    public $SUBSCRIPTION_SOURCE_ID;
    /**
     * The SUBSCRIPTION_SUBCODE_ID
     * @var string
     */
    public $SUBSCRIPTION_SUBCODE_ID;
    /**
     * The SUBSCRIPTION_SUBQUITREASON_ID
     * @var string
     */
    public $SUBSCRIPTION_SUBQUITREASON_ID;
    /**
     * Constructor method for TSubscription
     * @uses TSubscription::setSUBSCRIPTION_ACTIVE_FLAG()
     * @uses TSubscription::setSUBSCRIPTION_ADD_COSTS()
     * @uses TSubscription::setSUBSCRIPTION_AUTOPAYCODE_ID()
     * @uses TSubscription::setSUBSCRIPTION_CONTACT_ID()
     * @uses TSubscription::setSUBSCRIPTION_DATE()
     * @uses TSubscription::setSUBSCRIPTION_DATE_CANCELLATION()
     * @uses TSubscription::setSUBSCRIPTION_DATE_LATEST()
     * @uses TSubscription::setSUBSCRIPTION_DEL_BEGIN()
     * @uses TSubscription::setSUBSCRIPTION_DEL_CONTACT_ID()
     * @uses TSubscription::setSUBSCRIPTION_DEL_END()
     * @uses TSubscription::setSUBSCRIPTION_DEL_INVOICED()
     * @uses TSubscription::setSUBSCRIPTION_DEL_LATEST()
     * @uses TSubscription::setSUBSCRIPTION_DESCR()
     * @uses TSubscription::setSUBSCRIPTION_INFO()
     * @uses TSubscription::setSUBSCRIPTION_INV_CONTACT_ID()
     * @uses TSubscription::setSUBSCRIPTION_MGM_CONTACT_ID()
     * @uses TSubscription::setSUBSCRIPTION_PAYACCOUNT_ID()
     * @uses TSubscription::setSUBSCRIPTION_PERC_DISCOUNT()
     * @uses TSubscription::setSUBSCRIPTION_PR_ARTICLE_ID()
     * @uses TSubscription::setSUBSCRIPTION_PR_SEND_DATE()
     * @uses TSubscription::setSUBSCRIPTION_QUANTITY()
     * @uses TSubscription::setSUBSCRIPTION_RELATION_ID()
     * @uses TSubscription::setSUBSCRIPTION_SEQNO()
     * @uses TSubscription::setSUBSCRIPTION_SHIPCODE_ID()
     * @uses TSubscription::setSUBSCRIPTION_SOURCE_ID()
     * @uses TSubscription::setSUBSCRIPTION_SUBCODE_ID()
     * @uses TSubscription::setSUBSCRIPTION_SUBQUITREASON_ID()
     * @param string $sUBSCRIPTION_ACTIVE_FLAG
     * @param float $sUBSCRIPTION_ADD_COSTS
     * @param string $sUBSCRIPTION_AUTOPAYCODE_ID
     * @param int $sUBSCRIPTION_CONTACT_ID
     * @param string $sUBSCRIPTION_DATE
     * @param string $sUBSCRIPTION_DATE_CANCELLATION
     * @param string $sUBSCRIPTION_DATE_LATEST
     * @param int $sUBSCRIPTION_DEL_BEGIN
     * @param int $sUBSCRIPTION_DEL_CONTACT_ID
     * @param int $sUBSCRIPTION_DEL_END
     * @param int $sUBSCRIPTION_DEL_INVOICED
     * @param int $sUBSCRIPTION_DEL_LATEST
     * @param string $sUBSCRIPTION_DESCR
     * @param string $sUBSCRIPTION_INFO
     * @param int $sUBSCRIPTION_INV_CONTACT_ID
     * @param int $sUBSCRIPTION_MGM_CONTACT_ID
     * @param string $sUBSCRIPTION_PAYACCOUNT_ID
     * @param int $sUBSCRIPTION_PERC_DISCOUNT
     * @param string $sUBSCRIPTION_PR_ARTICLE_ID
     * @param string $sUBSCRIPTION_PR_SEND_DATE
     * @param int $sUBSCRIPTION_QUANTITY
     * @param int $sUBSCRIPTION_RELATION_ID
     * @param int $sUBSCRIPTION_SEQNO
     * @param string $sUBSCRIPTION_SHIPCODE_ID
     * @param string $sUBSCRIPTION_SOURCE_ID
     * @param string $sUBSCRIPTION_SUBCODE_ID
     * @param string $sUBSCRIPTION_SUBQUITREASON_ID
     */
    public function __construct($sUBSCRIPTION_ACTIVE_FLAG = null, $sUBSCRIPTION_ADD_COSTS = null, $sUBSCRIPTION_AUTOPAYCODE_ID = null, $sUBSCRIPTION_CONTACT_ID = null, $sUBSCRIPTION_DATE = null, $sUBSCRIPTION_DATE_CANCELLATION = null, $sUBSCRIPTION_DATE_LATEST = null, $sUBSCRIPTION_DEL_BEGIN = null, $sUBSCRIPTION_DEL_CONTACT_ID = null, $sUBSCRIPTION_DEL_END = null, $sUBSCRIPTION_DEL_INVOICED = null, $sUBSCRIPTION_DEL_LATEST = null, $sUBSCRIPTION_DESCR = null, $sUBSCRIPTION_INFO = null, $sUBSCRIPTION_INV_CONTACT_ID = null, $sUBSCRIPTION_MGM_CONTACT_ID = null, $sUBSCRIPTION_PAYACCOUNT_ID = null, $sUBSCRIPTION_PERC_DISCOUNT = null, $sUBSCRIPTION_PR_ARTICLE_ID = null, $sUBSCRIPTION_PR_SEND_DATE = null, $sUBSCRIPTION_QUANTITY = null, $sUBSCRIPTION_RELATION_ID = null, $sUBSCRIPTION_SEQNO = null, $sUBSCRIPTION_SHIPCODE_ID = null, $sUBSCRIPTION_SOURCE_ID = null, $sUBSCRIPTION_SUBCODE_ID = null, $sUBSCRIPTION_SUBQUITREASON_ID = null)
    {
        $this
            ->setSUBSCRIPTION_ACTIVE_FLAG($sUBSCRIPTION_ACTIVE_FLAG)
            ->setSUBSCRIPTION_ADD_COSTS($sUBSCRIPTION_ADD_COSTS)
            ->setSUBSCRIPTION_AUTOPAYCODE_ID($sUBSCRIPTION_AUTOPAYCODE_ID)
            ->setSUBSCRIPTION_CONTACT_ID($sUBSCRIPTION_CONTACT_ID)
            ->setSUBSCRIPTION_DATE($sUBSCRIPTION_DATE)
            ->setSUBSCRIPTION_DATE_CANCELLATION($sUBSCRIPTION_DATE_CANCELLATION)
            ->setSUBSCRIPTION_DATE_LATEST($sUBSCRIPTION_DATE_LATEST)
            ->setSUBSCRIPTION_DEL_BEGIN($sUBSCRIPTION_DEL_BEGIN)
            ->setSUBSCRIPTION_DEL_CONTACT_ID($sUBSCRIPTION_DEL_CONTACT_ID)
            ->setSUBSCRIPTION_DEL_END($sUBSCRIPTION_DEL_END)
            ->setSUBSCRIPTION_DEL_INVOICED($sUBSCRIPTION_DEL_INVOICED)
            ->setSUBSCRIPTION_DEL_LATEST($sUBSCRIPTION_DEL_LATEST)
            ->setSUBSCRIPTION_DESCR($sUBSCRIPTION_DESCR)
            ->setSUBSCRIPTION_INFO($sUBSCRIPTION_INFO)
            ->setSUBSCRIPTION_INV_CONTACT_ID($sUBSCRIPTION_INV_CONTACT_ID)
            ->setSUBSCRIPTION_MGM_CONTACT_ID($sUBSCRIPTION_MGM_CONTACT_ID)
            ->setSUBSCRIPTION_PAYACCOUNT_ID($sUBSCRIPTION_PAYACCOUNT_ID)
            ->setSUBSCRIPTION_PERC_DISCOUNT($sUBSCRIPTION_PERC_DISCOUNT)
            ->setSUBSCRIPTION_PR_ARTICLE_ID($sUBSCRIPTION_PR_ARTICLE_ID)
            ->setSUBSCRIPTION_PR_SEND_DATE($sUBSCRIPTION_PR_SEND_DATE)
            ->setSUBSCRIPTION_QUANTITY($sUBSCRIPTION_QUANTITY)
            ->setSUBSCRIPTION_RELATION_ID($sUBSCRIPTION_RELATION_ID)
            ->setSUBSCRIPTION_SEQNO($sUBSCRIPTION_SEQNO)
            ->setSUBSCRIPTION_SHIPCODE_ID($sUBSCRIPTION_SHIPCODE_ID)
            ->setSUBSCRIPTION_SOURCE_ID($sUBSCRIPTION_SOURCE_ID)
            ->setSUBSCRIPTION_SUBCODE_ID($sUBSCRIPTION_SUBCODE_ID)
            ->setSUBSCRIPTION_SUBQUITREASON_ID($sUBSCRIPTION_SUBQUITREASON_ID);
    }
    /**
     * Get SUBSCRIPTION_ACTIVE_FLAG value
     * @return string|null
     */
    public function getSUBSCRIPTION_ACTIVE_FLAG()
    {
        return $this->SUBSCRIPTION_ACTIVE_FLAG;
    }
    /**
     * Set SUBSCRIPTION_ACTIVE_FLAG value
     * @param string $sUBSCRIPTION_ACTIVE_FLAG
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_ACTIVE_FLAG($sUBSCRIPTION_ACTIVE_FLAG = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_ACTIVE_FLAG) && !is_string($sUBSCRIPTION_ACTIVE_FLAG)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_ACTIVE_FLAG)), __LINE__);
        }
        $this->SUBSCRIPTION_ACTIVE_FLAG = $sUBSCRIPTION_ACTIVE_FLAG;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_ADD_COSTS value
     * @return float|null
     */
    public function getSUBSCRIPTION_ADD_COSTS()
    {
        return $this->SUBSCRIPTION_ADD_COSTS;
    }
    /**
     * Set SUBSCRIPTION_ADD_COSTS value
     * @param float $sUBSCRIPTION_ADD_COSTS
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_ADD_COSTS($sUBSCRIPTION_ADD_COSTS = null)
    {
        $this->SUBSCRIPTION_ADD_COSTS = $sUBSCRIPTION_ADD_COSTS;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_AUTOPAYCODE_ID value
     * @return string|null
     */
    public function getSUBSCRIPTION_AUTOPAYCODE_ID()
    {
        return $this->SUBSCRIPTION_AUTOPAYCODE_ID;
    }
    /**
     * Set SUBSCRIPTION_AUTOPAYCODE_ID value
     * @param string $sUBSCRIPTION_AUTOPAYCODE_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_AUTOPAYCODE_ID($sUBSCRIPTION_AUTOPAYCODE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_AUTOPAYCODE_ID) && !is_string($sUBSCRIPTION_AUTOPAYCODE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_AUTOPAYCODE_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_AUTOPAYCODE_ID = $sUBSCRIPTION_AUTOPAYCODE_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_CONTACT_ID value
     * @return int|null
     */
    public function getSUBSCRIPTION_CONTACT_ID()
    {
        return $this->SUBSCRIPTION_CONTACT_ID;
    }
    /**
     * Set SUBSCRIPTION_CONTACT_ID value
     * @param int $sUBSCRIPTION_CONTACT_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_CONTACT_ID($sUBSCRIPTION_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_CONTACT_ID) && !is_numeric($sUBSCRIPTION_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_CONTACT_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_CONTACT_ID = $sUBSCRIPTION_CONTACT_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DATE value
     * @return string|null
     */
    public function getSUBSCRIPTION_DATE()
    {
        return $this->SUBSCRIPTION_DATE;
    }
    /**
     * Set SUBSCRIPTION_DATE value
     * @param string $sUBSCRIPTION_DATE
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DATE($sUBSCRIPTION_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_DATE) && !is_string($sUBSCRIPTION_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_DATE)), __LINE__);
        }
        $this->SUBSCRIPTION_DATE = $sUBSCRIPTION_DATE;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DATE_CANCELLATION value
     * @return string|null
     */
    public function getSUBSCRIPTION_DATE_CANCELLATION()
    {
        return $this->SUBSCRIPTION_DATE_CANCELLATION;
    }
    /**
     * Set SUBSCRIPTION_DATE_CANCELLATION value
     * @param string $sUBSCRIPTION_DATE_CANCELLATION
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DATE_CANCELLATION($sUBSCRIPTION_DATE_CANCELLATION = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_DATE_CANCELLATION) && !is_string($sUBSCRIPTION_DATE_CANCELLATION)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_DATE_CANCELLATION)), __LINE__);
        }
        $this->SUBSCRIPTION_DATE_CANCELLATION = $sUBSCRIPTION_DATE_CANCELLATION;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DATE_LATEST value
     * @return string|null
     */
    public function getSUBSCRIPTION_DATE_LATEST()
    {
        return $this->SUBSCRIPTION_DATE_LATEST;
    }
    /**
     * Set SUBSCRIPTION_DATE_LATEST value
     * @param string $sUBSCRIPTION_DATE_LATEST
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DATE_LATEST($sUBSCRIPTION_DATE_LATEST = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_DATE_LATEST) && !is_string($sUBSCRIPTION_DATE_LATEST)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_DATE_LATEST)), __LINE__);
        }
        $this->SUBSCRIPTION_DATE_LATEST = $sUBSCRIPTION_DATE_LATEST;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DEL_BEGIN value
     * @return int|null
     */
    public function getSUBSCRIPTION_DEL_BEGIN()
    {
        return $this->SUBSCRIPTION_DEL_BEGIN;
    }
    /**
     * Set SUBSCRIPTION_DEL_BEGIN value
     * @param int $sUBSCRIPTION_DEL_BEGIN
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DEL_BEGIN($sUBSCRIPTION_DEL_BEGIN = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_DEL_BEGIN) && !is_numeric($sUBSCRIPTION_DEL_BEGIN)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_DEL_BEGIN)), __LINE__);
        }
        $this->SUBSCRIPTION_DEL_BEGIN = $sUBSCRIPTION_DEL_BEGIN;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DEL_CONTACT_ID value
     * @return int|null
     */
    public function getSUBSCRIPTION_DEL_CONTACT_ID()
    {
        return $this->SUBSCRIPTION_DEL_CONTACT_ID;
    }
    /**
     * Set SUBSCRIPTION_DEL_CONTACT_ID value
     * @param int $sUBSCRIPTION_DEL_CONTACT_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DEL_CONTACT_ID($sUBSCRIPTION_DEL_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_DEL_CONTACT_ID) && !is_numeric($sUBSCRIPTION_DEL_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_DEL_CONTACT_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_DEL_CONTACT_ID = $sUBSCRIPTION_DEL_CONTACT_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DEL_END value
     * @return int|null
     */
    public function getSUBSCRIPTION_DEL_END()
    {
        return $this->SUBSCRIPTION_DEL_END;
    }
    /**
     * Set SUBSCRIPTION_DEL_END value
     * @param int $sUBSCRIPTION_DEL_END
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DEL_END($sUBSCRIPTION_DEL_END = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_DEL_END) && !is_numeric($sUBSCRIPTION_DEL_END)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_DEL_END)), __LINE__);
        }
        $this->SUBSCRIPTION_DEL_END = $sUBSCRIPTION_DEL_END;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DEL_INVOICED value
     * @return int|null
     */
    public function getSUBSCRIPTION_DEL_INVOICED()
    {
        return $this->SUBSCRIPTION_DEL_INVOICED;
    }
    /**
     * Set SUBSCRIPTION_DEL_INVOICED value
     * @param int $sUBSCRIPTION_DEL_INVOICED
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DEL_INVOICED($sUBSCRIPTION_DEL_INVOICED = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_DEL_INVOICED) && !is_numeric($sUBSCRIPTION_DEL_INVOICED)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_DEL_INVOICED)), __LINE__);
        }
        $this->SUBSCRIPTION_DEL_INVOICED = $sUBSCRIPTION_DEL_INVOICED;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DEL_LATEST value
     * @return int|null
     */
    public function getSUBSCRIPTION_DEL_LATEST()
    {
        return $this->SUBSCRIPTION_DEL_LATEST;
    }
    /**
     * Set SUBSCRIPTION_DEL_LATEST value
     * @param int $sUBSCRIPTION_DEL_LATEST
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DEL_LATEST($sUBSCRIPTION_DEL_LATEST = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_DEL_LATEST) && !is_numeric($sUBSCRIPTION_DEL_LATEST)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_DEL_LATEST)), __LINE__);
        }
        $this->SUBSCRIPTION_DEL_LATEST = $sUBSCRIPTION_DEL_LATEST;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_DESCR value
     * @return string|null
     */
    public function getSUBSCRIPTION_DESCR()
    {
        return $this->SUBSCRIPTION_DESCR;
    }
    /**
     * Set SUBSCRIPTION_DESCR value
     * @param string $sUBSCRIPTION_DESCR
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_DESCR($sUBSCRIPTION_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_DESCR) && !is_string($sUBSCRIPTION_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_DESCR)), __LINE__);
        }
        $this->SUBSCRIPTION_DESCR = $sUBSCRIPTION_DESCR;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_INFO value
     * @return string|null
     */
    public function getSUBSCRIPTION_INFO()
    {
        return $this->SUBSCRIPTION_INFO;
    }
    /**
     * Set SUBSCRIPTION_INFO value
     * @param string $sUBSCRIPTION_INFO
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_INFO($sUBSCRIPTION_INFO = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_INFO) && !is_string($sUBSCRIPTION_INFO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_INFO)), __LINE__);
        }
        $this->SUBSCRIPTION_INFO = $sUBSCRIPTION_INFO;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_INV_CONTACT_ID value
     * @return int|null
     */
    public function getSUBSCRIPTION_INV_CONTACT_ID()
    {
        return $this->SUBSCRIPTION_INV_CONTACT_ID;
    }
    /**
     * Set SUBSCRIPTION_INV_CONTACT_ID value
     * @param int $sUBSCRIPTION_INV_CONTACT_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_INV_CONTACT_ID($sUBSCRIPTION_INV_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_INV_CONTACT_ID) && !is_numeric($sUBSCRIPTION_INV_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_INV_CONTACT_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_INV_CONTACT_ID = $sUBSCRIPTION_INV_CONTACT_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_MGM_CONTACT_ID value
     * @return int|null
     */
    public function getSUBSCRIPTION_MGM_CONTACT_ID()
    {
        return $this->SUBSCRIPTION_MGM_CONTACT_ID;
    }
    /**
     * Set SUBSCRIPTION_MGM_CONTACT_ID value
     * @param int $sUBSCRIPTION_MGM_CONTACT_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_MGM_CONTACT_ID($sUBSCRIPTION_MGM_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_MGM_CONTACT_ID) && !is_numeric($sUBSCRIPTION_MGM_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_MGM_CONTACT_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_MGM_CONTACT_ID = $sUBSCRIPTION_MGM_CONTACT_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_PAYACCOUNT_ID value
     * @return string|null
     */
    public function getSUBSCRIPTION_PAYACCOUNT_ID()
    {
        return $this->SUBSCRIPTION_PAYACCOUNT_ID;
    }
    /**
     * Set SUBSCRIPTION_PAYACCOUNT_ID value
     * @param string $sUBSCRIPTION_PAYACCOUNT_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_PAYACCOUNT_ID($sUBSCRIPTION_PAYACCOUNT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_PAYACCOUNT_ID) && !is_string($sUBSCRIPTION_PAYACCOUNT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_PAYACCOUNT_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_PAYACCOUNT_ID = $sUBSCRIPTION_PAYACCOUNT_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_PERC_DISCOUNT value
     * @return int|null
     */
    public function getSUBSCRIPTION_PERC_DISCOUNT()
    {
        return $this->SUBSCRIPTION_PERC_DISCOUNT;
    }
    /**
     * Set SUBSCRIPTION_PERC_DISCOUNT value
     * @param int $sUBSCRIPTION_PERC_DISCOUNT
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_PERC_DISCOUNT($sUBSCRIPTION_PERC_DISCOUNT = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_PERC_DISCOUNT) && !is_numeric($sUBSCRIPTION_PERC_DISCOUNT)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_PERC_DISCOUNT)), __LINE__);
        }
        $this->SUBSCRIPTION_PERC_DISCOUNT = $sUBSCRIPTION_PERC_DISCOUNT;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_PR_ARTICLE_ID value
     * @return string|null
     */
    public function getSUBSCRIPTION_PR_ARTICLE_ID()
    {
        return $this->SUBSCRIPTION_PR_ARTICLE_ID;
    }
    /**
     * Set SUBSCRIPTION_PR_ARTICLE_ID value
     * @param string $sUBSCRIPTION_PR_ARTICLE_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_PR_ARTICLE_ID($sUBSCRIPTION_PR_ARTICLE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_PR_ARTICLE_ID) && !is_string($sUBSCRIPTION_PR_ARTICLE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_PR_ARTICLE_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_PR_ARTICLE_ID = $sUBSCRIPTION_PR_ARTICLE_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_PR_SEND_DATE value
     * @return string|null
     */
    public function getSUBSCRIPTION_PR_SEND_DATE()
    {
        return $this->SUBSCRIPTION_PR_SEND_DATE;
    }
    /**
     * Set SUBSCRIPTION_PR_SEND_DATE value
     * @param string $sUBSCRIPTION_PR_SEND_DATE
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_PR_SEND_DATE($sUBSCRIPTION_PR_SEND_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_PR_SEND_DATE) && !is_string($sUBSCRIPTION_PR_SEND_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_PR_SEND_DATE)), __LINE__);
        }
        $this->SUBSCRIPTION_PR_SEND_DATE = $sUBSCRIPTION_PR_SEND_DATE;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_QUANTITY value
     * @return int|null
     */
    public function getSUBSCRIPTION_QUANTITY()
    {
        return $this->SUBSCRIPTION_QUANTITY;
    }
    /**
     * Set SUBSCRIPTION_QUANTITY value
     * @param int $sUBSCRIPTION_QUANTITY
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_QUANTITY($sUBSCRIPTION_QUANTITY = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_QUANTITY) && !is_numeric($sUBSCRIPTION_QUANTITY)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_QUANTITY)), __LINE__);
        }
        $this->SUBSCRIPTION_QUANTITY = $sUBSCRIPTION_QUANTITY;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_RELATION_ID value
     * @return int|null
     */
    public function getSUBSCRIPTION_RELATION_ID()
    {
        return $this->SUBSCRIPTION_RELATION_ID;
    }
    /**
     * Set SUBSCRIPTION_RELATION_ID value
     * @param int $sUBSCRIPTION_RELATION_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_RELATION_ID($sUBSCRIPTION_RELATION_ID = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_RELATION_ID) && !is_numeric($sUBSCRIPTION_RELATION_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_RELATION_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_RELATION_ID = $sUBSCRIPTION_RELATION_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_SEQNO value
     * @return int|null
     */
    public function getSUBSCRIPTION_SEQNO()
    {
        return $this->SUBSCRIPTION_SEQNO;
    }
    /**
     * Set SUBSCRIPTION_SEQNO value
     * @param int $sUBSCRIPTION_SEQNO
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_SEQNO($sUBSCRIPTION_SEQNO = null)
    {
        // validation for constraint: int
        if (!is_null($sUBSCRIPTION_SEQNO) && !is_numeric($sUBSCRIPTION_SEQNO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($sUBSCRIPTION_SEQNO)), __LINE__);
        }
        $this->SUBSCRIPTION_SEQNO = $sUBSCRIPTION_SEQNO;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_SHIPCODE_ID value
     * @return string|null
     */
    public function getSUBSCRIPTION_SHIPCODE_ID()
    {
        return $this->SUBSCRIPTION_SHIPCODE_ID;
    }
    /**
     * Set SUBSCRIPTION_SHIPCODE_ID value
     * @param string $sUBSCRIPTION_SHIPCODE_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_SHIPCODE_ID($sUBSCRIPTION_SHIPCODE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_SHIPCODE_ID) && !is_string($sUBSCRIPTION_SHIPCODE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_SHIPCODE_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_SHIPCODE_ID = $sUBSCRIPTION_SHIPCODE_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_SOURCE_ID value
     * @return string|null
     */
    public function getSUBSCRIPTION_SOURCE_ID()
    {
        return $this->SUBSCRIPTION_SOURCE_ID;
    }
    /**
     * Set SUBSCRIPTION_SOURCE_ID value
     * @param string $sUBSCRIPTION_SOURCE_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_SOURCE_ID($sUBSCRIPTION_SOURCE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_SOURCE_ID) && !is_string($sUBSCRIPTION_SOURCE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_SOURCE_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_SOURCE_ID = $sUBSCRIPTION_SOURCE_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_SUBCODE_ID value
     * @return string|null
     */
    public function getSUBSCRIPTION_SUBCODE_ID()
    {
        return $this->SUBSCRIPTION_SUBCODE_ID;
    }
    /**
     * Set SUBSCRIPTION_SUBCODE_ID value
     * @param string $sUBSCRIPTION_SUBCODE_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_SUBCODE_ID($sUBSCRIPTION_SUBCODE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_SUBCODE_ID) && !is_string($sUBSCRIPTION_SUBCODE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_SUBCODE_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_SUBCODE_ID = $sUBSCRIPTION_SUBCODE_ID;
        return $this;
    }
    /**
     * Get SUBSCRIPTION_SUBQUITREASON_ID value
     * @return string|null
     */
    public function getSUBSCRIPTION_SUBQUITREASON_ID()
    {
        return $this->SUBSCRIPTION_SUBQUITREASON_ID;
    }
    /**
     * Set SUBSCRIPTION_SUBQUITREASON_ID value
     * @param string $sUBSCRIPTION_SUBQUITREASON_ID
     * @return \StructType\TSubscription
     */
    public function setSUBSCRIPTION_SUBQUITREASON_ID($sUBSCRIPTION_SUBQUITREASON_ID = null)
    {
        // validation for constraint: string
        if (!is_null($sUBSCRIPTION_SUBQUITREASON_ID) && !is_string($sUBSCRIPTION_SUBQUITREASON_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sUBSCRIPTION_SUBQUITREASON_ID)), __LINE__);
        }
        $this->SUBSCRIPTION_SUBQUITREASON_ID = $sUBSCRIPTION_SUBQUITREASON_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TSubscription
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
