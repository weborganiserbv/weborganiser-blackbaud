<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TPersonContact StructType
 * @subpackage Structs
 */
class TPersonContact extends AbstractStructBase
{
    /**
     * The Contact_Email
     * @var string
     */
    public $Contact_Email;
    /**
     * The Contact_Entry_SalesMan_ID
     * @var string
     */
    public $Contact_Entry_SalesMan_ID;
    /**
     * The Contact_Password
     * @var string
     */
    public $Contact_Password;
    /**
     * The Contact_Relation_ID
     * @var int
     */
    public $Contact_Relation_ID;
    /**
     * The Contact_Telephone
     * @var string
     */
    public $Contact_Telephone;
    /**
     * The Contact_Username
     * @var string
     */
    public $Contact_Username;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * The Person_Birth_Date
     * @var string
     */
    public $Person_Birth_Date;
    /**
     * The Person_Email
     * @var string
     */
    public $Person_Email;
    /**
     * The Person_First_Name
     * @var string
     */
    public $Person_First_Name;
    /**
     * The Person_Initials
     * @var string
     */
    public $Person_Initials;
    /**
     * The Person_Middle_Name
     * @var string
     */
    public $Person_Middle_Name;
    /**
     * The Person_Prefix
     * @var string
     */
    public $Person_Prefix;
    /**
     * The Person_Surname_1
     * @var string
     */
    public $Person_Surname_1;
    /**
     * The Person_Surname_2
     * @var string
     */
    public $Person_Surname_2;
    /**
     * The Person_Telephone
     * @var string
     */
    public $Person_Telephone;
    /**
     * The Person_Telephone_GSM
     * @var string
     */
    public $Person_Telephone_GSM;
    /**
     * The Person_Title_ID
     * @var string
     */
    public $Person_Title_ID;
    /**
     * Constructor method for TPersonContact
     * @uses TPersonContact::setContact_Email()
     * @uses TPersonContact::setContact_Entry_SalesMan_ID()
     * @uses TPersonContact::setContact_Password()
     * @uses TPersonContact::setContact_Relation_ID()
     * @uses TPersonContact::setContact_Telephone()
     * @uses TPersonContact::setContact_Username()
     * @uses TPersonContact::setFREE_X100_1()
     * @uses TPersonContact::setFREE_X100_2()
     * @uses TPersonContact::setFREE_X100_3()
     * @uses TPersonContact::setFREE_X100_4()
     * @uses TPersonContact::setFREE_X100_5()
     * @uses TPersonContact::setPerson_Birth_Date()
     * @uses TPersonContact::setPerson_Email()
     * @uses TPersonContact::setPerson_First_Name()
     * @uses TPersonContact::setPerson_Initials()
     * @uses TPersonContact::setPerson_Middle_Name()
     * @uses TPersonContact::setPerson_Prefix()
     * @uses TPersonContact::setPerson_Surname_1()
     * @uses TPersonContact::setPerson_Surname_2()
     * @uses TPersonContact::setPerson_Telephone()
     * @uses TPersonContact::setPerson_Telephone_GSM()
     * @uses TPersonContact::setPerson_Title_ID()
     * @param string $contact_Email
     * @param string $contact_Entry_SalesMan_ID
     * @param string $contact_Password
     * @param int $contact_Relation_ID
     * @param string $contact_Telephone
     * @param string $contact_Username
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     * @param string $person_Birth_Date
     * @param string $person_Email
     * @param string $person_First_Name
     * @param string $person_Initials
     * @param string $person_Middle_Name
     * @param string $person_Prefix
     * @param string $person_Surname_1
     * @param string $person_Surname_2
     * @param string $person_Telephone
     * @param string $person_Telephone_GSM
     * @param string $person_Title_ID
     */
    public function __construct($contact_Email = null, $contact_Entry_SalesMan_ID = null, $contact_Password = null, $contact_Relation_ID = null, $contact_Telephone = null, $contact_Username = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null, $person_Birth_Date = null, $person_Email = null, $person_First_Name = null, $person_Initials = null, $person_Middle_Name = null, $person_Prefix = null, $person_Surname_1 = null, $person_Surname_2 = null, $person_Telephone = null, $person_Telephone_GSM = null, $person_Title_ID = null)
    {
        $this
            ->setContact_Email($contact_Email)
            ->setContact_Entry_SalesMan_ID($contact_Entry_SalesMan_ID)
            ->setContact_Password($contact_Password)
            ->setContact_Relation_ID($contact_Relation_ID)
            ->setContact_Telephone($contact_Telephone)
            ->setContact_Username($contact_Username)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5)
            ->setPerson_Birth_Date($person_Birth_Date)
            ->setPerson_Email($person_Email)
            ->setPerson_First_Name($person_First_Name)
            ->setPerson_Initials($person_Initials)
            ->setPerson_Middle_Name($person_Middle_Name)
            ->setPerson_Prefix($person_Prefix)
            ->setPerson_Surname_1($person_Surname_1)
            ->setPerson_Surname_2($person_Surname_2)
            ->setPerson_Telephone($person_Telephone)
            ->setPerson_Telephone_GSM($person_Telephone_GSM)
            ->setPerson_Title_ID($person_Title_ID);
    }
    /**
     * Get Contact_Email value
     * @return string|null
     */
    public function getContact_Email()
    {
        return $this->Contact_Email;
    }
    /**
     * Set Contact_Email value
     * @param string $contact_Email
     * @return \StructType\TPersonContact
     */
    public function setContact_Email($contact_Email = null)
    {
        // validation for constraint: string
        if (!is_null($contact_Email) && !is_string($contact_Email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($contact_Email)), __LINE__);
        }
        $this->Contact_Email = $contact_Email;
        return $this;
    }
    /**
     * Get Contact_Entry_SalesMan_ID value
     * @return string|null
     */
    public function getContact_Entry_SalesMan_ID()
    {
        return $this->Contact_Entry_SalesMan_ID;
    }
    /**
     * Set Contact_Entry_SalesMan_ID value
     * @param string $contact_Entry_SalesMan_ID
     * @return \StructType\TPersonContact
     */
    public function setContact_Entry_SalesMan_ID($contact_Entry_SalesMan_ID = null)
    {
        // validation for constraint: string
        if (!is_null($contact_Entry_SalesMan_ID) && !is_string($contact_Entry_SalesMan_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($contact_Entry_SalesMan_ID)), __LINE__);
        }
        $this->Contact_Entry_SalesMan_ID = $contact_Entry_SalesMan_ID;
        return $this;
    }
    /**
     * Get Contact_Password value
     * @return string|null
     */
    public function getContact_Password()
    {
        return $this->Contact_Password;
    }
    /**
     * Set Contact_Password value
     * @param string $contact_Password
     * @return \StructType\TPersonContact
     */
    public function setContact_Password($contact_Password = null)
    {
        // validation for constraint: string
        if (!is_null($contact_Password) && !is_string($contact_Password)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($contact_Password)), __LINE__);
        }
        $this->Contact_Password = $contact_Password;
        return $this;
    }
    /**
     * Get Contact_Relation_ID value
     * @return int|null
     */
    public function getContact_Relation_ID()
    {
        return $this->Contact_Relation_ID;
    }
    /**
     * Set Contact_Relation_ID value
     * @param int $contact_Relation_ID
     * @return \StructType\TPersonContact
     */
    public function setContact_Relation_ID($contact_Relation_ID = null)
    {
        // validation for constraint: int
        if (!is_null($contact_Relation_ID) && !is_numeric($contact_Relation_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($contact_Relation_ID)), __LINE__);
        }
        $this->Contact_Relation_ID = $contact_Relation_ID;
        return $this;
    }
    /**
     * Get Contact_Telephone value
     * @return string|null
     */
    public function getContact_Telephone()
    {
        return $this->Contact_Telephone;
    }
    /**
     * Set Contact_Telephone value
     * @param string $contact_Telephone
     * @return \StructType\TPersonContact
     */
    public function setContact_Telephone($contact_Telephone = null)
    {
        // validation for constraint: string
        if (!is_null($contact_Telephone) && !is_string($contact_Telephone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($contact_Telephone)), __LINE__);
        }
        $this->Contact_Telephone = $contact_Telephone;
        return $this;
    }
    /**
     * Get Contact_Username value
     * @return string|null
     */
    public function getContact_Username()
    {
        return $this->Contact_Username;
    }
    /**
     * Set Contact_Username value
     * @param string $contact_Username
     * @return \StructType\TPersonContact
     */
    public function setContact_Username($contact_Username = null)
    {
        // validation for constraint: string
        if (!is_null($contact_Username) && !is_string($contact_Username)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($contact_Username)), __LINE__);
        }
        $this->Contact_Username = $contact_Username;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TPersonContact
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TPersonContact
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TPersonContact
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TPersonContact
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TPersonContact
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Get Person_Birth_Date value
     * @return string|null
     */
    public function getPerson_Birth_Date()
    {
        return $this->Person_Birth_Date;
    }
    /**
     * Set Person_Birth_Date value
     * @param string $person_Birth_Date
     * @return \StructType\TPersonContact
     */
    public function setPerson_Birth_Date($person_Birth_Date = null)
    {
        // validation for constraint: string
        if (!is_null($person_Birth_Date) && !is_string($person_Birth_Date)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Birth_Date)), __LINE__);
        }
        $this->Person_Birth_Date = $person_Birth_Date;
        return $this;
    }
    /**
     * Get Person_Email value
     * @return string|null
     */
    public function getPerson_Email()
    {
        return $this->Person_Email;
    }
    /**
     * Set Person_Email value
     * @param string $person_Email
     * @return \StructType\TPersonContact
     */
    public function setPerson_Email($person_Email = null)
    {
        // validation for constraint: string
        if (!is_null($person_Email) && !is_string($person_Email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Email)), __LINE__);
        }
        $this->Person_Email = $person_Email;
        return $this;
    }
    /**
     * Get Person_First_Name value
     * @return string|null
     */
    public function getPerson_First_Name()
    {
        return $this->Person_First_Name;
    }
    /**
     * Set Person_First_Name value
     * @param string $person_First_Name
     * @return \StructType\TPersonContact
     */
    public function setPerson_First_Name($person_First_Name = null)
    {
        // validation for constraint: string
        if (!is_null($person_First_Name) && !is_string($person_First_Name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_First_Name)), __LINE__);
        }
        $this->Person_First_Name = $person_First_Name;
        return $this;
    }
    /**
     * Get Person_Initials value
     * @return string|null
     */
    public function getPerson_Initials()
    {
        return $this->Person_Initials;
    }
    /**
     * Set Person_Initials value
     * @param string $person_Initials
     * @return \StructType\TPersonContact
     */
    public function setPerson_Initials($person_Initials = null)
    {
        // validation for constraint: string
        if (!is_null($person_Initials) && !is_string($person_Initials)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Initials)), __LINE__);
        }
        $this->Person_Initials = $person_Initials;
        return $this;
    }
    /**
     * Get Person_Middle_Name value
     * @return string|null
     */
    public function getPerson_Middle_Name()
    {
        return $this->Person_Middle_Name;
    }
    /**
     * Set Person_Middle_Name value
     * @param string $person_Middle_Name
     * @return \StructType\TPersonContact
     */
    public function setPerson_Middle_Name($person_Middle_Name = null)
    {
        // validation for constraint: string
        if (!is_null($person_Middle_Name) && !is_string($person_Middle_Name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Middle_Name)), __LINE__);
        }
        $this->Person_Middle_Name = $person_Middle_Name;
        return $this;
    }
    /**
     * Get Person_Prefix value
     * @return string|null
     */
    public function getPerson_Prefix()
    {
        return $this->Person_Prefix;
    }
    /**
     * Set Person_Prefix value
     * @param string $person_Prefix
     * @return \StructType\TPersonContact
     */
    public function setPerson_Prefix($person_Prefix = null)
    {
        // validation for constraint: string
        if (!is_null($person_Prefix) && !is_string($person_Prefix)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Prefix)), __LINE__);
        }
        $this->Person_Prefix = $person_Prefix;
        return $this;
    }
    /**
     * Get Person_Surname_1 value
     * @return string|null
     */
    public function getPerson_Surname_1()
    {
        return $this->Person_Surname_1;
    }
    /**
     * Set Person_Surname_1 value
     * @param string $person_Surname_1
     * @return \StructType\TPersonContact
     */
    public function setPerson_Surname_1($person_Surname_1 = null)
    {
        // validation for constraint: string
        if (!is_null($person_Surname_1) && !is_string($person_Surname_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Surname_1)), __LINE__);
        }
        $this->Person_Surname_1 = $person_Surname_1;
        return $this;
    }
    /**
     * Get Person_Surname_2 value
     * @return string|null
     */
    public function getPerson_Surname_2()
    {
        return $this->Person_Surname_2;
    }
    /**
     * Set Person_Surname_2 value
     * @param string $person_Surname_2
     * @return \StructType\TPersonContact
     */
    public function setPerson_Surname_2($person_Surname_2 = null)
    {
        // validation for constraint: string
        if (!is_null($person_Surname_2) && !is_string($person_Surname_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Surname_2)), __LINE__);
        }
        $this->Person_Surname_2 = $person_Surname_2;
        return $this;
    }
    /**
     * Get Person_Telephone value
     * @return string|null
     */
    public function getPerson_Telephone()
    {
        return $this->Person_Telephone;
    }
    /**
     * Set Person_Telephone value
     * @param string $person_Telephone
     * @return \StructType\TPersonContact
     */
    public function setPerson_Telephone($person_Telephone = null)
    {
        // validation for constraint: string
        if (!is_null($person_Telephone) && !is_string($person_Telephone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Telephone)), __LINE__);
        }
        $this->Person_Telephone = $person_Telephone;
        return $this;
    }
    /**
     * Get Person_Telephone_GSM value
     * @return string|null
     */
    public function getPerson_Telephone_GSM()
    {
        return $this->Person_Telephone_GSM;
    }
    /**
     * Set Person_Telephone_GSM value
     * @param string $person_Telephone_GSM
     * @return \StructType\TPersonContact
     */
    public function setPerson_Telephone_GSM($person_Telephone_GSM = null)
    {
        // validation for constraint: string
        if (!is_null($person_Telephone_GSM) && !is_string($person_Telephone_GSM)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Telephone_GSM)), __LINE__);
        }
        $this->Person_Telephone_GSM = $person_Telephone_GSM;
        return $this;
    }
    /**
     * Get Person_Title_ID value
     * @return string|null
     */
    public function getPerson_Title_ID()
    {
        return $this->Person_Title_ID;
    }
    /**
     * Set Person_Title_ID value
     * @param string $person_Title_ID
     * @return \StructType\TPersonContact
     */
    public function setPerson_Title_ID($person_Title_ID = null)
    {
        // validation for constraint: string
        if (!is_null($person_Title_ID) && !is_string($person_Title_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($person_Title_ID)), __LINE__);
        }
        $this->Person_Title_ID = $person_Title_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TPersonContact
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
