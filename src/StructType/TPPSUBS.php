<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TPPSUBS StructType
 * @subpackage Structs
 */
class TPPSUBS extends AbstractStructBase
{
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The PPSUBS
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TPPSUB[]
     * - ref: soapenc:arrayType
     * @var \StructType\TPPSUB[]
     */
    public $PPSUBS;
    /**
     * Constructor method for TPPSUBS
     * @uses TPPSUBS::setERROR_ID()
     * @uses TPPSUBS::setPPSUBS()
     * @param int $eRROR_ID
     * @param \StructType\TPPSUB[] $pPSUBS
     */
    public function __construct($eRROR_ID = null, array $pPSUBS = array())
    {
        $this
            ->setERROR_ID($eRROR_ID)
            ->setPPSUBS($pPSUBS);
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TPPSUBS
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get PPSUBS value
     * @return \StructType\TPPSUB[]|null
     */
    public function getPPSUBS()
    {
        return $this->PPSUBS;
    }
    /**
     * Set PPSUBS value
     * @throws \InvalidArgumentException
     * @param \StructType\TPPSUB[] $pPSUBS
     * @return \StructType\TPPSUBS
     */
    public function setPPSUBS(array $pPSUBS = array())
    {
        foreach ($pPSUBS as $tPPSUBSPPSUBSItem) {
            // validation for constraint: itemType
            if (!$tPPSUBSPPSUBSItem instanceof \StructType\TPPSUB) {
                throw new \InvalidArgumentException(sprintf('The PPSUBS property can only contain items of \StructType\TPPSUB, "%s" given', is_object($tPPSUBSPPSUBSItem) ? get_class($tPPSUBSPPSUBSItem) : gettype($tPPSUBSPPSUBSItem)), __LINE__);
            }
        }
        $this->PPSUBS = $pPSUBS;
        return $this;
    }
    /**
     * Add item to PPSUBS value
     * @throws \InvalidArgumentException
     * @param \StructType\TPPSUB $item
     * @return \StructType\TPPSUBS
     */
    public function addToPPSUBS(\StructType\TPPSUB $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TPPSUB) {
            throw new \InvalidArgumentException(sprintf('The PPSUBS property can only contain items of \StructType\TPPSUB, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->PPSUBS[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TPPSUBS
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
