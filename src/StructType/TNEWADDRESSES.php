<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TNEWADDRESSES StructType
 * @subpackage Structs
 */
class TNEWADDRESSES extends AbstractStructBase
{
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The NEWADDRESSES
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TNEWADDRESS[]
     * - ref: soapenc:arrayType
     * @var \StructType\TNEWADDRESS[]
     */
    public $NEWADDRESSES;
    /**
     * Constructor method for TNEWADDRESSES
     * @uses TNEWADDRESSES::setERROR_ID()
     * @uses TNEWADDRESSES::setNEWADDRESSES()
     * @param int $eRROR_ID
     * @param \StructType\TNEWADDRESS[] $nEWADDRESSES
     */
    public function __construct($eRROR_ID = null, array $nEWADDRESSES = array())
    {
        $this
            ->setERROR_ID($eRROR_ID)
            ->setNEWADDRESSES($nEWADDRESSES);
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TNEWADDRESSES
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get NEWADDRESSES value
     * @return \StructType\TNEWADDRESS[]|null
     */
    public function getNEWADDRESSES()
    {
        return $this->NEWADDRESSES;
    }
    /**
     * Set NEWADDRESSES value
     * @throws \InvalidArgumentException
     * @param \StructType\TNEWADDRESS[] $nEWADDRESSES
     * @return \StructType\TNEWADDRESSES
     */
    public function setNEWADDRESSES(array $nEWADDRESSES = array())
    {
        foreach ($nEWADDRESSES as $tNEWADDRESSESNEWADDRESSESItem) {
            // validation for constraint: itemType
            if (!$tNEWADDRESSESNEWADDRESSESItem instanceof \StructType\TNEWADDRESS) {
                throw new \InvalidArgumentException(sprintf('The NEWADDRESSES property can only contain items of \StructType\TNEWADDRESS, "%s" given', is_object($tNEWADDRESSESNEWADDRESSESItem) ? get_class($tNEWADDRESSESNEWADDRESSESItem) : gettype($tNEWADDRESSESNEWADDRESSESItem)), __LINE__);
            }
        }
        $this->NEWADDRESSES = $nEWADDRESSES;
        return $this;
    }
    /**
     * Add item to NEWADDRESSES value
     * @throws \InvalidArgumentException
     * @param \StructType\TNEWADDRESS $item
     * @return \StructType\TNEWADDRESSES
     */
    public function addToNEWADDRESSES(\StructType\TNEWADDRESS $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TNEWADDRESS) {
            throw new \InvalidArgumentException(sprintf('The NEWADDRESSES property can only contain items of \StructType\TNEWADDRESS, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->NEWADDRESSES[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TNEWADDRESSES
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
