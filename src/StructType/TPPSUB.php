<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TPPSUB StructType
 * @subpackage Structs
 */
class TPPSUB extends AbstractStructBase
{
    /**
     * The PPSUB_ACTIVE_FLAG
     * @var string
     */
    public $PPSUB_ACTIVE_FLAG;
    /**
     * The PPSUB_AMOUNT
     * @var float
     */
    public $PPSUB_AMOUNT;
    /**
     * The PPSUB_DEL_BEGIN
     * @var int
     */
    public $PPSUB_DEL_BEGIN;
    /**
     * The PPSUB_DEL_END
     * @var int
     */
    public $PPSUB_DEL_END;
    /**
     * The PPSUB_DEL_INVOICED
     * @var int
     */
    public $PPSUB_DEL_INVOICED;
    /**
     * The PPSUB_END_DATE
     * @var string
     */
    public $PPSUB_END_DATE;
    /**
     * The PPSUB_ENTRY_DATE
     * @var string
     */
    public $PPSUB_ENTRY_DATE;
    /**
     * The PPSUB_ID
     * @var int
     */
    public $PPSUB_ID;
    /**
     * The PPSUB_NOTARYACT_ID
     * @var int
     */
    public $PPSUB_NOTARYACT_ID;
    /**
     * The PPSUB_PAYACCOUNT_ID
     * @var string
     */
    public $PPSUB_PAYACCOUNT_ID;
    /**
     * The PPSUB_PPENDREASON_ID
     * @var string
     */
    public $PPSUB_PPENDREASON_ID;
    /**
     * The PPSUB_PPTERM_ID
     * @var string
     */
    public $PPSUB_PPTERM_ID;
    /**
     * The PPSUB_PROJECT_ID
     * @var string
     */
    public $PPSUB_PROJECT_ID;
    /**
     * The PPSUB_SALESMAN_ID
     * @var string
     */
    public $PPSUB_SALESMAN_ID;
    /**
     * The PPSUB_SOURCE_ID
     * @var string
     */
    public $PPSUB_SOURCE_ID;
    /**
     * The PPSUB_START_DATE
     * @var string
     */
    public $PPSUB_START_DATE;
    /**
     * Constructor method for TPPSUB
     * @uses TPPSUB::setPPSUB_ACTIVE_FLAG()
     * @uses TPPSUB::setPPSUB_AMOUNT()
     * @uses TPPSUB::setPPSUB_DEL_BEGIN()
     * @uses TPPSUB::setPPSUB_DEL_END()
     * @uses TPPSUB::setPPSUB_DEL_INVOICED()
     * @uses TPPSUB::setPPSUB_END_DATE()
     * @uses TPPSUB::setPPSUB_ENTRY_DATE()
     * @uses TPPSUB::setPPSUB_ID()
     * @uses TPPSUB::setPPSUB_NOTARYACT_ID()
     * @uses TPPSUB::setPPSUB_PAYACCOUNT_ID()
     * @uses TPPSUB::setPPSUB_PPENDREASON_ID()
     * @uses TPPSUB::setPPSUB_PPTERM_ID()
     * @uses TPPSUB::setPPSUB_PROJECT_ID()
     * @uses TPPSUB::setPPSUB_SALESMAN_ID()
     * @uses TPPSUB::setPPSUB_SOURCE_ID()
     * @uses TPPSUB::setPPSUB_START_DATE()
     * @param string $pPSUB_ACTIVE_FLAG
     * @param float $pPSUB_AMOUNT
     * @param int $pPSUB_DEL_BEGIN
     * @param int $pPSUB_DEL_END
     * @param int $pPSUB_DEL_INVOICED
     * @param string $pPSUB_END_DATE
     * @param string $pPSUB_ENTRY_DATE
     * @param int $pPSUB_ID
     * @param int $pPSUB_NOTARYACT_ID
     * @param string $pPSUB_PAYACCOUNT_ID
     * @param string $pPSUB_PPENDREASON_ID
     * @param string $pPSUB_PPTERM_ID
     * @param string $pPSUB_PROJECT_ID
     * @param string $pPSUB_SALESMAN_ID
     * @param string $pPSUB_SOURCE_ID
     * @param string $pPSUB_START_DATE
     */
    public function __construct($pPSUB_ACTIVE_FLAG = null, $pPSUB_AMOUNT = null, $pPSUB_DEL_BEGIN = null, $pPSUB_DEL_END = null, $pPSUB_DEL_INVOICED = null, $pPSUB_END_DATE = null, $pPSUB_ENTRY_DATE = null, $pPSUB_ID = null, $pPSUB_NOTARYACT_ID = null, $pPSUB_PAYACCOUNT_ID = null, $pPSUB_PPENDREASON_ID = null, $pPSUB_PPTERM_ID = null, $pPSUB_PROJECT_ID = null, $pPSUB_SALESMAN_ID = null, $pPSUB_SOURCE_ID = null, $pPSUB_START_DATE = null)
    {
        $this
            ->setPPSUB_ACTIVE_FLAG($pPSUB_ACTIVE_FLAG)
            ->setPPSUB_AMOUNT($pPSUB_AMOUNT)
            ->setPPSUB_DEL_BEGIN($pPSUB_DEL_BEGIN)
            ->setPPSUB_DEL_END($pPSUB_DEL_END)
            ->setPPSUB_DEL_INVOICED($pPSUB_DEL_INVOICED)
            ->setPPSUB_END_DATE($pPSUB_END_DATE)
            ->setPPSUB_ENTRY_DATE($pPSUB_ENTRY_DATE)
            ->setPPSUB_ID($pPSUB_ID)
            ->setPPSUB_NOTARYACT_ID($pPSUB_NOTARYACT_ID)
            ->setPPSUB_PAYACCOUNT_ID($pPSUB_PAYACCOUNT_ID)
            ->setPPSUB_PPENDREASON_ID($pPSUB_PPENDREASON_ID)
            ->setPPSUB_PPTERM_ID($pPSUB_PPTERM_ID)
            ->setPPSUB_PROJECT_ID($pPSUB_PROJECT_ID)
            ->setPPSUB_SALESMAN_ID($pPSUB_SALESMAN_ID)
            ->setPPSUB_SOURCE_ID($pPSUB_SOURCE_ID)
            ->setPPSUB_START_DATE($pPSUB_START_DATE);
    }
    /**
     * Get PPSUB_ACTIVE_FLAG value
     * @return string|null
     */
    public function getPPSUB_ACTIVE_FLAG()
    {
        return $this->PPSUB_ACTIVE_FLAG;
    }
    /**
     * Set PPSUB_ACTIVE_FLAG value
     * @param string $pPSUB_ACTIVE_FLAG
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_ACTIVE_FLAG($pPSUB_ACTIVE_FLAG = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_ACTIVE_FLAG) && !is_string($pPSUB_ACTIVE_FLAG)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_ACTIVE_FLAG)), __LINE__);
        }
        $this->PPSUB_ACTIVE_FLAG = $pPSUB_ACTIVE_FLAG;
        return $this;
    }
    /**
     * Get PPSUB_AMOUNT value
     * @return float|null
     */
    public function getPPSUB_AMOUNT()
    {
        return $this->PPSUB_AMOUNT;
    }
    /**
     * Set PPSUB_AMOUNT value
     * @param float $pPSUB_AMOUNT
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_AMOUNT($pPSUB_AMOUNT = null)
    {
        $this->PPSUB_AMOUNT = $pPSUB_AMOUNT;
        return $this;
    }
    /**
     * Get PPSUB_DEL_BEGIN value
     * @return int|null
     */
    public function getPPSUB_DEL_BEGIN()
    {
        return $this->PPSUB_DEL_BEGIN;
    }
    /**
     * Set PPSUB_DEL_BEGIN value
     * @param int $pPSUB_DEL_BEGIN
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_DEL_BEGIN($pPSUB_DEL_BEGIN = null)
    {
        // validation for constraint: int
        if (!is_null($pPSUB_DEL_BEGIN) && !is_numeric($pPSUB_DEL_BEGIN)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($pPSUB_DEL_BEGIN)), __LINE__);
        }
        $this->PPSUB_DEL_BEGIN = $pPSUB_DEL_BEGIN;
        return $this;
    }
    /**
     * Get PPSUB_DEL_END value
     * @return int|null
     */
    public function getPPSUB_DEL_END()
    {
        return $this->PPSUB_DEL_END;
    }
    /**
     * Set PPSUB_DEL_END value
     * @param int $pPSUB_DEL_END
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_DEL_END($pPSUB_DEL_END = null)
    {
        // validation for constraint: int
        if (!is_null($pPSUB_DEL_END) && !is_numeric($pPSUB_DEL_END)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($pPSUB_DEL_END)), __LINE__);
        }
        $this->PPSUB_DEL_END = $pPSUB_DEL_END;
        return $this;
    }
    /**
     * Get PPSUB_DEL_INVOICED value
     * @return int|null
     */
    public function getPPSUB_DEL_INVOICED()
    {
        return $this->PPSUB_DEL_INVOICED;
    }
    /**
     * Set PPSUB_DEL_INVOICED value
     * @param int $pPSUB_DEL_INVOICED
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_DEL_INVOICED($pPSUB_DEL_INVOICED = null)
    {
        // validation for constraint: int
        if (!is_null($pPSUB_DEL_INVOICED) && !is_numeric($pPSUB_DEL_INVOICED)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($pPSUB_DEL_INVOICED)), __LINE__);
        }
        $this->PPSUB_DEL_INVOICED = $pPSUB_DEL_INVOICED;
        return $this;
    }
    /**
     * Get PPSUB_END_DATE value
     * @return string|null
     */
    public function getPPSUB_END_DATE()
    {
        return $this->PPSUB_END_DATE;
    }
    /**
     * Set PPSUB_END_DATE value
     * @param string $pPSUB_END_DATE
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_END_DATE($pPSUB_END_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_END_DATE) && !is_string($pPSUB_END_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_END_DATE)), __LINE__);
        }
        $this->PPSUB_END_DATE = $pPSUB_END_DATE;
        return $this;
    }
    /**
     * Get PPSUB_ENTRY_DATE value
     * @return string|null
     */
    public function getPPSUB_ENTRY_DATE()
    {
        return $this->PPSUB_ENTRY_DATE;
    }
    /**
     * Set PPSUB_ENTRY_DATE value
     * @param string $pPSUB_ENTRY_DATE
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_ENTRY_DATE($pPSUB_ENTRY_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_ENTRY_DATE) && !is_string($pPSUB_ENTRY_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_ENTRY_DATE)), __LINE__);
        }
        $this->PPSUB_ENTRY_DATE = $pPSUB_ENTRY_DATE;
        return $this;
    }
    /**
     * Get PPSUB_ID value
     * @return int|null
     */
    public function getPPSUB_ID()
    {
        return $this->PPSUB_ID;
    }
    /**
     * Set PPSUB_ID value
     * @param int $pPSUB_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_ID($pPSUB_ID = null)
    {
        // validation for constraint: int
        if (!is_null($pPSUB_ID) && !is_numeric($pPSUB_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($pPSUB_ID)), __LINE__);
        }
        $this->PPSUB_ID = $pPSUB_ID;
        return $this;
    }
    /**
     * Get PPSUB_NOTARYACT_ID value
     * @return int|null
     */
    public function getPPSUB_NOTARYACT_ID()
    {
        return $this->PPSUB_NOTARYACT_ID;
    }
    /**
     * Set PPSUB_NOTARYACT_ID value
     * @param int $pPSUB_NOTARYACT_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_NOTARYACT_ID($pPSUB_NOTARYACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($pPSUB_NOTARYACT_ID) && !is_numeric($pPSUB_NOTARYACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($pPSUB_NOTARYACT_ID)), __LINE__);
        }
        $this->PPSUB_NOTARYACT_ID = $pPSUB_NOTARYACT_ID;
        return $this;
    }
    /**
     * Get PPSUB_PAYACCOUNT_ID value
     * @return string|null
     */
    public function getPPSUB_PAYACCOUNT_ID()
    {
        return $this->PPSUB_PAYACCOUNT_ID;
    }
    /**
     * Set PPSUB_PAYACCOUNT_ID value
     * @param string $pPSUB_PAYACCOUNT_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_PAYACCOUNT_ID($pPSUB_PAYACCOUNT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_PAYACCOUNT_ID) && !is_string($pPSUB_PAYACCOUNT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_PAYACCOUNT_ID)), __LINE__);
        }
        $this->PPSUB_PAYACCOUNT_ID = $pPSUB_PAYACCOUNT_ID;
        return $this;
    }
    /**
     * Get PPSUB_PPENDREASON_ID value
     * @return string|null
     */
    public function getPPSUB_PPENDREASON_ID()
    {
        return $this->PPSUB_PPENDREASON_ID;
    }
    /**
     * Set PPSUB_PPENDREASON_ID value
     * @param string $pPSUB_PPENDREASON_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_PPENDREASON_ID($pPSUB_PPENDREASON_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_PPENDREASON_ID) && !is_string($pPSUB_PPENDREASON_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_PPENDREASON_ID)), __LINE__);
        }
        $this->PPSUB_PPENDREASON_ID = $pPSUB_PPENDREASON_ID;
        return $this;
    }
    /**
     * Get PPSUB_PPTERM_ID value
     * @return string|null
     */
    public function getPPSUB_PPTERM_ID()
    {
        return $this->PPSUB_PPTERM_ID;
    }
    /**
     * Set PPSUB_PPTERM_ID value
     * @param string $pPSUB_PPTERM_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_PPTERM_ID($pPSUB_PPTERM_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_PPTERM_ID) && !is_string($pPSUB_PPTERM_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_PPTERM_ID)), __LINE__);
        }
        $this->PPSUB_PPTERM_ID = $pPSUB_PPTERM_ID;
        return $this;
    }
    /**
     * Get PPSUB_PROJECT_ID value
     * @return string|null
     */
    public function getPPSUB_PROJECT_ID()
    {
        return $this->PPSUB_PROJECT_ID;
    }
    /**
     * Set PPSUB_PROJECT_ID value
     * @param string $pPSUB_PROJECT_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_PROJECT_ID($pPSUB_PROJECT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_PROJECT_ID) && !is_string($pPSUB_PROJECT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_PROJECT_ID)), __LINE__);
        }
        $this->PPSUB_PROJECT_ID = $pPSUB_PROJECT_ID;
        return $this;
    }
    /**
     * Get PPSUB_SALESMAN_ID value
     * @return string|null
     */
    public function getPPSUB_SALESMAN_ID()
    {
        return $this->PPSUB_SALESMAN_ID;
    }
    /**
     * Set PPSUB_SALESMAN_ID value
     * @param string $pPSUB_SALESMAN_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_SALESMAN_ID($pPSUB_SALESMAN_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_SALESMAN_ID) && !is_string($pPSUB_SALESMAN_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_SALESMAN_ID)), __LINE__);
        }
        $this->PPSUB_SALESMAN_ID = $pPSUB_SALESMAN_ID;
        return $this;
    }
    /**
     * Get PPSUB_SOURCE_ID value
     * @return string|null
     */
    public function getPPSUB_SOURCE_ID()
    {
        return $this->PPSUB_SOURCE_ID;
    }
    /**
     * Set PPSUB_SOURCE_ID value
     * @param string $pPSUB_SOURCE_ID
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_SOURCE_ID($pPSUB_SOURCE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_SOURCE_ID) && !is_string($pPSUB_SOURCE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_SOURCE_ID)), __LINE__);
        }
        $this->PPSUB_SOURCE_ID = $pPSUB_SOURCE_ID;
        return $this;
    }
    /**
     * Get PPSUB_START_DATE value
     * @return string|null
     */
    public function getPPSUB_START_DATE()
    {
        return $this->PPSUB_START_DATE;
    }
    /**
     * Set PPSUB_START_DATE value
     * @param string $pPSUB_START_DATE
     * @return \StructType\TPPSUB
     */
    public function setPPSUB_START_DATE($pPSUB_START_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($pPSUB_START_DATE) && !is_string($pPSUB_START_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pPSUB_START_DATE)), __LINE__);
        }
        $this->PPSUB_START_DATE = $pPSUB_START_DATE;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TPPSUB
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
