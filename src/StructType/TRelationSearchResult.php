<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TRelationSearchResult StructType
 * @subpackage Structs
 */
class TRelationSearchResult extends AbstractStructBase
{
    /**
     * The CITY
     * @var string
     */
    public $CITY;
    /**
     * The CONTACT_ID
     * @var string
     */
    public $CONTACT_ID;
    /**
     * The CONTACT_TELEPHONE
     * @var string
     */
    public $CONTACT_TELEPHONE;
    /**
     * The COUNTRY_ID
     * @var string
     */
    public $COUNTRY_ID;
    /**
     * The EMAIL
     * @var string
     */
    public $EMAIL;
    /**
     * The FIRST_NAME
     * @var string
     */
    public $FIRST_NAME;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * The HOUSE_NO
     * @var string
     */
    public $HOUSE_NO;
    /**
     * The HOUSE_NO_ADD
     * @var string
     */
    public $HOUSE_NO_ADD;
    /**
     * The INITIALS
     * @var string
     */
    public $INITIALS;
    /**
     * The PERSON_TELEPHONE
     * @var string
     */
    public $PERSON_TELEPHONE;
    /**
     * The PERSON_TELEPHONE_GSM
     * @var string
     */
    public $PERSON_TELEPHONE_GSM;
    /**
     * The POSTCODE
     * @var string
     */
    public $POSTCODE;
    /**
     * The RELATION_TELEPHONE
     * @var string
     */
    public $RELATION_TELEPHONE;
    /**
     * The STREET
     * @var string
     */
    public $STREET;
    /**
     * The SURNAME_1
     * @var string
     */
    public $SURNAME_1;
    /**
     * Constructor method for TRelationSearchResult
     * @uses TRelationSearchResult::setCITY()
     * @uses TRelationSearchResult::setCONTACT_ID()
     * @uses TRelationSearchResult::setCONTACT_TELEPHONE()
     * @uses TRelationSearchResult::setCOUNTRY_ID()
     * @uses TRelationSearchResult::setEMAIL()
     * @uses TRelationSearchResult::setFIRST_NAME()
     * @uses TRelationSearchResult::setFREE_X100_1()
     * @uses TRelationSearchResult::setFREE_X100_2()
     * @uses TRelationSearchResult::setFREE_X100_3()
     * @uses TRelationSearchResult::setFREE_X100_4()
     * @uses TRelationSearchResult::setFREE_X100_5()
     * @uses TRelationSearchResult::setHOUSE_NO()
     * @uses TRelationSearchResult::setHOUSE_NO_ADD()
     * @uses TRelationSearchResult::setINITIALS()
     * @uses TRelationSearchResult::setPERSON_TELEPHONE()
     * @uses TRelationSearchResult::setPERSON_TELEPHONE_GSM()
     * @uses TRelationSearchResult::setPOSTCODE()
     * @uses TRelationSearchResult::setRELATION_TELEPHONE()
     * @uses TRelationSearchResult::setSTREET()
     * @uses TRelationSearchResult::setSURNAME_1()
     * @param string $cITY
     * @param string $cONTACT_ID
     * @param string $cONTACT_TELEPHONE
     * @param string $cOUNTRY_ID
     * @param string $eMAIL
     * @param string $fIRST_NAME
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     * @param string $hOUSE_NO
     * @param string $hOUSE_NO_ADD
     * @param string $iNITIALS
     * @param string $pERSON_TELEPHONE
     * @param string $pERSON_TELEPHONE_GSM
     * @param string $pOSTCODE
     * @param string $rELATION_TELEPHONE
     * @param string $sTREET
     * @param string $sURNAME_1
     */
    public function __construct($cITY = null, $cONTACT_ID = null, $cONTACT_TELEPHONE = null, $cOUNTRY_ID = null, $eMAIL = null, $fIRST_NAME = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null, $hOUSE_NO = null, $hOUSE_NO_ADD = null, $iNITIALS = null, $pERSON_TELEPHONE = null, $pERSON_TELEPHONE_GSM = null, $pOSTCODE = null, $rELATION_TELEPHONE = null, $sTREET = null, $sURNAME_1 = null)
    {
        $this
            ->setCITY($cITY)
            ->setCONTACT_ID($cONTACT_ID)
            ->setCONTACT_TELEPHONE($cONTACT_TELEPHONE)
            ->setCOUNTRY_ID($cOUNTRY_ID)
            ->setEMAIL($eMAIL)
            ->setFIRST_NAME($fIRST_NAME)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5)
            ->setHOUSE_NO($hOUSE_NO)
            ->setHOUSE_NO_ADD($hOUSE_NO_ADD)
            ->setINITIALS($iNITIALS)
            ->setPERSON_TELEPHONE($pERSON_TELEPHONE)
            ->setPERSON_TELEPHONE_GSM($pERSON_TELEPHONE_GSM)
            ->setPOSTCODE($pOSTCODE)
            ->setRELATION_TELEPHONE($rELATION_TELEPHONE)
            ->setSTREET($sTREET)
            ->setSURNAME_1($sURNAME_1);
    }
    /**
     * Get CITY value
     * @return string|null
     */
    public function getCITY()
    {
        return $this->CITY;
    }
    /**
     * Set CITY value
     * @param string $cITY
     * @return \StructType\TRelationSearchResult
     */
    public function setCITY($cITY = null)
    {
        // validation for constraint: string
        if (!is_null($cITY) && !is_string($cITY)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cITY)), __LINE__);
        }
        $this->CITY = $cITY;
        return $this;
    }
    /**
     * Get CONTACT_ID value
     * @return string|null
     */
    public function getCONTACT_ID()
    {
        return $this->CONTACT_ID;
    }
    /**
     * Set CONTACT_ID value
     * @param string $cONTACT_ID
     * @return \StructType\TRelationSearchResult
     */
    public function setCONTACT_ID($cONTACT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_ID) && !is_string($cONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_ID)), __LINE__);
        }
        $this->CONTACT_ID = $cONTACT_ID;
        return $this;
    }
    /**
     * Get CONTACT_TELEPHONE value
     * @return string|null
     */
    public function getCONTACT_TELEPHONE()
    {
        return $this->CONTACT_TELEPHONE;
    }
    /**
     * Set CONTACT_TELEPHONE value
     * @param string $cONTACT_TELEPHONE
     * @return \StructType\TRelationSearchResult
     */
    public function setCONTACT_TELEPHONE($cONTACT_TELEPHONE = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_TELEPHONE) && !is_string($cONTACT_TELEPHONE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_TELEPHONE)), __LINE__);
        }
        $this->CONTACT_TELEPHONE = $cONTACT_TELEPHONE;
        return $this;
    }
    /**
     * Get COUNTRY_ID value
     * @return string|null
     */
    public function getCOUNTRY_ID()
    {
        return $this->COUNTRY_ID;
    }
    /**
     * Set COUNTRY_ID value
     * @param string $cOUNTRY_ID
     * @return \StructType\TRelationSearchResult
     */
    public function setCOUNTRY_ID($cOUNTRY_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cOUNTRY_ID) && !is_string($cOUNTRY_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOUNTRY_ID)), __LINE__);
        }
        $this->COUNTRY_ID = $cOUNTRY_ID;
        return $this;
    }
    /**
     * Get EMAIL value
     * @return string|null
     */
    public function getEMAIL()
    {
        return $this->EMAIL;
    }
    /**
     * Set EMAIL value
     * @param string $eMAIL
     * @return \StructType\TRelationSearchResult
     */
    public function setEMAIL($eMAIL = null)
    {
        // validation for constraint: string
        if (!is_null($eMAIL) && !is_string($eMAIL)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eMAIL)), __LINE__);
        }
        $this->EMAIL = $eMAIL;
        return $this;
    }
    /**
     * Get FIRST_NAME value
     * @return string|null
     */
    public function getFIRST_NAME()
    {
        return $this->FIRST_NAME;
    }
    /**
     * Set FIRST_NAME value
     * @param string $fIRST_NAME
     * @return \StructType\TRelationSearchResult
     */
    public function setFIRST_NAME($fIRST_NAME = null)
    {
        // validation for constraint: string
        if (!is_null($fIRST_NAME) && !is_string($fIRST_NAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fIRST_NAME)), __LINE__);
        }
        $this->FIRST_NAME = $fIRST_NAME;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TRelationSearchResult
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TRelationSearchResult
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TRelationSearchResult
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TRelationSearchResult
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TRelationSearchResult
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Get HOUSE_NO value
     * @return string|null
     */
    public function getHOUSE_NO()
    {
        return $this->HOUSE_NO;
    }
    /**
     * Set HOUSE_NO value
     * @param string $hOUSE_NO
     * @return \StructType\TRelationSearchResult
     */
    public function setHOUSE_NO($hOUSE_NO = null)
    {
        // validation for constraint: string
        if (!is_null($hOUSE_NO) && !is_string($hOUSE_NO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($hOUSE_NO)), __LINE__);
        }
        $this->HOUSE_NO = $hOUSE_NO;
        return $this;
    }
    /**
     * Get HOUSE_NO_ADD value
     * @return string|null
     */
    public function getHOUSE_NO_ADD()
    {
        return $this->HOUSE_NO_ADD;
    }
    /**
     * Set HOUSE_NO_ADD value
     * @param string $hOUSE_NO_ADD
     * @return \StructType\TRelationSearchResult
     */
    public function setHOUSE_NO_ADD($hOUSE_NO_ADD = null)
    {
        // validation for constraint: string
        if (!is_null($hOUSE_NO_ADD) && !is_string($hOUSE_NO_ADD)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($hOUSE_NO_ADD)), __LINE__);
        }
        $this->HOUSE_NO_ADD = $hOUSE_NO_ADD;
        return $this;
    }
    /**
     * Get INITIALS value
     * @return string|null
     */
    public function getINITIALS()
    {
        return $this->INITIALS;
    }
    /**
     * Set INITIALS value
     * @param string $iNITIALS
     * @return \StructType\TRelationSearchResult
     */
    public function setINITIALS($iNITIALS = null)
    {
        // validation for constraint: string
        if (!is_null($iNITIALS) && !is_string($iNITIALS)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($iNITIALS)), __LINE__);
        }
        $this->INITIALS = $iNITIALS;
        return $this;
    }
    /**
     * Get PERSON_TELEPHONE value
     * @return string|null
     */
    public function getPERSON_TELEPHONE()
    {
        return $this->PERSON_TELEPHONE;
    }
    /**
     * Set PERSON_TELEPHONE value
     * @param string $pERSON_TELEPHONE
     * @return \StructType\TRelationSearchResult
     */
    public function setPERSON_TELEPHONE($pERSON_TELEPHONE = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_TELEPHONE) && !is_string($pERSON_TELEPHONE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_TELEPHONE)), __LINE__);
        }
        $this->PERSON_TELEPHONE = $pERSON_TELEPHONE;
        return $this;
    }
    /**
     * Get PERSON_TELEPHONE_GSM value
     * @return string|null
     */
    public function getPERSON_TELEPHONE_GSM()
    {
        return $this->PERSON_TELEPHONE_GSM;
    }
    /**
     * Set PERSON_TELEPHONE_GSM value
     * @param string $pERSON_TELEPHONE_GSM
     * @return \StructType\TRelationSearchResult
     */
    public function setPERSON_TELEPHONE_GSM($pERSON_TELEPHONE_GSM = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_TELEPHONE_GSM) && !is_string($pERSON_TELEPHONE_GSM)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_TELEPHONE_GSM)), __LINE__);
        }
        $this->PERSON_TELEPHONE_GSM = $pERSON_TELEPHONE_GSM;
        return $this;
    }
    /**
     * Get POSTCODE value
     * @return string|null
     */
    public function getPOSTCODE()
    {
        return $this->POSTCODE;
    }
    /**
     * Set POSTCODE value
     * @param string $pOSTCODE
     * @return \StructType\TRelationSearchResult
     */
    public function setPOSTCODE($pOSTCODE = null)
    {
        // validation for constraint: string
        if (!is_null($pOSTCODE) && !is_string($pOSTCODE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pOSTCODE)), __LINE__);
        }
        $this->POSTCODE = $pOSTCODE;
        return $this;
    }
    /**
     * Get RELATION_TELEPHONE value
     * @return string|null
     */
    public function getRELATION_TELEPHONE()
    {
        return $this->RELATION_TELEPHONE;
    }
    /**
     * Set RELATION_TELEPHONE value
     * @param string $rELATION_TELEPHONE
     * @return \StructType\TRelationSearchResult
     */
    public function setRELATION_TELEPHONE($rELATION_TELEPHONE = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_TELEPHONE) && !is_string($rELATION_TELEPHONE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_TELEPHONE)), __LINE__);
        }
        $this->RELATION_TELEPHONE = $rELATION_TELEPHONE;
        return $this;
    }
    /**
     * Get STREET value
     * @return string|null
     */
    public function getSTREET()
    {
        return $this->STREET;
    }
    /**
     * Set STREET value
     * @param string $sTREET
     * @return \StructType\TRelationSearchResult
     */
    public function setSTREET($sTREET = null)
    {
        // validation for constraint: string
        if (!is_null($sTREET) && !is_string($sTREET)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sTREET)), __LINE__);
        }
        $this->STREET = $sTREET;
        return $this;
    }
    /**
     * Get SURNAME_1 value
     * @return string|null
     */
    public function getSURNAME_1()
    {
        return $this->SURNAME_1;
    }
    /**
     * Set SURNAME_1 value
     * @param string $sURNAME_1
     * @return \StructType\TRelationSearchResult
     */
    public function setSURNAME_1($sURNAME_1 = null)
    {
        // validation for constraint: string
        if (!is_null($sURNAME_1) && !is_string($sURNAME_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sURNAME_1)), __LINE__);
        }
        $this->SURNAME_1 = $sURNAME_1;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TRelationSearchResult
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
