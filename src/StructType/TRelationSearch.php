<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TRelationSearch StructType
 * @subpackage Structs
 */
class TRelationSearch extends AbstractStructBase
{
    /**
     * The CITY
     * @var string
     */
    public $CITY;
    /**
     * The CONTACT_ID
     * @var string
     */
    public $CONTACT_ID;
    /**
     * The COUNTRY_ID
     * @var string
     */
    public $COUNTRY_ID;
    /**
     * The EMAIL
     * @var string
     */
    public $EMAIL;
    /**
     * The FIRST_NAME
     * @var string
     */
    public $FIRST_NAME;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * The HOUSE_NO
     * @var string
     */
    public $HOUSE_NO;
    /**
     * The POSTCODE
     * @var string
     */
    public $POSTCODE;
    /**
     * The STREET
     * @var string
     */
    public $STREET;
    /**
     * The SURNAME_1
     * @var string
     */
    public $SURNAME_1;
    /**
     * The TELEPHONE
     * @var string
     */
    public $TELEPHONE;
    /**
     * Constructor method for TRelationSearch
     * @uses TRelationSearch::setCITY()
     * @uses TRelationSearch::setCONTACT_ID()
     * @uses TRelationSearch::setCOUNTRY_ID()
     * @uses TRelationSearch::setEMAIL()
     * @uses TRelationSearch::setFIRST_NAME()
     * @uses TRelationSearch::setFREE_X100_1()
     * @uses TRelationSearch::setFREE_X100_2()
     * @uses TRelationSearch::setFREE_X100_3()
     * @uses TRelationSearch::setFREE_X100_4()
     * @uses TRelationSearch::setFREE_X100_5()
     * @uses TRelationSearch::setHOUSE_NO()
     * @uses TRelationSearch::setPOSTCODE()
     * @uses TRelationSearch::setSTREET()
     * @uses TRelationSearch::setSURNAME_1()
     * @uses TRelationSearch::setTELEPHONE()
     * @param string $cITY
     * @param string $cONTACT_ID
     * @param string $cOUNTRY_ID
     * @param string $eMAIL
     * @param string $fIRST_NAME
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     * @param string $hOUSE_NO
     * @param string $pOSTCODE
     * @param string $sTREET
     * @param string $sURNAME_1
     * @param string $tELEPHONE
     */
    public function __construct($cITY = null, $cONTACT_ID = null, $cOUNTRY_ID = null, $eMAIL = null, $fIRST_NAME = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null, $hOUSE_NO = null, $pOSTCODE = null, $sTREET = null, $sURNAME_1 = null, $tELEPHONE = null)
    {
        $this
            ->setCITY($cITY)
            ->setCONTACT_ID($cONTACT_ID)
            ->setCOUNTRY_ID($cOUNTRY_ID)
            ->setEMAIL($eMAIL)
            ->setFIRST_NAME($fIRST_NAME)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5)
            ->setHOUSE_NO($hOUSE_NO)
            ->setPOSTCODE($pOSTCODE)
            ->setSTREET($sTREET)
            ->setSURNAME_1($sURNAME_1)
            ->setTELEPHONE($tELEPHONE);
    }
    /**
     * Get CITY value
     * @return string|null
     */
    public function getCITY()
    {
        return $this->CITY;
    }
    /**
     * Set CITY value
     * @param string $cITY
     * @return \StructType\TRelationSearch
     */
    public function setCITY($cITY = null)
    {
        // validation for constraint: string
        if (!is_null($cITY) && !is_string($cITY)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cITY)), __LINE__);
        }
        $this->CITY = $cITY;
        return $this;
    }
    /**
     * Get CONTACT_ID value
     * @return string|null
     */
    public function getCONTACT_ID()
    {
        return $this->CONTACT_ID;
    }
    /**
     * Set CONTACT_ID value
     * @param string $cONTACT_ID
     * @return \StructType\TRelationSearch
     */
    public function setCONTACT_ID($cONTACT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_ID) && !is_string($cONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_ID)), __LINE__);
        }
        $this->CONTACT_ID = $cONTACT_ID;
        return $this;
    }
    /**
     * Get COUNTRY_ID value
     * @return string|null
     */
    public function getCOUNTRY_ID()
    {
        return $this->COUNTRY_ID;
    }
    /**
     * Set COUNTRY_ID value
     * @param string $cOUNTRY_ID
     * @return \StructType\TRelationSearch
     */
    public function setCOUNTRY_ID($cOUNTRY_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cOUNTRY_ID) && !is_string($cOUNTRY_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOUNTRY_ID)), __LINE__);
        }
        $this->COUNTRY_ID = $cOUNTRY_ID;
        return $this;
    }
    /**
     * Get EMAIL value
     * @return string|null
     */
    public function getEMAIL()
    {
        return $this->EMAIL;
    }
    /**
     * Set EMAIL value
     * @param string $eMAIL
     * @return \StructType\TRelationSearch
     */
    public function setEMAIL($eMAIL = null)
    {
        // validation for constraint: string
        if (!is_null($eMAIL) && !is_string($eMAIL)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eMAIL)), __LINE__);
        }
        $this->EMAIL = $eMAIL;
        return $this;
    }
    /**
     * Get FIRST_NAME value
     * @return string|null
     */
    public function getFIRST_NAME()
    {
        return $this->FIRST_NAME;
    }
    /**
     * Set FIRST_NAME value
     * @param string $fIRST_NAME
     * @return \StructType\TRelationSearch
     */
    public function setFIRST_NAME($fIRST_NAME = null)
    {
        // validation for constraint: string
        if (!is_null($fIRST_NAME) && !is_string($fIRST_NAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fIRST_NAME)), __LINE__);
        }
        $this->FIRST_NAME = $fIRST_NAME;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TRelationSearch
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TRelationSearch
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TRelationSearch
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TRelationSearch
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TRelationSearch
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Get HOUSE_NO value
     * @return string|null
     */
    public function getHOUSE_NO()
    {
        return $this->HOUSE_NO;
    }
    /**
     * Set HOUSE_NO value
     * @param string $hOUSE_NO
     * @return \StructType\TRelationSearch
     */
    public function setHOUSE_NO($hOUSE_NO = null)
    {
        // validation for constraint: string
        if (!is_null($hOUSE_NO) && !is_string($hOUSE_NO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($hOUSE_NO)), __LINE__);
        }
        $this->HOUSE_NO = $hOUSE_NO;
        return $this;
    }
    /**
     * Get POSTCODE value
     * @return string|null
     */
    public function getPOSTCODE()
    {
        return $this->POSTCODE;
    }
    /**
     * Set POSTCODE value
     * @param string $pOSTCODE
     * @return \StructType\TRelationSearch
     */
    public function setPOSTCODE($pOSTCODE = null)
    {
        // validation for constraint: string
        if (!is_null($pOSTCODE) && !is_string($pOSTCODE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pOSTCODE)), __LINE__);
        }
        $this->POSTCODE = $pOSTCODE;
        return $this;
    }
    /**
     * Get STREET value
     * @return string|null
     */
    public function getSTREET()
    {
        return $this->STREET;
    }
    /**
     * Set STREET value
     * @param string $sTREET
     * @return \StructType\TRelationSearch
     */
    public function setSTREET($sTREET = null)
    {
        // validation for constraint: string
        if (!is_null($sTREET) && !is_string($sTREET)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sTREET)), __LINE__);
        }
        $this->STREET = $sTREET;
        return $this;
    }
    /**
     * Get SURNAME_1 value
     * @return string|null
     */
    public function getSURNAME_1()
    {
        return $this->SURNAME_1;
    }
    /**
     * Set SURNAME_1 value
     * @param string $sURNAME_1
     * @return \StructType\TRelationSearch
     */
    public function setSURNAME_1($sURNAME_1 = null)
    {
        // validation for constraint: string
        if (!is_null($sURNAME_1) && !is_string($sURNAME_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sURNAME_1)), __LINE__);
        }
        $this->SURNAME_1 = $sURNAME_1;
        return $this;
    }
    /**
     * Get TELEPHONE value
     * @return string|null
     */
    public function getTELEPHONE()
    {
        return $this->TELEPHONE;
    }
    /**
     * Set TELEPHONE value
     * @param string $tELEPHONE
     * @return \StructType\TRelationSearch
     */
    public function setTELEPHONE($tELEPHONE = null)
    {
        // validation for constraint: string
        if (!is_null($tELEPHONE) && !is_string($tELEPHONE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($tELEPHONE)), __LINE__);
        }
        $this->TELEPHONE = $tELEPHONE;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TRelationSearch
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
