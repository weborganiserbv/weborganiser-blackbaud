<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TNEXTORDERNO StructType
 * @subpackage Structs
 */
class TNEXTORDERNO extends AbstractStructBase
{
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The NEXTORHEADER_ID
     * @var int
     */
    public $NEXTORHEADER_ID;
    /**
     * Constructor method for TNEXTORDERNO
     * @uses TNEXTORDERNO::setERROR_ID()
     * @uses TNEXTORDERNO::setNEXTORHEADER_ID()
     * @param int $eRROR_ID
     * @param int $nEXTORHEADER_ID
     */
    public function __construct($eRROR_ID = null, $nEXTORHEADER_ID = null)
    {
        $this
            ->setERROR_ID($eRROR_ID)
            ->setNEXTORHEADER_ID($nEXTORHEADER_ID);
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TNEXTORDERNO
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get NEXTORHEADER_ID value
     * @return int|null
     */
    public function getNEXTORHEADER_ID()
    {
        return $this->NEXTORHEADER_ID;
    }
    /**
     * Set NEXTORHEADER_ID value
     * @param int $nEXTORHEADER_ID
     * @return \StructType\TNEXTORDERNO
     */
    public function setNEXTORHEADER_ID($nEXTORHEADER_ID = null)
    {
        // validation for constraint: int
        if (!is_null($nEXTORHEADER_ID) && !is_numeric($nEXTORHEADER_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($nEXTORHEADER_ID)), __LINE__);
        }
        $this->NEXTORHEADER_ID = $nEXTORHEADER_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TNEXTORDERNO
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
