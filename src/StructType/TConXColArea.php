<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TConXColArea StructType
 * @subpackage Structs
 */
class TConXColArea extends AbstractStructBase
{
    /**
     * The CONXCOLAREA_COLAREA_ID
     * @var string
     */
    public $CONXCOLAREA_COLAREA_ID;
    /**
     * The CONXCOLAREA_COLENDREASON_ID
     * @var string
     */
    public $CONXCOLAREA_COLENDREASON_ID;
    /**
     * The CONXCOLAREA_COLFUNCTION_ID
     * @var string
     */
    public $CONXCOLAREA_COLFUNCTION_ID;
    /**
     * The CONXCOLAREA_COLSOURCE_ID
     * @var string
     */
    public $CONXCOLAREA_COLSOURCE_ID;
    /**
     * The CONXCOLAREA_CONTACT_ID
     * @var int
     */
    public $CONXCOLAREA_CONTACT_ID;
    /**
     * The CONXCOLAREA_END_DATE
     * @var string
     */
    public $CONXCOLAREA_END_DATE;
    /**
     * The CONXCOLAREA_ID
     * @var int
     */
    public $CONXCOLAREA_ID;
    /**
     * The CONXCOLAREA_START_DATE
     * @var string
     */
    public $CONXCOLAREA_START_DATE;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * Constructor method for TConXColArea
     * @uses TConXColArea::setCONXCOLAREA_COLAREA_ID()
     * @uses TConXColArea::setCONXCOLAREA_COLENDREASON_ID()
     * @uses TConXColArea::setCONXCOLAREA_COLFUNCTION_ID()
     * @uses TConXColArea::setCONXCOLAREA_COLSOURCE_ID()
     * @uses TConXColArea::setCONXCOLAREA_CONTACT_ID()
     * @uses TConXColArea::setCONXCOLAREA_END_DATE()
     * @uses TConXColArea::setCONXCOLAREA_ID()
     * @uses TConXColArea::setCONXCOLAREA_START_DATE()
     * @uses TConXColArea::setFREE_X100_1()
     * @uses TConXColArea::setFREE_X100_2()
     * @uses TConXColArea::setFREE_X100_3()
     * @uses TConXColArea::setFREE_X100_4()
     * @uses TConXColArea::setFREE_X100_5()
     * @param string $cONXCOLAREA_COLAREA_ID
     * @param string $cONXCOLAREA_COLENDREASON_ID
     * @param string $cONXCOLAREA_COLFUNCTION_ID
     * @param string $cONXCOLAREA_COLSOURCE_ID
     * @param int $cONXCOLAREA_CONTACT_ID
     * @param string $cONXCOLAREA_END_DATE
     * @param int $cONXCOLAREA_ID
     * @param string $cONXCOLAREA_START_DATE
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     */
    public function __construct($cONXCOLAREA_COLAREA_ID = null, $cONXCOLAREA_COLENDREASON_ID = null, $cONXCOLAREA_COLFUNCTION_ID = null, $cONXCOLAREA_COLSOURCE_ID = null, $cONXCOLAREA_CONTACT_ID = null, $cONXCOLAREA_END_DATE = null, $cONXCOLAREA_ID = null, $cONXCOLAREA_START_DATE = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null)
    {
        $this
            ->setCONXCOLAREA_COLAREA_ID($cONXCOLAREA_COLAREA_ID)
            ->setCONXCOLAREA_COLENDREASON_ID($cONXCOLAREA_COLENDREASON_ID)
            ->setCONXCOLAREA_COLFUNCTION_ID($cONXCOLAREA_COLFUNCTION_ID)
            ->setCONXCOLAREA_COLSOURCE_ID($cONXCOLAREA_COLSOURCE_ID)
            ->setCONXCOLAREA_CONTACT_ID($cONXCOLAREA_CONTACT_ID)
            ->setCONXCOLAREA_END_DATE($cONXCOLAREA_END_DATE)
            ->setCONXCOLAREA_ID($cONXCOLAREA_ID)
            ->setCONXCOLAREA_START_DATE($cONXCOLAREA_START_DATE)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5);
    }
    /**
     * Get CONXCOLAREA_COLAREA_ID value
     * @return string|null
     */
    public function getCONXCOLAREA_COLAREA_ID()
    {
        return $this->CONXCOLAREA_COLAREA_ID;
    }
    /**
     * Set CONXCOLAREA_COLAREA_ID value
     * @param string $cONXCOLAREA_COLAREA_ID
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_COLAREA_ID($cONXCOLAREA_COLAREA_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cONXCOLAREA_COLAREA_ID) && !is_string($cONXCOLAREA_COLAREA_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONXCOLAREA_COLAREA_ID)), __LINE__);
        }
        $this->CONXCOLAREA_COLAREA_ID = $cONXCOLAREA_COLAREA_ID;
        return $this;
    }
    /**
     * Get CONXCOLAREA_COLENDREASON_ID value
     * @return string|null
     */
    public function getCONXCOLAREA_COLENDREASON_ID()
    {
        return $this->CONXCOLAREA_COLENDREASON_ID;
    }
    /**
     * Set CONXCOLAREA_COLENDREASON_ID value
     * @param string $cONXCOLAREA_COLENDREASON_ID
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_COLENDREASON_ID($cONXCOLAREA_COLENDREASON_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cONXCOLAREA_COLENDREASON_ID) && !is_string($cONXCOLAREA_COLENDREASON_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONXCOLAREA_COLENDREASON_ID)), __LINE__);
        }
        $this->CONXCOLAREA_COLENDREASON_ID = $cONXCOLAREA_COLENDREASON_ID;
        return $this;
    }
    /**
     * Get CONXCOLAREA_COLFUNCTION_ID value
     * @return string|null
     */
    public function getCONXCOLAREA_COLFUNCTION_ID()
    {
        return $this->CONXCOLAREA_COLFUNCTION_ID;
    }
    /**
     * Set CONXCOLAREA_COLFUNCTION_ID value
     * @param string $cONXCOLAREA_COLFUNCTION_ID
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_COLFUNCTION_ID($cONXCOLAREA_COLFUNCTION_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cONXCOLAREA_COLFUNCTION_ID) && !is_string($cONXCOLAREA_COLFUNCTION_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONXCOLAREA_COLFUNCTION_ID)), __LINE__);
        }
        $this->CONXCOLAREA_COLFUNCTION_ID = $cONXCOLAREA_COLFUNCTION_ID;
        return $this;
    }
    /**
     * Get CONXCOLAREA_COLSOURCE_ID value
     * @return string|null
     */
    public function getCONXCOLAREA_COLSOURCE_ID()
    {
        return $this->CONXCOLAREA_COLSOURCE_ID;
    }
    /**
     * Set CONXCOLAREA_COLSOURCE_ID value
     * @param string $cONXCOLAREA_COLSOURCE_ID
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_COLSOURCE_ID($cONXCOLAREA_COLSOURCE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cONXCOLAREA_COLSOURCE_ID) && !is_string($cONXCOLAREA_COLSOURCE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONXCOLAREA_COLSOURCE_ID)), __LINE__);
        }
        $this->CONXCOLAREA_COLSOURCE_ID = $cONXCOLAREA_COLSOURCE_ID;
        return $this;
    }
    /**
     * Get CONXCOLAREA_CONTACT_ID value
     * @return int|null
     */
    public function getCONXCOLAREA_CONTACT_ID()
    {
        return $this->CONXCOLAREA_CONTACT_ID;
    }
    /**
     * Set CONXCOLAREA_CONTACT_ID value
     * @param int $cONXCOLAREA_CONTACT_ID
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_CONTACT_ID($cONXCOLAREA_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($cONXCOLAREA_CONTACT_ID) && !is_numeric($cONXCOLAREA_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cONXCOLAREA_CONTACT_ID)), __LINE__);
        }
        $this->CONXCOLAREA_CONTACT_ID = $cONXCOLAREA_CONTACT_ID;
        return $this;
    }
    /**
     * Get CONXCOLAREA_END_DATE value
     * @return string|null
     */
    public function getCONXCOLAREA_END_DATE()
    {
        return $this->CONXCOLAREA_END_DATE;
    }
    /**
     * Set CONXCOLAREA_END_DATE value
     * @param string $cONXCOLAREA_END_DATE
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_END_DATE($cONXCOLAREA_END_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($cONXCOLAREA_END_DATE) && !is_string($cONXCOLAREA_END_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONXCOLAREA_END_DATE)), __LINE__);
        }
        $this->CONXCOLAREA_END_DATE = $cONXCOLAREA_END_DATE;
        return $this;
    }
    /**
     * Get CONXCOLAREA_ID value
     * @return int|null
     */
    public function getCONXCOLAREA_ID()
    {
        return $this->CONXCOLAREA_ID;
    }
    /**
     * Set CONXCOLAREA_ID value
     * @param int $cONXCOLAREA_ID
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_ID($cONXCOLAREA_ID = null)
    {
        // validation for constraint: int
        if (!is_null($cONXCOLAREA_ID) && !is_numeric($cONXCOLAREA_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cONXCOLAREA_ID)), __LINE__);
        }
        $this->CONXCOLAREA_ID = $cONXCOLAREA_ID;
        return $this;
    }
    /**
     * Get CONXCOLAREA_START_DATE value
     * @return string|null
     */
    public function getCONXCOLAREA_START_DATE()
    {
        return $this->CONXCOLAREA_START_DATE;
    }
    /**
     * Set CONXCOLAREA_START_DATE value
     * @param string $cONXCOLAREA_START_DATE
     * @return \StructType\TConXColArea
     */
    public function setCONXCOLAREA_START_DATE($cONXCOLAREA_START_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($cONXCOLAREA_START_DATE) && !is_string($cONXCOLAREA_START_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONXCOLAREA_START_DATE)), __LINE__);
        }
        $this->CONXCOLAREA_START_DATE = $cONXCOLAREA_START_DATE;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TConXColArea
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TConXColArea
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TConXColArea
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TConXColArea
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TConXColArea
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TConXColArea
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
