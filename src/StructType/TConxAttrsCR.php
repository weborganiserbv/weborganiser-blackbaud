<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TConxAttrsCR StructType
 * @subpackage Structs
 */
class TConxAttrsCR extends AbstractStructBase
{
    /**
     * The ConxAttrs
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TConxAttr[]
     * - ref: soapenc:arrayType
     * @var \StructType\TConxAttr[]
     */
    public $ConxAttrs;
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * Constructor method for TConxAttrsCR
     * @uses TConxAttrsCR::setConxAttrs()
     * @uses TConxAttrsCR::setERROR_ID()
     * @param \StructType\TConxAttr[] $conxAttrs
     * @param int $eRROR_ID
     */
    public function __construct(array $conxAttrs = array(), $eRROR_ID = null)
    {
        $this
            ->setConxAttrs($conxAttrs)
            ->setERROR_ID($eRROR_ID);
    }
    /**
     * Get ConxAttrs value
     * @return \StructType\TConxAttr[]|null
     */
    public function getConxAttrs()
    {
        return $this->ConxAttrs;
    }
    /**
     * Set ConxAttrs value
     * @throws \InvalidArgumentException
     * @param \StructType\TConxAttr[] $conxAttrs
     * @return \StructType\TConxAttrsCR
     */
    public function setConxAttrs(array $conxAttrs = array())
    {
        foreach ($conxAttrs as $tConxAttrsCRConxAttrsItem) {
            // validation for constraint: itemType
            if (!$tConxAttrsCRConxAttrsItem instanceof \StructType\TConxAttr) {
                throw new \InvalidArgumentException(sprintf('The ConxAttrs property can only contain items of \StructType\TConxAttr, "%s" given', is_object($tConxAttrsCRConxAttrsItem) ? get_class($tConxAttrsCRConxAttrsItem) : gettype($tConxAttrsCRConxAttrsItem)), __LINE__);
            }
        }
        $this->ConxAttrs = $conxAttrs;
        return $this;
    }
    /**
     * Add item to ConxAttrs value
     * @throws \InvalidArgumentException
     * @param \StructType\TConxAttr $item
     * @return \StructType\TConxAttrsCR
     */
    public function addToConxAttrs(\StructType\TConxAttr $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TConxAttr) {
            throw new \InvalidArgumentException(sprintf('The ConxAttrs property can only contain items of \StructType\TConxAttr, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->ConxAttrs[] = $item;
        return $this;
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TConxAttrsCR
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TConxAttrsCR
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
