<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TEVENT StructType
 * @subpackage Structs
 */
class TEVENT extends AbstractStructBase
{
    /**
     * The EVCONXROLE_CONTACT_ID
     * @var int
     */
    public $EVCONXROLE_CONTACT_ID;
    /**
     * The EVCONXROLE_EVENT_ID
     * @var string
     */
    public $EVCONXROLE_EVENT_ID;
    /**
     * The EVCONXROLE_EVROLE_ID
     * @var string
     */
    public $EVCONXROLE_EVROLE_ID;
    /**
     * The EVCONXROLE_EVSTAT_DATE
     * @var string
     */
    public $EVCONXROLE_EVSTAT_DATE;
    /**
     * The EVCONXROLE_EVSTAT_ID
     * @var string
     */
    public $EVCONXROLE_EVSTAT_ID;
    /**
     * The EVCONXROLE_ID
     * @var int
     */
    public $EVCONXROLE_ID;
    /**
     * The EVCONXROLE_PAYACCOUNT_ID
     * @var string
     */
    public $EVCONXROLE_PAYACCOUNT_ID;
    /**
     * The EVCONXROLE_SOURCE_ID
     * @var string
     */
    public $EVCONXROLE_SOURCE_ID;
    /**
     * The EVENT_MEMO
     * @var string
     */
    public $EVENT_MEMO;
    /**
     * The EVENT_NAME
     * @var string
     */
    public $EVENT_NAME;
    /**
     * The EVENT_QUANTITY
     * @var int
     */
    public $EVENT_QUANTITY;
    /**
     * The EVROLE_DESCR
     * @var string
     */
    public $EVROLE_DESCR;
    /**
     * The EVSTAT_DESCR
     * @var string
     */
    public $EVSTAT_DESCR;
    /**
     * Constructor method for TEVENT
     * @uses TEVENT::setEVCONXROLE_CONTACT_ID()
     * @uses TEVENT::setEVCONXROLE_EVENT_ID()
     * @uses TEVENT::setEVCONXROLE_EVROLE_ID()
     * @uses TEVENT::setEVCONXROLE_EVSTAT_DATE()
     * @uses TEVENT::setEVCONXROLE_EVSTAT_ID()
     * @uses TEVENT::setEVCONXROLE_ID()
     * @uses TEVENT::setEVCONXROLE_PAYACCOUNT_ID()
     * @uses TEVENT::setEVCONXROLE_SOURCE_ID()
     * @uses TEVENT::setEVENT_MEMO()
     * @uses TEVENT::setEVENT_NAME()
     * @uses TEVENT::setEVENT_QUANTITY()
     * @uses TEVENT::setEVROLE_DESCR()
     * @uses TEVENT::setEVSTAT_DESCR()
     * @param int $eVCONXROLE_CONTACT_ID
     * @param string $eVCONXROLE_EVENT_ID
     * @param string $eVCONXROLE_EVROLE_ID
     * @param string $eVCONXROLE_EVSTAT_DATE
     * @param string $eVCONXROLE_EVSTAT_ID
     * @param int $eVCONXROLE_ID
     * @param string $eVCONXROLE_PAYACCOUNT_ID
     * @param string $eVCONXROLE_SOURCE_ID
     * @param string $eVENT_MEMO
     * @param string $eVENT_NAME
     * @param int $eVENT_QUANTITY
     * @param string $eVROLE_DESCR
     * @param string $eVSTAT_DESCR
     */
    public function __construct($eVCONXROLE_CONTACT_ID = null, $eVCONXROLE_EVENT_ID = null, $eVCONXROLE_EVROLE_ID = null, $eVCONXROLE_EVSTAT_DATE = null, $eVCONXROLE_EVSTAT_ID = null, $eVCONXROLE_ID = null, $eVCONXROLE_PAYACCOUNT_ID = null, $eVCONXROLE_SOURCE_ID = null, $eVENT_MEMO = null, $eVENT_NAME = null, $eVENT_QUANTITY = null, $eVROLE_DESCR = null, $eVSTAT_DESCR = null)
    {
        $this
            ->setEVCONXROLE_CONTACT_ID($eVCONXROLE_CONTACT_ID)
            ->setEVCONXROLE_EVENT_ID($eVCONXROLE_EVENT_ID)
            ->setEVCONXROLE_EVROLE_ID($eVCONXROLE_EVROLE_ID)
            ->setEVCONXROLE_EVSTAT_DATE($eVCONXROLE_EVSTAT_DATE)
            ->setEVCONXROLE_EVSTAT_ID($eVCONXROLE_EVSTAT_ID)
            ->setEVCONXROLE_ID($eVCONXROLE_ID)
            ->setEVCONXROLE_PAYACCOUNT_ID($eVCONXROLE_PAYACCOUNT_ID)
            ->setEVCONXROLE_SOURCE_ID($eVCONXROLE_SOURCE_ID)
            ->setEVENT_MEMO($eVENT_MEMO)
            ->setEVENT_NAME($eVENT_NAME)
            ->setEVENT_QUANTITY($eVENT_QUANTITY)
            ->setEVROLE_DESCR($eVROLE_DESCR)
            ->setEVSTAT_DESCR($eVSTAT_DESCR);
    }
    /**
     * Get EVCONXROLE_CONTACT_ID value
     * @return int|null
     */
    public function getEVCONXROLE_CONTACT_ID()
    {
        return $this->EVCONXROLE_CONTACT_ID;
    }
    /**
     * Set EVCONXROLE_CONTACT_ID value
     * @param int $eVCONXROLE_CONTACT_ID
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_CONTACT_ID($eVCONXROLE_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eVCONXROLE_CONTACT_ID) && !is_numeric($eVCONXROLE_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eVCONXROLE_CONTACT_ID)), __LINE__);
        }
        $this->EVCONXROLE_CONTACT_ID = $eVCONXROLE_CONTACT_ID;
        return $this;
    }
    /**
     * Get EVCONXROLE_EVENT_ID value
     * @return string|null
     */
    public function getEVCONXROLE_EVENT_ID()
    {
        return $this->EVCONXROLE_EVENT_ID;
    }
    /**
     * Set EVCONXROLE_EVENT_ID value
     * @param string $eVCONXROLE_EVENT_ID
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_EVENT_ID($eVCONXROLE_EVENT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($eVCONXROLE_EVENT_ID) && !is_string($eVCONXROLE_EVENT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVCONXROLE_EVENT_ID)), __LINE__);
        }
        $this->EVCONXROLE_EVENT_ID = $eVCONXROLE_EVENT_ID;
        return $this;
    }
    /**
     * Get EVCONXROLE_EVROLE_ID value
     * @return string|null
     */
    public function getEVCONXROLE_EVROLE_ID()
    {
        return $this->EVCONXROLE_EVROLE_ID;
    }
    /**
     * Set EVCONXROLE_EVROLE_ID value
     * @param string $eVCONXROLE_EVROLE_ID
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_EVROLE_ID($eVCONXROLE_EVROLE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($eVCONXROLE_EVROLE_ID) && !is_string($eVCONXROLE_EVROLE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVCONXROLE_EVROLE_ID)), __LINE__);
        }
        $this->EVCONXROLE_EVROLE_ID = $eVCONXROLE_EVROLE_ID;
        return $this;
    }
    /**
     * Get EVCONXROLE_EVSTAT_DATE value
     * @return string|null
     */
    public function getEVCONXROLE_EVSTAT_DATE()
    {
        return $this->EVCONXROLE_EVSTAT_DATE;
    }
    /**
     * Set EVCONXROLE_EVSTAT_DATE value
     * @param string $eVCONXROLE_EVSTAT_DATE
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_EVSTAT_DATE($eVCONXROLE_EVSTAT_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($eVCONXROLE_EVSTAT_DATE) && !is_string($eVCONXROLE_EVSTAT_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVCONXROLE_EVSTAT_DATE)), __LINE__);
        }
        $this->EVCONXROLE_EVSTAT_DATE = $eVCONXROLE_EVSTAT_DATE;
        return $this;
    }
    /**
     * Get EVCONXROLE_EVSTAT_ID value
     * @return string|null
     */
    public function getEVCONXROLE_EVSTAT_ID()
    {
        return $this->EVCONXROLE_EVSTAT_ID;
    }
    /**
     * Set EVCONXROLE_EVSTAT_ID value
     * @param string $eVCONXROLE_EVSTAT_ID
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_EVSTAT_ID($eVCONXROLE_EVSTAT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($eVCONXROLE_EVSTAT_ID) && !is_string($eVCONXROLE_EVSTAT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVCONXROLE_EVSTAT_ID)), __LINE__);
        }
        $this->EVCONXROLE_EVSTAT_ID = $eVCONXROLE_EVSTAT_ID;
        return $this;
    }
    /**
     * Get EVCONXROLE_ID value
     * @return int|null
     */
    public function getEVCONXROLE_ID()
    {
        return $this->EVCONXROLE_ID;
    }
    /**
     * Set EVCONXROLE_ID value
     * @param int $eVCONXROLE_ID
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_ID($eVCONXROLE_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eVCONXROLE_ID) && !is_numeric($eVCONXROLE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eVCONXROLE_ID)), __LINE__);
        }
        $this->EVCONXROLE_ID = $eVCONXROLE_ID;
        return $this;
    }
    /**
     * Get EVCONXROLE_PAYACCOUNT_ID value
     * @return string|null
     */
    public function getEVCONXROLE_PAYACCOUNT_ID()
    {
        return $this->EVCONXROLE_PAYACCOUNT_ID;
    }
    /**
     * Set EVCONXROLE_PAYACCOUNT_ID value
     * @param string $eVCONXROLE_PAYACCOUNT_ID
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_PAYACCOUNT_ID($eVCONXROLE_PAYACCOUNT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($eVCONXROLE_PAYACCOUNT_ID) && !is_string($eVCONXROLE_PAYACCOUNT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVCONXROLE_PAYACCOUNT_ID)), __LINE__);
        }
        $this->EVCONXROLE_PAYACCOUNT_ID = $eVCONXROLE_PAYACCOUNT_ID;
        return $this;
    }
    /**
     * Get EVCONXROLE_SOURCE_ID value
     * @return string|null
     */
    public function getEVCONXROLE_SOURCE_ID()
    {
        return $this->EVCONXROLE_SOURCE_ID;
    }
    /**
     * Set EVCONXROLE_SOURCE_ID value
     * @param string $eVCONXROLE_SOURCE_ID
     * @return \StructType\TEVENT
     */
    public function setEVCONXROLE_SOURCE_ID($eVCONXROLE_SOURCE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($eVCONXROLE_SOURCE_ID) && !is_string($eVCONXROLE_SOURCE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVCONXROLE_SOURCE_ID)), __LINE__);
        }
        $this->EVCONXROLE_SOURCE_ID = $eVCONXROLE_SOURCE_ID;
        return $this;
    }
    /**
     * Get EVENT_MEMO value
     * @return string|null
     */
    public function getEVENT_MEMO()
    {
        return $this->EVENT_MEMO;
    }
    /**
     * Set EVENT_MEMO value
     * @param string $eVENT_MEMO
     * @return \StructType\TEVENT
     */
    public function setEVENT_MEMO($eVENT_MEMO = null)
    {
        // validation for constraint: string
        if (!is_null($eVENT_MEMO) && !is_string($eVENT_MEMO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVENT_MEMO)), __LINE__);
        }
        $this->EVENT_MEMO = $eVENT_MEMO;
        return $this;
    }
    /**
     * Get EVENT_NAME value
     * @return string|null
     */
    public function getEVENT_NAME()
    {
        return $this->EVENT_NAME;
    }
    /**
     * Set EVENT_NAME value
     * @param string $eVENT_NAME
     * @return \StructType\TEVENT
     */
    public function setEVENT_NAME($eVENT_NAME = null)
    {
        // validation for constraint: string
        if (!is_null($eVENT_NAME) && !is_string($eVENT_NAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVENT_NAME)), __LINE__);
        }
        $this->EVENT_NAME = $eVENT_NAME;
        return $this;
    }
    /**
     * Get EVENT_QUANTITY value
     * @return int|null
     */
    public function getEVENT_QUANTITY()
    {
        return $this->EVENT_QUANTITY;
    }
    /**
     * Set EVENT_QUANTITY value
     * @param int $eVENT_QUANTITY
     * @return \StructType\TEVENT
     */
    public function setEVENT_QUANTITY($eVENT_QUANTITY = null)
    {
        // validation for constraint: int
        if (!is_null($eVENT_QUANTITY) && !is_numeric($eVENT_QUANTITY)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eVENT_QUANTITY)), __LINE__);
        }
        $this->EVENT_QUANTITY = $eVENT_QUANTITY;
        return $this;
    }
    /**
     * Get EVROLE_DESCR value
     * @return string|null
     */
    public function getEVROLE_DESCR()
    {
        return $this->EVROLE_DESCR;
    }
    /**
     * Set EVROLE_DESCR value
     * @param string $eVROLE_DESCR
     * @return \StructType\TEVENT
     */
    public function setEVROLE_DESCR($eVROLE_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($eVROLE_DESCR) && !is_string($eVROLE_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVROLE_DESCR)), __LINE__);
        }
        $this->EVROLE_DESCR = $eVROLE_DESCR;
        return $this;
    }
    /**
     * Get EVSTAT_DESCR value
     * @return string|null
     */
    public function getEVSTAT_DESCR()
    {
        return $this->EVSTAT_DESCR;
    }
    /**
     * Set EVSTAT_DESCR value
     * @param string $eVSTAT_DESCR
     * @return \StructType\TEVENT
     */
    public function setEVSTAT_DESCR($eVSTAT_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($eVSTAT_DESCR) && !is_string($eVSTAT_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($eVSTAT_DESCR)), __LINE__);
        }
        $this->EVSTAT_DESCR = $eVSTAT_DESCR;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TEVENT
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
