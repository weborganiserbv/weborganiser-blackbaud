<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TArticle StructType
 * @subpackage Structs
 */
class TArticle extends AbstractStructBase
{
    /**
     * The ARTICLE_ACTIVE_FLAG
     * @var string
     */
    public $ARTICLE_ACTIVE_FLAG;
    /**
     * The ARTICLE_ACTIVE_FROM_DATE
     * @var string
     */
    public $ARTICLE_ACTIVE_FROM_DATE;
    /**
     * The ARTICLE_ACTIVE_TO_DATE
     * @var string
     */
    public $ARTICLE_ACTIVE_TO_DATE;
    /**
     * The ARTICLE_ARTGROUP_ID
     * @var string
     */
    public $ARTICLE_ARTGROUP_ID;
    /**
     * The ARTICLE_ARTKIT_ID
     * @var string
     */
    public $ARTICLE_ARTKIT_ID;
    /**
     * The ARTICLE_BLOCK_PDEL_FLAG
     * @var string
     */
    public $ARTICLE_BLOCK_PDEL_FLAG;
    /**
     * The ARTICLE_DESCR
     * @var string
     */
    public $ARTICLE_DESCR;
    /**
     * The ARTICLE_ID
     * @var string
     */
    public $ARTICLE_ID;
    /**
     * The ARTICLE_NUMBER_EXTERN
     * @var string
     */
    public $ARTICLE_NUMBER_EXTERN;
    /**
     * The ARTICLE_PRICE_0
     * @var float
     */
    public $ARTICLE_PRICE_0;
    /**
     * The ARTICLE_PRICE_1
     * @var float
     */
    public $ARTICLE_PRICE_1;
    /**
     * The ARTICLE_VAT_ID
     * @var int
     */
    public $ARTICLE_VAT_ID;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * Constructor method for TArticle
     * @uses TArticle::setARTICLE_ACTIVE_FLAG()
     * @uses TArticle::setARTICLE_ACTIVE_FROM_DATE()
     * @uses TArticle::setARTICLE_ACTIVE_TO_DATE()
     * @uses TArticle::setARTICLE_ARTGROUP_ID()
     * @uses TArticle::setARTICLE_ARTKIT_ID()
     * @uses TArticle::setARTICLE_BLOCK_PDEL_FLAG()
     * @uses TArticle::setARTICLE_DESCR()
     * @uses TArticle::setARTICLE_ID()
     * @uses TArticle::setARTICLE_NUMBER_EXTERN()
     * @uses TArticle::setARTICLE_PRICE_0()
     * @uses TArticle::setARTICLE_PRICE_1()
     * @uses TArticle::setARTICLE_VAT_ID()
     * @uses TArticle::setFREE_X100_1()
     * @uses TArticle::setFREE_X100_2()
     * @uses TArticle::setFREE_X100_3()
     * @uses TArticle::setFREE_X100_4()
     * @uses TArticle::setFREE_X100_5()
     * @param string $aRTICLE_ACTIVE_FLAG
     * @param string $aRTICLE_ACTIVE_FROM_DATE
     * @param string $aRTICLE_ACTIVE_TO_DATE
     * @param string $aRTICLE_ARTGROUP_ID
     * @param string $aRTICLE_ARTKIT_ID
     * @param string $aRTICLE_BLOCK_PDEL_FLAG
     * @param string $aRTICLE_DESCR
     * @param string $aRTICLE_ID
     * @param string $aRTICLE_NUMBER_EXTERN
     * @param float $aRTICLE_PRICE_0
     * @param float $aRTICLE_PRICE_1
     * @param int $aRTICLE_VAT_ID
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     */
    public function __construct($aRTICLE_ACTIVE_FLAG = null, $aRTICLE_ACTIVE_FROM_DATE = null, $aRTICLE_ACTIVE_TO_DATE = null, $aRTICLE_ARTGROUP_ID = null, $aRTICLE_ARTKIT_ID = null, $aRTICLE_BLOCK_PDEL_FLAG = null, $aRTICLE_DESCR = null, $aRTICLE_ID = null, $aRTICLE_NUMBER_EXTERN = null, $aRTICLE_PRICE_0 = null, $aRTICLE_PRICE_1 = null, $aRTICLE_VAT_ID = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null)
    {
        $this
            ->setARTICLE_ACTIVE_FLAG($aRTICLE_ACTIVE_FLAG)
            ->setARTICLE_ACTIVE_FROM_DATE($aRTICLE_ACTIVE_FROM_DATE)
            ->setARTICLE_ACTIVE_TO_DATE($aRTICLE_ACTIVE_TO_DATE)
            ->setARTICLE_ARTGROUP_ID($aRTICLE_ARTGROUP_ID)
            ->setARTICLE_ARTKIT_ID($aRTICLE_ARTKIT_ID)
            ->setARTICLE_BLOCK_PDEL_FLAG($aRTICLE_BLOCK_PDEL_FLAG)
            ->setARTICLE_DESCR($aRTICLE_DESCR)
            ->setARTICLE_ID($aRTICLE_ID)
            ->setARTICLE_NUMBER_EXTERN($aRTICLE_NUMBER_EXTERN)
            ->setARTICLE_PRICE_0($aRTICLE_PRICE_0)
            ->setARTICLE_PRICE_1($aRTICLE_PRICE_1)
            ->setARTICLE_VAT_ID($aRTICLE_VAT_ID)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5);
    }
    /**
     * Get ARTICLE_ACTIVE_FLAG value
     * @return string|null
     */
    public function getARTICLE_ACTIVE_FLAG()
    {
        return $this->ARTICLE_ACTIVE_FLAG;
    }
    /**
     * Set ARTICLE_ACTIVE_FLAG value
     * @param string $aRTICLE_ACTIVE_FLAG
     * @return \StructType\TArticle
     */
    public function setARTICLE_ACTIVE_FLAG($aRTICLE_ACTIVE_FLAG = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_ACTIVE_FLAG) && !is_string($aRTICLE_ACTIVE_FLAG)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_ACTIVE_FLAG)), __LINE__);
        }
        $this->ARTICLE_ACTIVE_FLAG = $aRTICLE_ACTIVE_FLAG;
        return $this;
    }
    /**
     * Get ARTICLE_ACTIVE_FROM_DATE value
     * @return string|null
     */
    public function getARTICLE_ACTIVE_FROM_DATE()
    {
        return $this->ARTICLE_ACTIVE_FROM_DATE;
    }
    /**
     * Set ARTICLE_ACTIVE_FROM_DATE value
     * @param string $aRTICLE_ACTIVE_FROM_DATE
     * @return \StructType\TArticle
     */
    public function setARTICLE_ACTIVE_FROM_DATE($aRTICLE_ACTIVE_FROM_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_ACTIVE_FROM_DATE) && !is_string($aRTICLE_ACTIVE_FROM_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_ACTIVE_FROM_DATE)), __LINE__);
        }
        $this->ARTICLE_ACTIVE_FROM_DATE = $aRTICLE_ACTIVE_FROM_DATE;
        return $this;
    }
    /**
     * Get ARTICLE_ACTIVE_TO_DATE value
     * @return string|null
     */
    public function getARTICLE_ACTIVE_TO_DATE()
    {
        return $this->ARTICLE_ACTIVE_TO_DATE;
    }
    /**
     * Set ARTICLE_ACTIVE_TO_DATE value
     * @param string $aRTICLE_ACTIVE_TO_DATE
     * @return \StructType\TArticle
     */
    public function setARTICLE_ACTIVE_TO_DATE($aRTICLE_ACTIVE_TO_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_ACTIVE_TO_DATE) && !is_string($aRTICLE_ACTIVE_TO_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_ACTIVE_TO_DATE)), __LINE__);
        }
        $this->ARTICLE_ACTIVE_TO_DATE = $aRTICLE_ACTIVE_TO_DATE;
        return $this;
    }
    /**
     * Get ARTICLE_ARTGROUP_ID value
     * @return string|null
     */
    public function getARTICLE_ARTGROUP_ID()
    {
        return $this->ARTICLE_ARTGROUP_ID;
    }
    /**
     * Set ARTICLE_ARTGROUP_ID value
     * @param string $aRTICLE_ARTGROUP_ID
     * @return \StructType\TArticle
     */
    public function setARTICLE_ARTGROUP_ID($aRTICLE_ARTGROUP_ID = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_ARTGROUP_ID) && !is_string($aRTICLE_ARTGROUP_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_ARTGROUP_ID)), __LINE__);
        }
        $this->ARTICLE_ARTGROUP_ID = $aRTICLE_ARTGROUP_ID;
        return $this;
    }
    /**
     * Get ARTICLE_ARTKIT_ID value
     * @return string|null
     */
    public function getARTICLE_ARTKIT_ID()
    {
        return $this->ARTICLE_ARTKIT_ID;
    }
    /**
     * Set ARTICLE_ARTKIT_ID value
     * @param string $aRTICLE_ARTKIT_ID
     * @return \StructType\TArticle
     */
    public function setARTICLE_ARTKIT_ID($aRTICLE_ARTKIT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_ARTKIT_ID) && !is_string($aRTICLE_ARTKIT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_ARTKIT_ID)), __LINE__);
        }
        $this->ARTICLE_ARTKIT_ID = $aRTICLE_ARTKIT_ID;
        return $this;
    }
    /**
     * Get ARTICLE_BLOCK_PDEL_FLAG value
     * @return string|null
     */
    public function getARTICLE_BLOCK_PDEL_FLAG()
    {
        return $this->ARTICLE_BLOCK_PDEL_FLAG;
    }
    /**
     * Set ARTICLE_BLOCK_PDEL_FLAG value
     * @param string $aRTICLE_BLOCK_PDEL_FLAG
     * @return \StructType\TArticle
     */
    public function setARTICLE_BLOCK_PDEL_FLAG($aRTICLE_BLOCK_PDEL_FLAG = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_BLOCK_PDEL_FLAG) && !is_string($aRTICLE_BLOCK_PDEL_FLAG)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_BLOCK_PDEL_FLAG)), __LINE__);
        }
        $this->ARTICLE_BLOCK_PDEL_FLAG = $aRTICLE_BLOCK_PDEL_FLAG;
        return $this;
    }
    /**
     * Get ARTICLE_DESCR value
     * @return string|null
     */
    public function getARTICLE_DESCR()
    {
        return $this->ARTICLE_DESCR;
    }
    /**
     * Set ARTICLE_DESCR value
     * @param string $aRTICLE_DESCR
     * @return \StructType\TArticle
     */
    public function setARTICLE_DESCR($aRTICLE_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_DESCR) && !is_string($aRTICLE_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_DESCR)), __LINE__);
        }
        $this->ARTICLE_DESCR = $aRTICLE_DESCR;
        return $this;
    }
    /**
     * Get ARTICLE_ID value
     * @return string|null
     */
    public function getARTICLE_ID()
    {
        return $this->ARTICLE_ID;
    }
    /**
     * Set ARTICLE_ID value
     * @param string $aRTICLE_ID
     * @return \StructType\TArticle
     */
    public function setARTICLE_ID($aRTICLE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_ID) && !is_string($aRTICLE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_ID)), __LINE__);
        }
        $this->ARTICLE_ID = $aRTICLE_ID;
        return $this;
    }
    /**
     * Get ARTICLE_NUMBER_EXTERN value
     * @return string|null
     */
    public function getARTICLE_NUMBER_EXTERN()
    {
        return $this->ARTICLE_NUMBER_EXTERN;
    }
    /**
     * Set ARTICLE_NUMBER_EXTERN value
     * @param string $aRTICLE_NUMBER_EXTERN
     * @return \StructType\TArticle
     */
    public function setARTICLE_NUMBER_EXTERN($aRTICLE_NUMBER_EXTERN = null)
    {
        // validation for constraint: string
        if (!is_null($aRTICLE_NUMBER_EXTERN) && !is_string($aRTICLE_NUMBER_EXTERN)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aRTICLE_NUMBER_EXTERN)), __LINE__);
        }
        $this->ARTICLE_NUMBER_EXTERN = $aRTICLE_NUMBER_EXTERN;
        return $this;
    }
    /**
     * Get ARTICLE_PRICE_0 value
     * @return float|null
     */
    public function getARTICLE_PRICE_0()
    {
        return $this->ARTICLE_PRICE_0;
    }
    /**
     * Set ARTICLE_PRICE_0 value
     * @param float $aRTICLE_PRICE_0
     * @return \StructType\TArticle
     */
    public function setARTICLE_PRICE_0($aRTICLE_PRICE_0 = null)
    {
        $this->ARTICLE_PRICE_0 = $aRTICLE_PRICE_0;
        return $this;
    }
    /**
     * Get ARTICLE_PRICE_1 value
     * @return float|null
     */
    public function getARTICLE_PRICE_1()
    {
        return $this->ARTICLE_PRICE_1;
    }
    /**
     * Set ARTICLE_PRICE_1 value
     * @param float $aRTICLE_PRICE_1
     * @return \StructType\TArticle
     */
    public function setARTICLE_PRICE_1($aRTICLE_PRICE_1 = null)
    {
        $this->ARTICLE_PRICE_1 = $aRTICLE_PRICE_1;
        return $this;
    }
    /**
     * Get ARTICLE_VAT_ID value
     * @return int|null
     */
    public function getARTICLE_VAT_ID()
    {
        return $this->ARTICLE_VAT_ID;
    }
    /**
     * Set ARTICLE_VAT_ID value
     * @param int $aRTICLE_VAT_ID
     * @return \StructType\TArticle
     */
    public function setARTICLE_VAT_ID($aRTICLE_VAT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($aRTICLE_VAT_ID) && !is_numeric($aRTICLE_VAT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($aRTICLE_VAT_ID)), __LINE__);
        }
        $this->ARTICLE_VAT_ID = $aRTICLE_VAT_ID;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TArticle
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TArticle
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TArticle
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TArticle
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TArticle
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TArticle
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
