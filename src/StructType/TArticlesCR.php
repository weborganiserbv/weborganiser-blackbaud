<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TArticlesCR StructType
 * @subpackage Structs
 */
class TArticlesCR extends AbstractStructBase
{
    /**
     * The ARTICLES
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TArticle[]
     * - ref: soapenc:arrayType
     * @var \StructType\TArticle[]
     */
    public $ARTICLES;
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * Constructor method for TArticlesCR
     * @uses TArticlesCR::setARTICLES()
     * @uses TArticlesCR::setERROR_ID()
     * @param \StructType\TArticle[] $aRTICLES
     * @param int $eRROR_ID
     */
    public function __construct(array $aRTICLES = array(), $eRROR_ID = null)
    {
        $this
            ->setARTICLES($aRTICLES)
            ->setERROR_ID($eRROR_ID);
    }
    /**
     * Get ARTICLES value
     * @return \StructType\TArticle[]|null
     */
    public function getARTICLES()
    {
        return $this->ARTICLES;
    }
    /**
     * Set ARTICLES value
     * @throws \InvalidArgumentException
     * @param \StructType\TArticle[] $aRTICLES
     * @return \StructType\TArticlesCR
     */
    public function setARTICLES(array $aRTICLES = array())
    {
        foreach ($aRTICLES as $tArticlesCRARTICLESItem) {
            // validation for constraint: itemType
            if (!$tArticlesCRARTICLESItem instanceof \StructType\TArticle) {
                throw new \InvalidArgumentException(sprintf('The ARTICLES property can only contain items of \StructType\TArticle, "%s" given', is_object($tArticlesCRARTICLESItem) ? get_class($tArticlesCRARTICLESItem) : gettype($tArticlesCRARTICLESItem)), __LINE__);
            }
        }
        $this->ARTICLES = $aRTICLES;
        return $this;
    }
    /**
     * Add item to ARTICLES value
     * @throws \InvalidArgumentException
     * @param \StructType\TArticle $item
     * @return \StructType\TArticlesCR
     */
    public function addToARTICLES(\StructType\TArticle $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TArticle) {
            throw new \InvalidArgumentException(sprintf('The ARTICLES property can only contain items of \StructType\TArticle, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->ARTICLES[] = $item;
        return $this;
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TArticlesCR
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TArticlesCR
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
