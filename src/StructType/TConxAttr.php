<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TConxAttr StructType
 * @subpackage Structs
 */
class TConxAttr extends AbstractStructBase
{
    /**
     * The CONXATTR_ATTRIBUTE_ID
     * @var string
     */
    public $CONXATTR_ATTRIBUTE_ID;
    /**
     * The CONXATTR_CONTACT_ID
     * @var int
     */
    public $CONXATTR_CONTACT_ID;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * Constructor method for TConxAttr
     * @uses TConxAttr::setCONXATTR_ATTRIBUTE_ID()
     * @uses TConxAttr::setCONXATTR_CONTACT_ID()
     * @uses TConxAttr::setFREE_X100_1()
     * @uses TConxAttr::setFREE_X100_2()
     * @uses TConxAttr::setFREE_X100_3()
     * @uses TConxAttr::setFREE_X100_4()
     * @uses TConxAttr::setFREE_X100_5()
     * @param string $cONXATTR_ATTRIBUTE_ID
     * @param int $cONXATTR_CONTACT_ID
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     */
    public function __construct($cONXATTR_ATTRIBUTE_ID = null, $cONXATTR_CONTACT_ID = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null)
    {
        $this
            ->setCONXATTR_ATTRIBUTE_ID($cONXATTR_ATTRIBUTE_ID)
            ->setCONXATTR_CONTACT_ID($cONXATTR_CONTACT_ID)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5);
    }
    /**
     * Get CONXATTR_ATTRIBUTE_ID value
     * @return string|null
     */
    public function getCONXATTR_ATTRIBUTE_ID()
    {
        return $this->CONXATTR_ATTRIBUTE_ID;
    }
    /**
     * Set CONXATTR_ATTRIBUTE_ID value
     * @param string $cONXATTR_ATTRIBUTE_ID
     * @return \StructType\TConxAttr
     */
    public function setCONXATTR_ATTRIBUTE_ID($cONXATTR_ATTRIBUTE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cONXATTR_ATTRIBUTE_ID) && !is_string($cONXATTR_ATTRIBUTE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONXATTR_ATTRIBUTE_ID)), __LINE__);
        }
        $this->CONXATTR_ATTRIBUTE_ID = $cONXATTR_ATTRIBUTE_ID;
        return $this;
    }
    /**
     * Get CONXATTR_CONTACT_ID value
     * @return int|null
     */
    public function getCONXATTR_CONTACT_ID()
    {
        return $this->CONXATTR_CONTACT_ID;
    }
    /**
     * Set CONXATTR_CONTACT_ID value
     * @param int $cONXATTR_CONTACT_ID
     * @return \StructType\TConxAttr
     */
    public function setCONXATTR_CONTACT_ID($cONXATTR_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($cONXATTR_CONTACT_ID) && !is_numeric($cONXATTR_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cONXATTR_CONTACT_ID)), __LINE__);
        }
        $this->CONXATTR_CONTACT_ID = $cONXATTR_CONTACT_ID;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TConxAttr
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TConxAttr
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TConxAttr
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TConxAttr
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TConxAttr
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TConxAttr
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
