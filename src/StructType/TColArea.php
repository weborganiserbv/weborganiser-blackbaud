<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TColArea StructType
 * @subpackage Structs
 */
class TColArea extends AbstractStructBase
{
    /**
     * The COLAREA_ACTIVE_FLAG
     * @var string
     */
    public $COLAREA_ACTIVE_FLAG;
    /**
     * The COLAREA_CITY
     * @var string
     */
    public $COLAREA_CITY;
    /**
     * The COLAREA_COLENDREASON_ID
     * @var string
     */
    public $COLAREA_COLENDREASON_ID;
    /**
     * The COLAREA_COLLECTANTS_WISH
     * @var int
     */
    public $COLAREA_COLLECTANTS_WISH;
    /**
     * The COLAREA_COLREGION_DESCR
     * @var string
     */
    public $COLAREA_COLREGION_DESCR;
    /**
     * The COLAREA_COUNTY_RELATION_ID
     * @var int
     */
    public $COLAREA_COUNTY_RELATION_ID;
    /**
     * The COLAREA_COUNTY_RELATION_NAME
     * @var string
     */
    public $COLAREA_COUNTY_RELATION_NAME;
    /**
     * The COLAREA_DESCR
     * @var string
     */
    public $COLAREA_DESCR;
    /**
     * The COLAREA_END_DATE
     * @var string
     */
    public $COLAREA_END_DATE;
    /**
     * The COLAREA_HOUSES_LAST_YEAR
     * @var int
     */
    public $COLAREA_HOUSES_LAST_YEAR;
    /**
     * The COLAREA_HOUSES_THIS_YEAR
     * @var int
     */
    public $COLAREA_HOUSES_THIS_YEAR;
    /**
     * The COLAREA_ID
     * @var string
     */
    public $COLAREA_ID;
    /**
     * The COLAREA_MEMO
     * @var string
     */
    public $COLAREA_MEMO;
    /**
     * The COLAREA_ORG_COLSOURCE_ID
     * @var string
     */
    public $COLAREA_ORG_COLSOURCE_ID;
    /**
     * The COLAREA_ORG_CONTACT_ID
     * @var int
     */
    public $COLAREA_ORG_CONTACT_ID;
    /**
     * The COLAREA_ORG_CONTACT_ID_DESCR
     * @var string
     */
    public $COLAREA_ORG_CONTACT_ID_DESCR;
    /**
     * The COLAREA_ORG_END_DATE
     * @var string
     */
    public $COLAREA_ORG_END_DATE;
    /**
     * The COLAREA_ORG_START_DATE
     * @var string
     */
    public $COLAREA_ORG_START_DATE;
    /**
     * The COLAREA_START_DATE
     * @var string
     */
    public $COLAREA_START_DATE;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * Constructor method for TColArea
     * @uses TColArea::setCOLAREA_ACTIVE_FLAG()
     * @uses TColArea::setCOLAREA_CITY()
     * @uses TColArea::setCOLAREA_COLENDREASON_ID()
     * @uses TColArea::setCOLAREA_COLLECTANTS_WISH()
     * @uses TColArea::setCOLAREA_COLREGION_DESCR()
     * @uses TColArea::setCOLAREA_COUNTY_RELATION_ID()
     * @uses TColArea::setCOLAREA_COUNTY_RELATION_NAME()
     * @uses TColArea::setCOLAREA_DESCR()
     * @uses TColArea::setCOLAREA_END_DATE()
     * @uses TColArea::setCOLAREA_HOUSES_LAST_YEAR()
     * @uses TColArea::setCOLAREA_HOUSES_THIS_YEAR()
     * @uses TColArea::setCOLAREA_ID()
     * @uses TColArea::setCOLAREA_MEMO()
     * @uses TColArea::setCOLAREA_ORG_COLSOURCE_ID()
     * @uses TColArea::setCOLAREA_ORG_CONTACT_ID()
     * @uses TColArea::setCOLAREA_ORG_CONTACT_ID_DESCR()
     * @uses TColArea::setCOLAREA_ORG_END_DATE()
     * @uses TColArea::setCOLAREA_ORG_START_DATE()
     * @uses TColArea::setCOLAREA_START_DATE()
     * @uses TColArea::setFREE_X100_1()
     * @uses TColArea::setFREE_X100_2()
     * @uses TColArea::setFREE_X100_3()
     * @uses TColArea::setFREE_X100_4()
     * @uses TColArea::setFREE_X100_5()
     * @param string $cOLAREA_ACTIVE_FLAG
     * @param string $cOLAREA_CITY
     * @param string $cOLAREA_COLENDREASON_ID
     * @param int $cOLAREA_COLLECTANTS_WISH
     * @param string $cOLAREA_COLREGION_DESCR
     * @param int $cOLAREA_COUNTY_RELATION_ID
     * @param string $cOLAREA_COUNTY_RELATION_NAME
     * @param string $cOLAREA_DESCR
     * @param string $cOLAREA_END_DATE
     * @param int $cOLAREA_HOUSES_LAST_YEAR
     * @param int $cOLAREA_HOUSES_THIS_YEAR
     * @param string $cOLAREA_ID
     * @param string $cOLAREA_MEMO
     * @param string $cOLAREA_ORG_COLSOURCE_ID
     * @param int $cOLAREA_ORG_CONTACT_ID
     * @param string $cOLAREA_ORG_CONTACT_ID_DESCR
     * @param string $cOLAREA_ORG_END_DATE
     * @param string $cOLAREA_ORG_START_DATE
     * @param string $cOLAREA_START_DATE
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     */
    public function __construct($cOLAREA_ACTIVE_FLAG = null, $cOLAREA_CITY = null, $cOLAREA_COLENDREASON_ID = null, $cOLAREA_COLLECTANTS_WISH = null, $cOLAREA_COLREGION_DESCR = null, $cOLAREA_COUNTY_RELATION_ID = null, $cOLAREA_COUNTY_RELATION_NAME = null, $cOLAREA_DESCR = null, $cOLAREA_END_DATE = null, $cOLAREA_HOUSES_LAST_YEAR = null, $cOLAREA_HOUSES_THIS_YEAR = null, $cOLAREA_ID = null, $cOLAREA_MEMO = null, $cOLAREA_ORG_COLSOURCE_ID = null, $cOLAREA_ORG_CONTACT_ID = null, $cOLAREA_ORG_CONTACT_ID_DESCR = null, $cOLAREA_ORG_END_DATE = null, $cOLAREA_ORG_START_DATE = null, $cOLAREA_START_DATE = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null)
    {
        $this
            ->setCOLAREA_ACTIVE_FLAG($cOLAREA_ACTIVE_FLAG)
            ->setCOLAREA_CITY($cOLAREA_CITY)
            ->setCOLAREA_COLENDREASON_ID($cOLAREA_COLENDREASON_ID)
            ->setCOLAREA_COLLECTANTS_WISH($cOLAREA_COLLECTANTS_WISH)
            ->setCOLAREA_COLREGION_DESCR($cOLAREA_COLREGION_DESCR)
            ->setCOLAREA_COUNTY_RELATION_ID($cOLAREA_COUNTY_RELATION_ID)
            ->setCOLAREA_COUNTY_RELATION_NAME($cOLAREA_COUNTY_RELATION_NAME)
            ->setCOLAREA_DESCR($cOLAREA_DESCR)
            ->setCOLAREA_END_DATE($cOLAREA_END_DATE)
            ->setCOLAREA_HOUSES_LAST_YEAR($cOLAREA_HOUSES_LAST_YEAR)
            ->setCOLAREA_HOUSES_THIS_YEAR($cOLAREA_HOUSES_THIS_YEAR)
            ->setCOLAREA_ID($cOLAREA_ID)
            ->setCOLAREA_MEMO($cOLAREA_MEMO)
            ->setCOLAREA_ORG_COLSOURCE_ID($cOLAREA_ORG_COLSOURCE_ID)
            ->setCOLAREA_ORG_CONTACT_ID($cOLAREA_ORG_CONTACT_ID)
            ->setCOLAREA_ORG_CONTACT_ID_DESCR($cOLAREA_ORG_CONTACT_ID_DESCR)
            ->setCOLAREA_ORG_END_DATE($cOLAREA_ORG_END_DATE)
            ->setCOLAREA_ORG_START_DATE($cOLAREA_ORG_START_DATE)
            ->setCOLAREA_START_DATE($cOLAREA_START_DATE)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5);
    }
    /**
     * Get COLAREA_ACTIVE_FLAG value
     * @return string|null
     */
    public function getCOLAREA_ACTIVE_FLAG()
    {
        return $this->COLAREA_ACTIVE_FLAG;
    }
    /**
     * Set COLAREA_ACTIVE_FLAG value
     * @param string $cOLAREA_ACTIVE_FLAG
     * @return \StructType\TColArea
     */
    public function setCOLAREA_ACTIVE_FLAG($cOLAREA_ACTIVE_FLAG = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_ACTIVE_FLAG) && !is_string($cOLAREA_ACTIVE_FLAG)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_ACTIVE_FLAG)), __LINE__);
        }
        $this->COLAREA_ACTIVE_FLAG = $cOLAREA_ACTIVE_FLAG;
        return $this;
    }
    /**
     * Get COLAREA_CITY value
     * @return string|null
     */
    public function getCOLAREA_CITY()
    {
        return $this->COLAREA_CITY;
    }
    /**
     * Set COLAREA_CITY value
     * @param string $cOLAREA_CITY
     * @return \StructType\TColArea
     */
    public function setCOLAREA_CITY($cOLAREA_CITY = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_CITY) && !is_string($cOLAREA_CITY)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_CITY)), __LINE__);
        }
        $this->COLAREA_CITY = $cOLAREA_CITY;
        return $this;
    }
    /**
     * Get COLAREA_COLENDREASON_ID value
     * @return string|null
     */
    public function getCOLAREA_COLENDREASON_ID()
    {
        return $this->COLAREA_COLENDREASON_ID;
    }
    /**
     * Set COLAREA_COLENDREASON_ID value
     * @param string $cOLAREA_COLENDREASON_ID
     * @return \StructType\TColArea
     */
    public function setCOLAREA_COLENDREASON_ID($cOLAREA_COLENDREASON_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_COLENDREASON_ID) && !is_string($cOLAREA_COLENDREASON_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_COLENDREASON_ID)), __LINE__);
        }
        $this->COLAREA_COLENDREASON_ID = $cOLAREA_COLENDREASON_ID;
        return $this;
    }
    /**
     * Get COLAREA_COLLECTANTS_WISH value
     * @return int|null
     */
    public function getCOLAREA_COLLECTANTS_WISH()
    {
        return $this->COLAREA_COLLECTANTS_WISH;
    }
    /**
     * Set COLAREA_COLLECTANTS_WISH value
     * @param int $cOLAREA_COLLECTANTS_WISH
     * @return \StructType\TColArea
     */
    public function setCOLAREA_COLLECTANTS_WISH($cOLAREA_COLLECTANTS_WISH = null)
    {
        // validation for constraint: int
        if (!is_null($cOLAREA_COLLECTANTS_WISH) && !is_numeric($cOLAREA_COLLECTANTS_WISH)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cOLAREA_COLLECTANTS_WISH)), __LINE__);
        }
        $this->COLAREA_COLLECTANTS_WISH = $cOLAREA_COLLECTANTS_WISH;
        return $this;
    }
    /**
     * Get COLAREA_COLREGION_DESCR value
     * @return string|null
     */
    public function getCOLAREA_COLREGION_DESCR()
    {
        return $this->COLAREA_COLREGION_DESCR;
    }
    /**
     * Set COLAREA_COLREGION_DESCR value
     * @param string $cOLAREA_COLREGION_DESCR
     * @return \StructType\TColArea
     */
    public function setCOLAREA_COLREGION_DESCR($cOLAREA_COLREGION_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_COLREGION_DESCR) && !is_string($cOLAREA_COLREGION_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_COLREGION_DESCR)), __LINE__);
        }
        $this->COLAREA_COLREGION_DESCR = $cOLAREA_COLREGION_DESCR;
        return $this;
    }
    /**
     * Get COLAREA_COUNTY_RELATION_ID value
     * @return int|null
     */
    public function getCOLAREA_COUNTY_RELATION_ID()
    {
        return $this->COLAREA_COUNTY_RELATION_ID;
    }
    /**
     * Set COLAREA_COUNTY_RELATION_ID value
     * @param int $cOLAREA_COUNTY_RELATION_ID
     * @return \StructType\TColArea
     */
    public function setCOLAREA_COUNTY_RELATION_ID($cOLAREA_COUNTY_RELATION_ID = null)
    {
        // validation for constraint: int
        if (!is_null($cOLAREA_COUNTY_RELATION_ID) && !is_numeric($cOLAREA_COUNTY_RELATION_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cOLAREA_COUNTY_RELATION_ID)), __LINE__);
        }
        $this->COLAREA_COUNTY_RELATION_ID = $cOLAREA_COUNTY_RELATION_ID;
        return $this;
    }
    /**
     * Get COLAREA_COUNTY_RELATION_NAME value
     * @return string|null
     */
    public function getCOLAREA_COUNTY_RELATION_NAME()
    {
        return $this->COLAREA_COUNTY_RELATION_NAME;
    }
    /**
     * Set COLAREA_COUNTY_RELATION_NAME value
     * @param string $cOLAREA_COUNTY_RELATION_NAME
     * @return \StructType\TColArea
     */
    public function setCOLAREA_COUNTY_RELATION_NAME($cOLAREA_COUNTY_RELATION_NAME = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_COUNTY_RELATION_NAME) && !is_string($cOLAREA_COUNTY_RELATION_NAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_COUNTY_RELATION_NAME)), __LINE__);
        }
        $this->COLAREA_COUNTY_RELATION_NAME = $cOLAREA_COUNTY_RELATION_NAME;
        return $this;
    }
    /**
     * Get COLAREA_DESCR value
     * @return string|null
     */
    public function getCOLAREA_DESCR()
    {
        return $this->COLAREA_DESCR;
    }
    /**
     * Set COLAREA_DESCR value
     * @param string $cOLAREA_DESCR
     * @return \StructType\TColArea
     */
    public function setCOLAREA_DESCR($cOLAREA_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_DESCR) && !is_string($cOLAREA_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_DESCR)), __LINE__);
        }
        $this->COLAREA_DESCR = $cOLAREA_DESCR;
        return $this;
    }
    /**
     * Get COLAREA_END_DATE value
     * @return string|null
     */
    public function getCOLAREA_END_DATE()
    {
        return $this->COLAREA_END_DATE;
    }
    /**
     * Set COLAREA_END_DATE value
     * @param string $cOLAREA_END_DATE
     * @return \StructType\TColArea
     */
    public function setCOLAREA_END_DATE($cOLAREA_END_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_END_DATE) && !is_string($cOLAREA_END_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_END_DATE)), __LINE__);
        }
        $this->COLAREA_END_DATE = $cOLAREA_END_DATE;
        return $this;
    }
    /**
     * Get COLAREA_HOUSES_LAST_YEAR value
     * @return int|null
     */
    public function getCOLAREA_HOUSES_LAST_YEAR()
    {
        return $this->COLAREA_HOUSES_LAST_YEAR;
    }
    /**
     * Set COLAREA_HOUSES_LAST_YEAR value
     * @param int $cOLAREA_HOUSES_LAST_YEAR
     * @return \StructType\TColArea
     */
    public function setCOLAREA_HOUSES_LAST_YEAR($cOLAREA_HOUSES_LAST_YEAR = null)
    {
        // validation for constraint: int
        if (!is_null($cOLAREA_HOUSES_LAST_YEAR) && !is_numeric($cOLAREA_HOUSES_LAST_YEAR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cOLAREA_HOUSES_LAST_YEAR)), __LINE__);
        }
        $this->COLAREA_HOUSES_LAST_YEAR = $cOLAREA_HOUSES_LAST_YEAR;
        return $this;
    }
    /**
     * Get COLAREA_HOUSES_THIS_YEAR value
     * @return int|null
     */
    public function getCOLAREA_HOUSES_THIS_YEAR()
    {
        return $this->COLAREA_HOUSES_THIS_YEAR;
    }
    /**
     * Set COLAREA_HOUSES_THIS_YEAR value
     * @param int $cOLAREA_HOUSES_THIS_YEAR
     * @return \StructType\TColArea
     */
    public function setCOLAREA_HOUSES_THIS_YEAR($cOLAREA_HOUSES_THIS_YEAR = null)
    {
        // validation for constraint: int
        if (!is_null($cOLAREA_HOUSES_THIS_YEAR) && !is_numeric($cOLAREA_HOUSES_THIS_YEAR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cOLAREA_HOUSES_THIS_YEAR)), __LINE__);
        }
        $this->COLAREA_HOUSES_THIS_YEAR = $cOLAREA_HOUSES_THIS_YEAR;
        return $this;
    }
    /**
     * Get COLAREA_ID value
     * @return string|null
     */
    public function getCOLAREA_ID()
    {
        return $this->COLAREA_ID;
    }
    /**
     * Set COLAREA_ID value
     * @param string $cOLAREA_ID
     * @return \StructType\TColArea
     */
    public function setCOLAREA_ID($cOLAREA_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_ID) && !is_string($cOLAREA_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_ID)), __LINE__);
        }
        $this->COLAREA_ID = $cOLAREA_ID;
        return $this;
    }
    /**
     * Get COLAREA_MEMO value
     * @return string|null
     */
    public function getCOLAREA_MEMO()
    {
        return $this->COLAREA_MEMO;
    }
    /**
     * Set COLAREA_MEMO value
     * @param string $cOLAREA_MEMO
     * @return \StructType\TColArea
     */
    public function setCOLAREA_MEMO($cOLAREA_MEMO = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_MEMO) && !is_string($cOLAREA_MEMO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_MEMO)), __LINE__);
        }
        $this->COLAREA_MEMO = $cOLAREA_MEMO;
        return $this;
    }
    /**
     * Get COLAREA_ORG_COLSOURCE_ID value
     * @return string|null
     */
    public function getCOLAREA_ORG_COLSOURCE_ID()
    {
        return $this->COLAREA_ORG_COLSOURCE_ID;
    }
    /**
     * Set COLAREA_ORG_COLSOURCE_ID value
     * @param string $cOLAREA_ORG_COLSOURCE_ID
     * @return \StructType\TColArea
     */
    public function setCOLAREA_ORG_COLSOURCE_ID($cOLAREA_ORG_COLSOURCE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_ORG_COLSOURCE_ID) && !is_string($cOLAREA_ORG_COLSOURCE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_ORG_COLSOURCE_ID)), __LINE__);
        }
        $this->COLAREA_ORG_COLSOURCE_ID = $cOLAREA_ORG_COLSOURCE_ID;
        return $this;
    }
    /**
     * Get COLAREA_ORG_CONTACT_ID value
     * @return int|null
     */
    public function getCOLAREA_ORG_CONTACT_ID()
    {
        return $this->COLAREA_ORG_CONTACT_ID;
    }
    /**
     * Set COLAREA_ORG_CONTACT_ID value
     * @param int $cOLAREA_ORG_CONTACT_ID
     * @return \StructType\TColArea
     */
    public function setCOLAREA_ORG_CONTACT_ID($cOLAREA_ORG_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($cOLAREA_ORG_CONTACT_ID) && !is_numeric($cOLAREA_ORG_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cOLAREA_ORG_CONTACT_ID)), __LINE__);
        }
        $this->COLAREA_ORG_CONTACT_ID = $cOLAREA_ORG_CONTACT_ID;
        return $this;
    }
    /**
     * Get COLAREA_ORG_CONTACT_ID_DESCR value
     * @return string|null
     */
    public function getCOLAREA_ORG_CONTACT_ID_DESCR()
    {
        return $this->COLAREA_ORG_CONTACT_ID_DESCR;
    }
    /**
     * Set COLAREA_ORG_CONTACT_ID_DESCR value
     * @param string $cOLAREA_ORG_CONTACT_ID_DESCR
     * @return \StructType\TColArea
     */
    public function setCOLAREA_ORG_CONTACT_ID_DESCR($cOLAREA_ORG_CONTACT_ID_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_ORG_CONTACT_ID_DESCR) && !is_string($cOLAREA_ORG_CONTACT_ID_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_ORG_CONTACT_ID_DESCR)), __LINE__);
        }
        $this->COLAREA_ORG_CONTACT_ID_DESCR = $cOLAREA_ORG_CONTACT_ID_DESCR;
        return $this;
    }
    /**
     * Get COLAREA_ORG_END_DATE value
     * @return string|null
     */
    public function getCOLAREA_ORG_END_DATE()
    {
        return $this->COLAREA_ORG_END_DATE;
    }
    /**
     * Set COLAREA_ORG_END_DATE value
     * @param string $cOLAREA_ORG_END_DATE
     * @return \StructType\TColArea
     */
    public function setCOLAREA_ORG_END_DATE($cOLAREA_ORG_END_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_ORG_END_DATE) && !is_string($cOLAREA_ORG_END_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_ORG_END_DATE)), __LINE__);
        }
        $this->COLAREA_ORG_END_DATE = $cOLAREA_ORG_END_DATE;
        return $this;
    }
    /**
     * Get COLAREA_ORG_START_DATE value
     * @return string|null
     */
    public function getCOLAREA_ORG_START_DATE()
    {
        return $this->COLAREA_ORG_START_DATE;
    }
    /**
     * Set COLAREA_ORG_START_DATE value
     * @param string $cOLAREA_ORG_START_DATE
     * @return \StructType\TColArea
     */
    public function setCOLAREA_ORG_START_DATE($cOLAREA_ORG_START_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_ORG_START_DATE) && !is_string($cOLAREA_ORG_START_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_ORG_START_DATE)), __LINE__);
        }
        $this->COLAREA_ORG_START_DATE = $cOLAREA_ORG_START_DATE;
        return $this;
    }
    /**
     * Get COLAREA_START_DATE value
     * @return string|null
     */
    public function getCOLAREA_START_DATE()
    {
        return $this->COLAREA_START_DATE;
    }
    /**
     * Set COLAREA_START_DATE value
     * @param string $cOLAREA_START_DATE
     * @return \StructType\TColArea
     */
    public function setCOLAREA_START_DATE($cOLAREA_START_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($cOLAREA_START_DATE) && !is_string($cOLAREA_START_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cOLAREA_START_DATE)), __LINE__);
        }
        $this->COLAREA_START_DATE = $cOLAREA_START_DATE;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TColArea
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TColArea
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TColArea
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TColArea
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TColArea
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TColArea
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
