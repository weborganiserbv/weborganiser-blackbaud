<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TCONTACTID StructType
 * @subpackage Structs
 */
class TCONTACTID extends AbstractStructBase
{
    /**
     * The CONTACT_ID
     * @var int
     */
    public $CONTACT_ID;
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * Constructor method for TCONTACTID
     * @uses TCONTACTID::setCONTACT_ID()
     * @uses TCONTACTID::setERROR_ID()
     * @param int $cONTACT_ID
     * @param int $eRROR_ID
     */
    public function __construct($cONTACT_ID = null, $eRROR_ID = null)
    {
        $this
            ->setCONTACT_ID($cONTACT_ID)
            ->setERROR_ID($eRROR_ID);
    }
    /**
     * Get CONTACT_ID value
     * @return int|null
     */
    public function getCONTACT_ID()
    {
        return $this->CONTACT_ID;
    }
    /**
     * Set CONTACT_ID value
     * @param int $cONTACT_ID
     * @return \StructType\TCONTACTID
     */
    public function setCONTACT_ID($cONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($cONTACT_ID) && !is_numeric($cONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($cONTACT_ID)), __LINE__);
        }
        $this->CONTACT_ID = $cONTACT_ID;
        return $this;
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TCONTACTID
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TCONTACTID
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
