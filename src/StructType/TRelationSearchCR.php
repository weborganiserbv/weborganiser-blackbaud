<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TRelationSearchCR StructType
 * @subpackage Structs
 */
class TRelationSearchCR extends AbstractStructBase
{
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The RelationSearchArray
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TRelationSearchResult[]
     * - ref: soapenc:arrayType
     * @var \StructType\TRelationSearchResult[]
     */
    public $RelationSearchArray;
    /**
     * Constructor method for TRelationSearchCR
     * @uses TRelationSearchCR::setERROR_ID()
     * @uses TRelationSearchCR::setRelationSearchArray()
     * @param int $eRROR_ID
     * @param \StructType\TRelationSearchResult[] $relationSearchArray
     */
    public function __construct($eRROR_ID = null, array $relationSearchArray = array())
    {
        $this
            ->setERROR_ID($eRROR_ID)
            ->setRelationSearchArray($relationSearchArray);
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TRelationSearchCR
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get RelationSearchArray value
     * @return \StructType\TRelationSearchResult[]|null
     */
    public function getRelationSearchArray()
    {
        return $this->RelationSearchArray;
    }
    /**
     * Set RelationSearchArray value
     * @throws \InvalidArgumentException
     * @param \StructType\TRelationSearchResult[] $relationSearchArray
     * @return \StructType\TRelationSearchCR
     */
    public function setRelationSearchArray(array $relationSearchArray = array())
    {
        foreach ($relationSearchArray as $tRelationSearchCRRelationSearchArrayItem) {
            // validation for constraint: itemType
            if (!$tRelationSearchCRRelationSearchArrayItem instanceof \StructType\TRelationSearchResult) {
                throw new \InvalidArgumentException(sprintf('The RelationSearchArray property can only contain items of \StructType\TRelationSearchResult, "%s" given', is_object($tRelationSearchCRRelationSearchArrayItem) ? get_class($tRelationSearchCRRelationSearchArrayItem) : gettype($tRelationSearchCRRelationSearchArrayItem)), __LINE__);
            }
        }
        $this->RelationSearchArray = $relationSearchArray;
        return $this;
    }
    /**
     * Add item to RelationSearchArray value
     * @throws \InvalidArgumentException
     * @param \StructType\TRelationSearchResult $item
     * @return \StructType\TRelationSearchCR
     */
    public function addToRelationSearchArray(\StructType\TRelationSearchResult $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TRelationSearchResult) {
            throw new \InvalidArgumentException(sprintf('The RelationSearchArray property can only contain items of \StructType\TRelationSearchResult, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->RelationSearchArray[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TRelationSearchCR
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
