<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TPersonContactCR StructType
 * @subpackage Structs
 */
class TPersonContactCR extends AbstractStructBase
{
    /**
     * The Contact_Id
     * @var int
     */
    public $Contact_Id;
    /**
     * The Error_Id
     * @var int
     */
    public $Error_Id;
    /**
     * Constructor method for TPersonContactCR
     * @uses TPersonContactCR::setContact_Id()
     * @uses TPersonContactCR::setError_Id()
     * @param int $contact_Id
     * @param int $error_Id
     */
    public function __construct($contact_Id = null, $error_Id = null)
    {
        $this
            ->setContact_Id($contact_Id)
            ->setError_Id($error_Id);
    }
    /**
     * Get Contact_Id value
     * @return int|null
     */
    public function getContact_Id()
    {
        return $this->Contact_Id;
    }
    /**
     * Set Contact_Id value
     * @param int $contact_Id
     * @return \StructType\TPersonContactCR
     */
    public function setContact_Id($contact_Id = null)
    {
        // validation for constraint: int
        if (!is_null($contact_Id) && !is_numeric($contact_Id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($contact_Id)), __LINE__);
        }
        $this->Contact_Id = $contact_Id;
        return $this;
    }
    /**
     * Get Error_Id value
     * @return int|null
     */
    public function getError_Id()
    {
        return $this->Error_Id;
    }
    /**
     * Set Error_Id value
     * @param int $error_Id
     * @return \StructType\TPersonContactCR
     */
    public function setError_Id($error_Id = null)
    {
        // validation for constraint: int
        if (!is_null($error_Id) && !is_numeric($error_Id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($error_Id)), __LINE__);
        }
        $this->Error_Id = $error_Id;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TPersonContactCR
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
