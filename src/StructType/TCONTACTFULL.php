<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TCONTACTFULL StructType
 * @subpackage Structs
 */
class TCONTACTFULL extends AbstractStructBase
{
    /**
     * The ADDRESS_CITY
     * @var string
     */
    public $ADDRESS_CITY;
    /**
     * The ADDRESS_COUNTRY_ID
     * @var string
     */
    public $ADDRESS_COUNTRY_ID;
    /**
     * The ADDRESS_HOUSE_NO
     * @var string
     */
    public $ADDRESS_HOUSE_NO;
    /**
     * The ADDRESS_HOUSE_NO_ADD
     * @var string
     */
    public $ADDRESS_HOUSE_NO_ADD;
    /**
     * The ADDRESS_POSTCODE
     * @var string
     */
    public $ADDRESS_POSTCODE;
    /**
     * The ADDRESS_STREET
     * @var string
     */
    public $ADDRESS_STREET;
    /**
     * The CONTACT_COMSTAT_LIST
     * @var string
     */
    public $CONTACT_COMSTAT_LIST;
    /**
     * The CONTACT_EMAIL
     * @var string
     */
    public $CONTACT_EMAIL;
    /**
     * The CONTACT_PASSWORD
     * @var string
     */
    public $CONTACT_PASSWORD;
    /**
     * The CONTACT_STAT1
     * @var string
     */
    public $CONTACT_STAT1;
    /**
     * The CONTACT_STAT2
     * @var string
     */
    public $CONTACT_STAT2;
    /**
     * The CONTACT_TELEPHONE
     * @var string
     */
    public $CONTACT_TELEPHONE;
    /**
     * The CONTACT_USERNAME
     * @var string
     */
    public $CONTACT_USERNAME;
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The FREE_X1000_10
     * @var string
     */
    public $FREE_X1000_10;
    /**
     * The FREE_X100_1
     * @var string
     */
    public $FREE_X100_1;
    /**
     * The FREE_X100_2
     * @var string
     */
    public $FREE_X100_2;
    /**
     * The FREE_X100_3
     * @var string
     */
    public $FREE_X100_3;
    /**
     * The FREE_X100_4
     * @var string
     */
    public $FREE_X100_4;
    /**
     * The FREE_X100_5
     * @var string
     */
    public $FREE_X100_5;
    /**
     * The FREE_X100_6
     * @var string
     */
    public $FREE_X100_6;
    /**
     * The FREE_X100_7
     * @var string
     */
    public $FREE_X100_7;
    /**
     * The FREE_X100_8
     * @var string
     */
    public $FREE_X100_8;
    /**
     * The FREE_X100_9
     * @var string
     */
    public $FREE_X100_9;
    /**
     * The PERSON_BIRTH_DATE
     * @var string
     */
    public $PERSON_BIRTH_DATE;
    /**
     * The PERSON_EMAIL
     * @var string
     */
    public $PERSON_EMAIL;
    /**
     * The PERSON_FIRST_NAME
     * @var string
     */
    public $PERSON_FIRST_NAME;
    /**
     * The PERSON_ID
     * @var int
     */
    public $PERSON_ID;
    /**
     * The PERSON_INITIALS
     * @var string
     */
    public $PERSON_INITIALS;
    /**
     * The PERSON_MIDDLE_NAME
     * @var string
     */
    public $PERSON_MIDDLE_NAME;
    /**
     * The PERSON_PREFIX
     * @var string
     */
    public $PERSON_PREFIX;
    /**
     * The PERSON_SURNAME_1
     * @var string
     */
    public $PERSON_SURNAME_1;
    /**
     * The PERSON_SURNAME_2
     * @var string
     */
    public $PERSON_SURNAME_2;
    /**
     * The PERSON_TELEPHONE
     * @var string
     */
    public $PERSON_TELEPHONE;
    /**
     * The PERSON_TELEPHONE_GSM
     * @var string
     */
    public $PERSON_TELEPHONE_GSM;
    /**
     * The PERSON_TITLE_ID
     * @var string
     */
    public $PERSON_TITLE_ID;
    /**
     * The RELATION_DUN_DATE
     * @var string
     */
    public $RELATION_DUN_DATE;
    /**
     * The RELATION_DUN_STATUS
     * @var int
     */
    public $RELATION_DUN_STATUS;
    /**
     * The RELATION_ID
     * @var int
     */
    public $RELATION_ID;
    /**
     * The RELATION_NAME_1
     * @var string
     */
    public $RELATION_NAME_1;
    /**
     * The RELATION_NAME_2
     * @var string
     */
    public $RELATION_NAME_2;
    /**
     * The RELATION_PAYACCOUNT_ID
     * @var string
     */
    public $RELATION_PAYACCOUNT_ID;
    /**
     * The RELATION_SOURCE_ID
     * @var string
     */
    public $RELATION_SOURCE_ID;
    /**
     * The RELATION_SUBPROFILE_ID
     * @var string
     */
    public $RELATION_SUBPROFILE_ID;
    /**
     * The RELATION_UDPROFILE_ID
     * @var string
     */
    public $RELATION_UDPROFILE_ID;
    /**
     * Constructor method for TCONTACTFULL
     * @uses TCONTACTFULL::setADDRESS_CITY()
     * @uses TCONTACTFULL::setADDRESS_COUNTRY_ID()
     * @uses TCONTACTFULL::setADDRESS_HOUSE_NO()
     * @uses TCONTACTFULL::setADDRESS_HOUSE_NO_ADD()
     * @uses TCONTACTFULL::setADDRESS_POSTCODE()
     * @uses TCONTACTFULL::setADDRESS_STREET()
     * @uses TCONTACTFULL::setCONTACT_COMSTAT_LIST()
     * @uses TCONTACTFULL::setCONTACT_EMAIL()
     * @uses TCONTACTFULL::setCONTACT_PASSWORD()
     * @uses TCONTACTFULL::setCONTACT_STAT1()
     * @uses TCONTACTFULL::setCONTACT_STAT2()
     * @uses TCONTACTFULL::setCONTACT_TELEPHONE()
     * @uses TCONTACTFULL::setCONTACT_USERNAME()
     * @uses TCONTACTFULL::setERROR_ID()
     * @uses TCONTACTFULL::setFREE_X1000_10()
     * @uses TCONTACTFULL::setFREE_X100_1()
     * @uses TCONTACTFULL::setFREE_X100_2()
     * @uses TCONTACTFULL::setFREE_X100_3()
     * @uses TCONTACTFULL::setFREE_X100_4()
     * @uses TCONTACTFULL::setFREE_X100_5()
     * @uses TCONTACTFULL::setFREE_X100_6()
     * @uses TCONTACTFULL::setFREE_X100_7()
     * @uses TCONTACTFULL::setFREE_X100_8()
     * @uses TCONTACTFULL::setFREE_X100_9()
     * @uses TCONTACTFULL::setPERSON_BIRTH_DATE()
     * @uses TCONTACTFULL::setPERSON_EMAIL()
     * @uses TCONTACTFULL::setPERSON_FIRST_NAME()
     * @uses TCONTACTFULL::setPERSON_ID()
     * @uses TCONTACTFULL::setPERSON_INITIALS()
     * @uses TCONTACTFULL::setPERSON_MIDDLE_NAME()
     * @uses TCONTACTFULL::setPERSON_PREFIX()
     * @uses TCONTACTFULL::setPERSON_SURNAME_1()
     * @uses TCONTACTFULL::setPERSON_SURNAME_2()
     * @uses TCONTACTFULL::setPERSON_TELEPHONE()
     * @uses TCONTACTFULL::setPERSON_TELEPHONE_GSM()
     * @uses TCONTACTFULL::setPERSON_TITLE_ID()
     * @uses TCONTACTFULL::setRELATION_DUN_DATE()
     * @uses TCONTACTFULL::setRELATION_DUN_STATUS()
     * @uses TCONTACTFULL::setRELATION_ID()
     * @uses TCONTACTFULL::setRELATION_NAME_1()
     * @uses TCONTACTFULL::setRELATION_NAME_2()
     * @uses TCONTACTFULL::setRELATION_PAYACCOUNT_ID()
     * @uses TCONTACTFULL::setRELATION_SOURCE_ID()
     * @uses TCONTACTFULL::setRELATION_SUBPROFILE_ID()
     * @uses TCONTACTFULL::setRELATION_UDPROFILE_ID()
     * @param string $aDDRESS_CITY
     * @param string $aDDRESS_COUNTRY_ID
     * @param string $aDDRESS_HOUSE_NO
     * @param string $aDDRESS_HOUSE_NO_ADD
     * @param string $aDDRESS_POSTCODE
     * @param string $aDDRESS_STREET
     * @param string $cONTACT_COMSTAT_LIST
     * @param string $cONTACT_EMAIL
     * @param string $cONTACT_PASSWORD
     * @param string $cONTACT_STAT1
     * @param string $cONTACT_STAT2
     * @param string $cONTACT_TELEPHONE
     * @param string $cONTACT_USERNAME
     * @param int $eRROR_ID
     * @param string $fREE_X1000_10
     * @param string $fREE_X100_1
     * @param string $fREE_X100_2
     * @param string $fREE_X100_3
     * @param string $fREE_X100_4
     * @param string $fREE_X100_5
     * @param string $fREE_X100_6
     * @param string $fREE_X100_7
     * @param string $fREE_X100_8
     * @param string $fREE_X100_9
     * @param string $pERSON_BIRTH_DATE
     * @param string $pERSON_EMAIL
     * @param string $pERSON_FIRST_NAME
     * @param int $pERSON_ID
     * @param string $pERSON_INITIALS
     * @param string $pERSON_MIDDLE_NAME
     * @param string $pERSON_PREFIX
     * @param string $pERSON_SURNAME_1
     * @param string $pERSON_SURNAME_2
     * @param string $pERSON_TELEPHONE
     * @param string $pERSON_TELEPHONE_GSM
     * @param string $pERSON_TITLE_ID
     * @param string $rELATION_DUN_DATE
     * @param int $rELATION_DUN_STATUS
     * @param int $rELATION_ID
     * @param string $rELATION_NAME_1
     * @param string $rELATION_NAME_2
     * @param string $rELATION_PAYACCOUNT_ID
     * @param string $rELATION_SOURCE_ID
     * @param string $rELATION_SUBPROFILE_ID
     * @param string $rELATION_UDPROFILE_ID
     */
    public function __construct($aDDRESS_CITY = null, $aDDRESS_COUNTRY_ID = null, $aDDRESS_HOUSE_NO = null, $aDDRESS_HOUSE_NO_ADD = null, $aDDRESS_POSTCODE = null, $aDDRESS_STREET = null, $cONTACT_COMSTAT_LIST = null, $cONTACT_EMAIL = null, $cONTACT_PASSWORD = null, $cONTACT_STAT1 = null, $cONTACT_STAT2 = null, $cONTACT_TELEPHONE = null, $cONTACT_USERNAME = null, $eRROR_ID = null, $fREE_X1000_10 = null, $fREE_X100_1 = null, $fREE_X100_2 = null, $fREE_X100_3 = null, $fREE_X100_4 = null, $fREE_X100_5 = null, $fREE_X100_6 = null, $fREE_X100_7 = null, $fREE_X100_8 = null, $fREE_X100_9 = null, $pERSON_BIRTH_DATE = null, $pERSON_EMAIL = null, $pERSON_FIRST_NAME = null, $pERSON_ID = null, $pERSON_INITIALS = null, $pERSON_MIDDLE_NAME = null, $pERSON_PREFIX = null, $pERSON_SURNAME_1 = null, $pERSON_SURNAME_2 = null, $pERSON_TELEPHONE = null, $pERSON_TELEPHONE_GSM = null, $pERSON_TITLE_ID = null, $rELATION_DUN_DATE = null, $rELATION_DUN_STATUS = null, $rELATION_ID = null, $rELATION_NAME_1 = null, $rELATION_NAME_2 = null, $rELATION_PAYACCOUNT_ID = null, $rELATION_SOURCE_ID = null, $rELATION_SUBPROFILE_ID = null, $rELATION_UDPROFILE_ID = null)
    {
        $this
            ->setADDRESS_CITY($aDDRESS_CITY)
            ->setADDRESS_COUNTRY_ID($aDDRESS_COUNTRY_ID)
            ->setADDRESS_HOUSE_NO($aDDRESS_HOUSE_NO)
            ->setADDRESS_HOUSE_NO_ADD($aDDRESS_HOUSE_NO_ADD)
            ->setADDRESS_POSTCODE($aDDRESS_POSTCODE)
            ->setADDRESS_STREET($aDDRESS_STREET)
            ->setCONTACT_COMSTAT_LIST($cONTACT_COMSTAT_LIST)
            ->setCONTACT_EMAIL($cONTACT_EMAIL)
            ->setCONTACT_PASSWORD($cONTACT_PASSWORD)
            ->setCONTACT_STAT1($cONTACT_STAT1)
            ->setCONTACT_STAT2($cONTACT_STAT2)
            ->setCONTACT_TELEPHONE($cONTACT_TELEPHONE)
            ->setCONTACT_USERNAME($cONTACT_USERNAME)
            ->setERROR_ID($eRROR_ID)
            ->setFREE_X1000_10($fREE_X1000_10)
            ->setFREE_X100_1($fREE_X100_1)
            ->setFREE_X100_2($fREE_X100_2)
            ->setFREE_X100_3($fREE_X100_3)
            ->setFREE_X100_4($fREE_X100_4)
            ->setFREE_X100_5($fREE_X100_5)
            ->setFREE_X100_6($fREE_X100_6)
            ->setFREE_X100_7($fREE_X100_7)
            ->setFREE_X100_8($fREE_X100_8)
            ->setFREE_X100_9($fREE_X100_9)
            ->setPERSON_BIRTH_DATE($pERSON_BIRTH_DATE)
            ->setPERSON_EMAIL($pERSON_EMAIL)
            ->setPERSON_FIRST_NAME($pERSON_FIRST_NAME)
            ->setPERSON_ID($pERSON_ID)
            ->setPERSON_INITIALS($pERSON_INITIALS)
            ->setPERSON_MIDDLE_NAME($pERSON_MIDDLE_NAME)
            ->setPERSON_PREFIX($pERSON_PREFIX)
            ->setPERSON_SURNAME_1($pERSON_SURNAME_1)
            ->setPERSON_SURNAME_2($pERSON_SURNAME_2)
            ->setPERSON_TELEPHONE($pERSON_TELEPHONE)
            ->setPERSON_TELEPHONE_GSM($pERSON_TELEPHONE_GSM)
            ->setPERSON_TITLE_ID($pERSON_TITLE_ID)
            ->setRELATION_DUN_DATE($rELATION_DUN_DATE)
            ->setRELATION_DUN_STATUS($rELATION_DUN_STATUS)
            ->setRELATION_ID($rELATION_ID)
            ->setRELATION_NAME_1($rELATION_NAME_1)
            ->setRELATION_NAME_2($rELATION_NAME_2)
            ->setRELATION_PAYACCOUNT_ID($rELATION_PAYACCOUNT_ID)
            ->setRELATION_SOURCE_ID($rELATION_SOURCE_ID)
            ->setRELATION_SUBPROFILE_ID($rELATION_SUBPROFILE_ID)
            ->setRELATION_UDPROFILE_ID($rELATION_UDPROFILE_ID);
    }
    /**
     * Get ADDRESS_CITY value
     * @return string|null
     */
    public function getADDRESS_CITY()
    {
        return $this->ADDRESS_CITY;
    }
    /**
     * Set ADDRESS_CITY value
     * @param string $aDDRESS_CITY
     * @return \StructType\TCONTACTFULL
     */
    public function setADDRESS_CITY($aDDRESS_CITY = null)
    {
        // validation for constraint: string
        if (!is_null($aDDRESS_CITY) && !is_string($aDDRESS_CITY)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aDDRESS_CITY)), __LINE__);
        }
        $this->ADDRESS_CITY = $aDDRESS_CITY;
        return $this;
    }
    /**
     * Get ADDRESS_COUNTRY_ID value
     * @return string|null
     */
    public function getADDRESS_COUNTRY_ID()
    {
        return $this->ADDRESS_COUNTRY_ID;
    }
    /**
     * Set ADDRESS_COUNTRY_ID value
     * @param string $aDDRESS_COUNTRY_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setADDRESS_COUNTRY_ID($aDDRESS_COUNTRY_ID = null)
    {
        // validation for constraint: string
        if (!is_null($aDDRESS_COUNTRY_ID) && !is_string($aDDRESS_COUNTRY_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aDDRESS_COUNTRY_ID)), __LINE__);
        }
        $this->ADDRESS_COUNTRY_ID = $aDDRESS_COUNTRY_ID;
        return $this;
    }
    /**
     * Get ADDRESS_HOUSE_NO value
     * @return string|null
     */
    public function getADDRESS_HOUSE_NO()
    {
        return $this->ADDRESS_HOUSE_NO;
    }
    /**
     * Set ADDRESS_HOUSE_NO value
     * @param string $aDDRESS_HOUSE_NO
     * @return \StructType\TCONTACTFULL
     */
    public function setADDRESS_HOUSE_NO($aDDRESS_HOUSE_NO = null)
    {
        // validation for constraint: string
        if (!is_null($aDDRESS_HOUSE_NO) && !is_string($aDDRESS_HOUSE_NO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aDDRESS_HOUSE_NO)), __LINE__);
        }
        $this->ADDRESS_HOUSE_NO = $aDDRESS_HOUSE_NO;
        return $this;
    }
    /**
     * Get ADDRESS_HOUSE_NO_ADD value
     * @return string|null
     */
    public function getADDRESS_HOUSE_NO_ADD()
    {
        return $this->ADDRESS_HOUSE_NO_ADD;
    }
    /**
     * Set ADDRESS_HOUSE_NO_ADD value
     * @param string $aDDRESS_HOUSE_NO_ADD
     * @return \StructType\TCONTACTFULL
     */
    public function setADDRESS_HOUSE_NO_ADD($aDDRESS_HOUSE_NO_ADD = null)
    {
        // validation for constraint: string
        if (!is_null($aDDRESS_HOUSE_NO_ADD) && !is_string($aDDRESS_HOUSE_NO_ADD)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aDDRESS_HOUSE_NO_ADD)), __LINE__);
        }
        $this->ADDRESS_HOUSE_NO_ADD = $aDDRESS_HOUSE_NO_ADD;
        return $this;
    }
    /**
     * Get ADDRESS_POSTCODE value
     * @return string|null
     */
    public function getADDRESS_POSTCODE()
    {
        return $this->ADDRESS_POSTCODE;
    }
    /**
     * Set ADDRESS_POSTCODE value
     * @param string $aDDRESS_POSTCODE
     * @return \StructType\TCONTACTFULL
     */
    public function setADDRESS_POSTCODE($aDDRESS_POSTCODE = null)
    {
        // validation for constraint: string
        if (!is_null($aDDRESS_POSTCODE) && !is_string($aDDRESS_POSTCODE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aDDRESS_POSTCODE)), __LINE__);
        }
        $this->ADDRESS_POSTCODE = $aDDRESS_POSTCODE;
        return $this;
    }
    /**
     * Get ADDRESS_STREET value
     * @return string|null
     */
    public function getADDRESS_STREET()
    {
        return $this->ADDRESS_STREET;
    }
    /**
     * Set ADDRESS_STREET value
     * @param string $aDDRESS_STREET
     * @return \StructType\TCONTACTFULL
     */
    public function setADDRESS_STREET($aDDRESS_STREET = null)
    {
        // validation for constraint: string
        if (!is_null($aDDRESS_STREET) && !is_string($aDDRESS_STREET)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($aDDRESS_STREET)), __LINE__);
        }
        $this->ADDRESS_STREET = $aDDRESS_STREET;
        return $this;
    }
    /**
     * Get CONTACT_COMSTAT_LIST value
     * @return string|null
     */
    public function getCONTACT_COMSTAT_LIST()
    {
        return $this->CONTACT_COMSTAT_LIST;
    }
    /**
     * Set CONTACT_COMSTAT_LIST value
     * @param string $cONTACT_COMSTAT_LIST
     * @return \StructType\TCONTACTFULL
     */
    public function setCONTACT_COMSTAT_LIST($cONTACT_COMSTAT_LIST = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_COMSTAT_LIST) && !is_string($cONTACT_COMSTAT_LIST)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_COMSTAT_LIST)), __LINE__);
        }
        $this->CONTACT_COMSTAT_LIST = $cONTACT_COMSTAT_LIST;
        return $this;
    }
    /**
     * Get CONTACT_EMAIL value
     * @return string|null
     */
    public function getCONTACT_EMAIL()
    {
        return $this->CONTACT_EMAIL;
    }
    /**
     * Set CONTACT_EMAIL value
     * @param string $cONTACT_EMAIL
     * @return \StructType\TCONTACTFULL
     */
    public function setCONTACT_EMAIL($cONTACT_EMAIL = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_EMAIL) && !is_string($cONTACT_EMAIL)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_EMAIL)), __LINE__);
        }
        $this->CONTACT_EMAIL = $cONTACT_EMAIL;
        return $this;
    }
    /**
     * Get CONTACT_PASSWORD value
     * @return string|null
     */
    public function getCONTACT_PASSWORD()
    {
        return $this->CONTACT_PASSWORD;
    }
    /**
     * Set CONTACT_PASSWORD value
     * @param string $cONTACT_PASSWORD
     * @return \StructType\TCONTACTFULL
     */
    public function setCONTACT_PASSWORD($cONTACT_PASSWORD = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_PASSWORD) && !is_string($cONTACT_PASSWORD)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_PASSWORD)), __LINE__);
        }
        $this->CONTACT_PASSWORD = $cONTACT_PASSWORD;
        return $this;
    }
    /**
     * Get CONTACT_STAT1 value
     * @return string|null
     */
    public function getCONTACT_STAT1()
    {
        return $this->CONTACT_STAT1;
    }
    /**
     * Set CONTACT_STAT1 value
     * @param string $cONTACT_STAT1
     * @return \StructType\TCONTACTFULL
     */
    public function setCONTACT_STAT1($cONTACT_STAT1 = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_STAT1) && !is_string($cONTACT_STAT1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_STAT1)), __LINE__);
        }
        $this->CONTACT_STAT1 = $cONTACT_STAT1;
        return $this;
    }
    /**
     * Get CONTACT_STAT2 value
     * @return string|null
     */
    public function getCONTACT_STAT2()
    {
        return $this->CONTACT_STAT2;
    }
    /**
     * Set CONTACT_STAT2 value
     * @param string $cONTACT_STAT2
     * @return \StructType\TCONTACTFULL
     */
    public function setCONTACT_STAT2($cONTACT_STAT2 = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_STAT2) && !is_string($cONTACT_STAT2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_STAT2)), __LINE__);
        }
        $this->CONTACT_STAT2 = $cONTACT_STAT2;
        return $this;
    }
    /**
     * Get CONTACT_TELEPHONE value
     * @return string|null
     */
    public function getCONTACT_TELEPHONE()
    {
        return $this->CONTACT_TELEPHONE;
    }
    /**
     * Set CONTACT_TELEPHONE value
     * @param string $cONTACT_TELEPHONE
     * @return \StructType\TCONTACTFULL
     */
    public function setCONTACT_TELEPHONE($cONTACT_TELEPHONE = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_TELEPHONE) && !is_string($cONTACT_TELEPHONE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_TELEPHONE)), __LINE__);
        }
        $this->CONTACT_TELEPHONE = $cONTACT_TELEPHONE;
        return $this;
    }
    /**
     * Get CONTACT_USERNAME value
     * @return string|null
     */
    public function getCONTACT_USERNAME()
    {
        return $this->CONTACT_USERNAME;
    }
    /**
     * Set CONTACT_USERNAME value
     * @param string $cONTACT_USERNAME
     * @return \StructType\TCONTACTFULL
     */
    public function setCONTACT_USERNAME($cONTACT_USERNAME = null)
    {
        // validation for constraint: string
        if (!is_null($cONTACT_USERNAME) && !is_string($cONTACT_USERNAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($cONTACT_USERNAME)), __LINE__);
        }
        $this->CONTACT_USERNAME = $cONTACT_USERNAME;
        return $this;
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get FREE_X1000_10 value
     * @return string|null
     */
    public function getFREE_X1000_10()
    {
        return $this->FREE_X1000_10;
    }
    /**
     * Set FREE_X1000_10 value
     * @param string $fREE_X1000_10
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X1000_10($fREE_X1000_10 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X1000_10) && !is_string($fREE_X1000_10)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X1000_10)), __LINE__);
        }
        $this->FREE_X1000_10 = $fREE_X1000_10;
        return $this;
    }
    /**
     * Get FREE_X100_1 value
     * @return string|null
     */
    public function getFREE_X100_1()
    {
        return $this->FREE_X100_1;
    }
    /**
     * Set FREE_X100_1 value
     * @param string $fREE_X100_1
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_1($fREE_X100_1 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_1) && !is_string($fREE_X100_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_1)), __LINE__);
        }
        $this->FREE_X100_1 = $fREE_X100_1;
        return $this;
    }
    /**
     * Get FREE_X100_2 value
     * @return string|null
     */
    public function getFREE_X100_2()
    {
        return $this->FREE_X100_2;
    }
    /**
     * Set FREE_X100_2 value
     * @param string $fREE_X100_2
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_2($fREE_X100_2 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_2) && !is_string($fREE_X100_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_2)), __LINE__);
        }
        $this->FREE_X100_2 = $fREE_X100_2;
        return $this;
    }
    /**
     * Get FREE_X100_3 value
     * @return string|null
     */
    public function getFREE_X100_3()
    {
        return $this->FREE_X100_3;
    }
    /**
     * Set FREE_X100_3 value
     * @param string $fREE_X100_3
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_3($fREE_X100_3 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_3) && !is_string($fREE_X100_3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_3)), __LINE__);
        }
        $this->FREE_X100_3 = $fREE_X100_3;
        return $this;
    }
    /**
     * Get FREE_X100_4 value
     * @return string|null
     */
    public function getFREE_X100_4()
    {
        return $this->FREE_X100_4;
    }
    /**
     * Set FREE_X100_4 value
     * @param string $fREE_X100_4
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_4($fREE_X100_4 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_4) && !is_string($fREE_X100_4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_4)), __LINE__);
        }
        $this->FREE_X100_4 = $fREE_X100_4;
        return $this;
    }
    /**
     * Get FREE_X100_5 value
     * @return string|null
     */
    public function getFREE_X100_5()
    {
        return $this->FREE_X100_5;
    }
    /**
     * Set FREE_X100_5 value
     * @param string $fREE_X100_5
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_5($fREE_X100_5 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_5) && !is_string($fREE_X100_5)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_5)), __LINE__);
        }
        $this->FREE_X100_5 = $fREE_X100_5;
        return $this;
    }
    /**
     * Get FREE_X100_6 value
     * @return string|null
     */
    public function getFREE_X100_6()
    {
        return $this->FREE_X100_6;
    }
    /**
     * Set FREE_X100_6 value
     * @param string $fREE_X100_6
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_6($fREE_X100_6 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_6) && !is_string($fREE_X100_6)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_6)), __LINE__);
        }
        $this->FREE_X100_6 = $fREE_X100_6;
        return $this;
    }
    /**
     * Get FREE_X100_7 value
     * @return string|null
     */
    public function getFREE_X100_7()
    {
        return $this->FREE_X100_7;
    }
    /**
     * Set FREE_X100_7 value
     * @param string $fREE_X100_7
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_7($fREE_X100_7 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_7) && !is_string($fREE_X100_7)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_7)), __LINE__);
        }
        $this->FREE_X100_7 = $fREE_X100_7;
        return $this;
    }
    /**
     * Get FREE_X100_8 value
     * @return string|null
     */
    public function getFREE_X100_8()
    {
        return $this->FREE_X100_8;
    }
    /**
     * Set FREE_X100_8 value
     * @param string $fREE_X100_8
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_8($fREE_X100_8 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_8) && !is_string($fREE_X100_8)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_8)), __LINE__);
        }
        $this->FREE_X100_8 = $fREE_X100_8;
        return $this;
    }
    /**
     * Get FREE_X100_9 value
     * @return string|null
     */
    public function getFREE_X100_9()
    {
        return $this->FREE_X100_9;
    }
    /**
     * Set FREE_X100_9 value
     * @param string $fREE_X100_9
     * @return \StructType\TCONTACTFULL
     */
    public function setFREE_X100_9($fREE_X100_9 = null)
    {
        // validation for constraint: string
        if (!is_null($fREE_X100_9) && !is_string($fREE_X100_9)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($fREE_X100_9)), __LINE__);
        }
        $this->FREE_X100_9 = $fREE_X100_9;
        return $this;
    }
    /**
     * Get PERSON_BIRTH_DATE value
     * @return string|null
     */
    public function getPERSON_BIRTH_DATE()
    {
        return $this->PERSON_BIRTH_DATE;
    }
    /**
     * Set PERSON_BIRTH_DATE value
     * @param string $pERSON_BIRTH_DATE
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_BIRTH_DATE($pERSON_BIRTH_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_BIRTH_DATE) && !is_string($pERSON_BIRTH_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_BIRTH_DATE)), __LINE__);
        }
        $this->PERSON_BIRTH_DATE = $pERSON_BIRTH_DATE;
        return $this;
    }
    /**
     * Get PERSON_EMAIL value
     * @return string|null
     */
    public function getPERSON_EMAIL()
    {
        return $this->PERSON_EMAIL;
    }
    /**
     * Set PERSON_EMAIL value
     * @param string $pERSON_EMAIL
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_EMAIL($pERSON_EMAIL = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_EMAIL) && !is_string($pERSON_EMAIL)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_EMAIL)), __LINE__);
        }
        $this->PERSON_EMAIL = $pERSON_EMAIL;
        return $this;
    }
    /**
     * Get PERSON_FIRST_NAME value
     * @return string|null
     */
    public function getPERSON_FIRST_NAME()
    {
        return $this->PERSON_FIRST_NAME;
    }
    /**
     * Set PERSON_FIRST_NAME value
     * @param string $pERSON_FIRST_NAME
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_FIRST_NAME($pERSON_FIRST_NAME = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_FIRST_NAME) && !is_string($pERSON_FIRST_NAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_FIRST_NAME)), __LINE__);
        }
        $this->PERSON_FIRST_NAME = $pERSON_FIRST_NAME;
        return $this;
    }
    /**
     * Get PERSON_ID value
     * @return int|null
     */
    public function getPERSON_ID()
    {
        return $this->PERSON_ID;
    }
    /**
     * Set PERSON_ID value
     * @param int $pERSON_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_ID($pERSON_ID = null)
    {
        // validation for constraint: int
        if (!is_null($pERSON_ID) && !is_numeric($pERSON_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($pERSON_ID)), __LINE__);
        }
        $this->PERSON_ID = $pERSON_ID;
        return $this;
    }
    /**
     * Get PERSON_INITIALS value
     * @return string|null
     */
    public function getPERSON_INITIALS()
    {
        return $this->PERSON_INITIALS;
    }
    /**
     * Set PERSON_INITIALS value
     * @param string $pERSON_INITIALS
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_INITIALS($pERSON_INITIALS = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_INITIALS) && !is_string($pERSON_INITIALS)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_INITIALS)), __LINE__);
        }
        $this->PERSON_INITIALS = $pERSON_INITIALS;
        return $this;
    }
    /**
     * Get PERSON_MIDDLE_NAME value
     * @return string|null
     */
    public function getPERSON_MIDDLE_NAME()
    {
        return $this->PERSON_MIDDLE_NAME;
    }
    /**
     * Set PERSON_MIDDLE_NAME value
     * @param string $pERSON_MIDDLE_NAME
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_MIDDLE_NAME($pERSON_MIDDLE_NAME = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_MIDDLE_NAME) && !is_string($pERSON_MIDDLE_NAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_MIDDLE_NAME)), __LINE__);
        }
        $this->PERSON_MIDDLE_NAME = $pERSON_MIDDLE_NAME;
        return $this;
    }
    /**
     * Get PERSON_PREFIX value
     * @return string|null
     */
    public function getPERSON_PREFIX()
    {
        return $this->PERSON_PREFIX;
    }
    /**
     * Set PERSON_PREFIX value
     * @param string $pERSON_PREFIX
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_PREFIX($pERSON_PREFIX = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_PREFIX) && !is_string($pERSON_PREFIX)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_PREFIX)), __LINE__);
        }
        $this->PERSON_PREFIX = $pERSON_PREFIX;
        return $this;
    }
    /**
     * Get PERSON_SURNAME_1 value
     * @return string|null
     */
    public function getPERSON_SURNAME_1()
    {
        return $this->PERSON_SURNAME_1;
    }
    /**
     * Set PERSON_SURNAME_1 value
     * @param string $pERSON_SURNAME_1
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_SURNAME_1($pERSON_SURNAME_1 = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_SURNAME_1) && !is_string($pERSON_SURNAME_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_SURNAME_1)), __LINE__);
        }
        $this->PERSON_SURNAME_1 = $pERSON_SURNAME_1;
        return $this;
    }
    /**
     * Get PERSON_SURNAME_2 value
     * @return string|null
     */
    public function getPERSON_SURNAME_2()
    {
        return $this->PERSON_SURNAME_2;
    }
    /**
     * Set PERSON_SURNAME_2 value
     * @param string $pERSON_SURNAME_2
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_SURNAME_2($pERSON_SURNAME_2 = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_SURNAME_2) && !is_string($pERSON_SURNAME_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_SURNAME_2)), __LINE__);
        }
        $this->PERSON_SURNAME_2 = $pERSON_SURNAME_2;
        return $this;
    }
    /**
     * Get PERSON_TELEPHONE value
     * @return string|null
     */
    public function getPERSON_TELEPHONE()
    {
        return $this->PERSON_TELEPHONE;
    }
    /**
     * Set PERSON_TELEPHONE value
     * @param string $pERSON_TELEPHONE
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_TELEPHONE($pERSON_TELEPHONE = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_TELEPHONE) && !is_string($pERSON_TELEPHONE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_TELEPHONE)), __LINE__);
        }
        $this->PERSON_TELEPHONE = $pERSON_TELEPHONE;
        return $this;
    }
    /**
     * Get PERSON_TELEPHONE_GSM value
     * @return string|null
     */
    public function getPERSON_TELEPHONE_GSM()
    {
        return $this->PERSON_TELEPHONE_GSM;
    }
    /**
     * Set PERSON_TELEPHONE_GSM value
     * @param string $pERSON_TELEPHONE_GSM
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_TELEPHONE_GSM($pERSON_TELEPHONE_GSM = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_TELEPHONE_GSM) && !is_string($pERSON_TELEPHONE_GSM)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_TELEPHONE_GSM)), __LINE__);
        }
        $this->PERSON_TELEPHONE_GSM = $pERSON_TELEPHONE_GSM;
        return $this;
    }
    /**
     * Get PERSON_TITLE_ID value
     * @return string|null
     */
    public function getPERSON_TITLE_ID()
    {
        return $this->PERSON_TITLE_ID;
    }
    /**
     * Set PERSON_TITLE_ID value
     * @param string $pERSON_TITLE_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setPERSON_TITLE_ID($pERSON_TITLE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_TITLE_ID) && !is_string($pERSON_TITLE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_TITLE_ID)), __LINE__);
        }
        $this->PERSON_TITLE_ID = $pERSON_TITLE_ID;
        return $this;
    }
    /**
     * Get RELATION_DUN_DATE value
     * @return string|null
     */
    public function getRELATION_DUN_DATE()
    {
        return $this->RELATION_DUN_DATE;
    }
    /**
     * Set RELATION_DUN_DATE value
     * @param string $rELATION_DUN_DATE
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_DUN_DATE($rELATION_DUN_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_DUN_DATE) && !is_string($rELATION_DUN_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_DUN_DATE)), __LINE__);
        }
        $this->RELATION_DUN_DATE = $rELATION_DUN_DATE;
        return $this;
    }
    /**
     * Get RELATION_DUN_STATUS value
     * @return int|null
     */
    public function getRELATION_DUN_STATUS()
    {
        return $this->RELATION_DUN_STATUS;
    }
    /**
     * Set RELATION_DUN_STATUS value
     * @param int $rELATION_DUN_STATUS
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_DUN_STATUS($rELATION_DUN_STATUS = null)
    {
        // validation for constraint: int
        if (!is_null($rELATION_DUN_STATUS) && !is_numeric($rELATION_DUN_STATUS)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($rELATION_DUN_STATUS)), __LINE__);
        }
        $this->RELATION_DUN_STATUS = $rELATION_DUN_STATUS;
        return $this;
    }
    /**
     * Get RELATION_ID value
     * @return int|null
     */
    public function getRELATION_ID()
    {
        return $this->RELATION_ID;
    }
    /**
     * Set RELATION_ID value
     * @param int $rELATION_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_ID($rELATION_ID = null)
    {
        // validation for constraint: int
        if (!is_null($rELATION_ID) && !is_numeric($rELATION_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($rELATION_ID)), __LINE__);
        }
        $this->RELATION_ID = $rELATION_ID;
        return $this;
    }
    /**
     * Get RELATION_NAME_1 value
     * @return string|null
     */
    public function getRELATION_NAME_1()
    {
        return $this->RELATION_NAME_1;
    }
    /**
     * Set RELATION_NAME_1 value
     * @param string $rELATION_NAME_1
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_NAME_1($rELATION_NAME_1 = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_NAME_1) && !is_string($rELATION_NAME_1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_NAME_1)), __LINE__);
        }
        $this->RELATION_NAME_1 = $rELATION_NAME_1;
        return $this;
    }
    /**
     * Get RELATION_NAME_2 value
     * @return string|null
     */
    public function getRELATION_NAME_2()
    {
        return $this->RELATION_NAME_2;
    }
    /**
     * Set RELATION_NAME_2 value
     * @param string $rELATION_NAME_2
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_NAME_2($rELATION_NAME_2 = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_NAME_2) && !is_string($rELATION_NAME_2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_NAME_2)), __LINE__);
        }
        $this->RELATION_NAME_2 = $rELATION_NAME_2;
        return $this;
    }
    /**
     * Get RELATION_PAYACCOUNT_ID value
     * @return string|null
     */
    public function getRELATION_PAYACCOUNT_ID()
    {
        return $this->RELATION_PAYACCOUNT_ID;
    }
    /**
     * Set RELATION_PAYACCOUNT_ID value
     * @param string $rELATION_PAYACCOUNT_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_PAYACCOUNT_ID($rELATION_PAYACCOUNT_ID = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_PAYACCOUNT_ID) && !is_string($rELATION_PAYACCOUNT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_PAYACCOUNT_ID)), __LINE__);
        }
        $this->RELATION_PAYACCOUNT_ID = $rELATION_PAYACCOUNT_ID;
        return $this;
    }
    /**
     * Get RELATION_SOURCE_ID value
     * @return string|null
     */
    public function getRELATION_SOURCE_ID()
    {
        return $this->RELATION_SOURCE_ID;
    }
    /**
     * Set RELATION_SOURCE_ID value
     * @param string $rELATION_SOURCE_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_SOURCE_ID($rELATION_SOURCE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_SOURCE_ID) && !is_string($rELATION_SOURCE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_SOURCE_ID)), __LINE__);
        }
        $this->RELATION_SOURCE_ID = $rELATION_SOURCE_ID;
        return $this;
    }
    /**
     * Get RELATION_SUBPROFILE_ID value
     * @return string|null
     */
    public function getRELATION_SUBPROFILE_ID()
    {
        return $this->RELATION_SUBPROFILE_ID;
    }
    /**
     * Set RELATION_SUBPROFILE_ID value
     * @param string $rELATION_SUBPROFILE_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_SUBPROFILE_ID($rELATION_SUBPROFILE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_SUBPROFILE_ID) && !is_string($rELATION_SUBPROFILE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_SUBPROFILE_ID)), __LINE__);
        }
        $this->RELATION_SUBPROFILE_ID = $rELATION_SUBPROFILE_ID;
        return $this;
    }
    /**
     * Get RELATION_UDPROFILE_ID value
     * @return string|null
     */
    public function getRELATION_UDPROFILE_ID()
    {
        return $this->RELATION_UDPROFILE_ID;
    }
    /**
     * Set RELATION_UDPROFILE_ID value
     * @param string $rELATION_UDPROFILE_ID
     * @return \StructType\TCONTACTFULL
     */
    public function setRELATION_UDPROFILE_ID($rELATION_UDPROFILE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($rELATION_UDPROFILE_ID) && !is_string($rELATION_UDPROFILE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($rELATION_UDPROFILE_ID)), __LINE__);
        }
        $this->RELATION_UDPROFILE_ID = $rELATION_UDPROFILE_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TCONTACTFULL
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
