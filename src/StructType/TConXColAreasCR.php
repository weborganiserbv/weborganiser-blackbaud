<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TConXColAreasCR StructType
 * @subpackage Structs
 */
class TConXColAreasCR extends AbstractStructBase
{
    /**
     * The ConXColAreas
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TConXColArea[]
     * - ref: soapenc:arrayType
     * @var \StructType\TConXColArea[]
     */
    public $ConXColAreas;
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * Constructor method for TConXColAreasCR
     * @uses TConXColAreasCR::setConXColAreas()
     * @uses TConXColAreasCR::setERROR_ID()
     * @param \StructType\TConXColArea[] $conXColAreas
     * @param int $eRROR_ID
     */
    public function __construct(array $conXColAreas = array(), $eRROR_ID = null)
    {
        $this
            ->setConXColAreas($conXColAreas)
            ->setERROR_ID($eRROR_ID);
    }
    /**
     * Get ConXColAreas value
     * @return \StructType\TConXColArea[]|null
     */
    public function getConXColAreas()
    {
        return $this->ConXColAreas;
    }
    /**
     * Set ConXColAreas value
     * @throws \InvalidArgumentException
     * @param \StructType\TConXColArea[] $conXColAreas
     * @return \StructType\TConXColAreasCR
     */
    public function setConXColAreas(array $conXColAreas = array())
    {
        foreach ($conXColAreas as $tConXColAreasCRConXColAreasItem) {
            // validation for constraint: itemType
            if (!$tConXColAreasCRConXColAreasItem instanceof \StructType\TConXColArea) {
                throw new \InvalidArgumentException(sprintf('The ConXColAreas property can only contain items of \StructType\TConXColArea, "%s" given', is_object($tConXColAreasCRConXColAreasItem) ? get_class($tConXColAreasCRConXColAreasItem) : gettype($tConXColAreasCRConXColAreasItem)), __LINE__);
            }
        }
        $this->ConXColAreas = $conXColAreas;
        return $this;
    }
    /**
     * Add item to ConXColAreas value
     * @throws \InvalidArgumentException
     * @param \StructType\TConXColArea $item
     * @return \StructType\TConXColAreasCR
     */
    public function addToConXColAreas(\StructType\TConXColArea $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TConXColArea) {
            throw new \InvalidArgumentException(sprintf('The ConXColAreas property can only contain items of \StructType\TConXColArea, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->ConXColAreas[] = $item;
        return $this;
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TConXColAreasCR
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TConXColAreasCR
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
