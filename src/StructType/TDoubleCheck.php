<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TDoubleCheck StructType
 * @subpackage Structs
 */
class TDoubleCheck extends AbstractStructBase
{
    /**
     * The HOUSENO
     * @var string
     */
    public $HOUSENO;
    /**
     * The INITIALS
     * @var string
     */
    public $INITIALS;
    /**
     * The PERSON_BIRTH_DATE
     * @var string
     */
    public $PERSON_BIRTH_DATE;
    /**
     * The POSTCODE
     * @var string
     */
    public $POSTCODE;
    /**
     * The SURNAME
     * @var string
     */
    public $SURNAME;
    /**
     * Constructor method for TDoubleCheck
     * @uses TDoubleCheck::setHOUSENO()
     * @uses TDoubleCheck::setINITIALS()
     * @uses TDoubleCheck::setPERSON_BIRTH_DATE()
     * @uses TDoubleCheck::setPOSTCODE()
     * @uses TDoubleCheck::setSURNAME()
     * @param string $hOUSENO
     * @param string $iNITIALS
     * @param string $pERSON_BIRTH_DATE
     * @param string $pOSTCODE
     * @param string $sURNAME
     */
    public function __construct($hOUSENO = null, $iNITIALS = null, $pERSON_BIRTH_DATE = null, $pOSTCODE = null, $sURNAME = null)
    {
        $this
            ->setHOUSENO($hOUSENO)
            ->setINITIALS($iNITIALS)
            ->setPERSON_BIRTH_DATE($pERSON_BIRTH_DATE)
            ->setPOSTCODE($pOSTCODE)
            ->setSURNAME($sURNAME);
    }
    /**
     * Get HOUSENO value
     * @return string|null
     */
    public function getHOUSENO()
    {
        return $this->HOUSENO;
    }
    /**
     * Set HOUSENO value
     * @param string $hOUSENO
     * @return \StructType\TDoubleCheck
     */
    public function setHOUSENO($hOUSENO = null)
    {
        // validation for constraint: string
        if (!is_null($hOUSENO) && !is_string($hOUSENO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($hOUSENO)), __LINE__);
        }
        $this->HOUSENO = $hOUSENO;
        return $this;
    }
    /**
     * Get INITIALS value
     * @return string|null
     */
    public function getINITIALS()
    {
        return $this->INITIALS;
    }
    /**
     * Set INITIALS value
     * @param string $iNITIALS
     * @return \StructType\TDoubleCheck
     */
    public function setINITIALS($iNITIALS = null)
    {
        // validation for constraint: string
        if (!is_null($iNITIALS) && !is_string($iNITIALS)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($iNITIALS)), __LINE__);
        }
        $this->INITIALS = $iNITIALS;
        return $this;
    }
    /**
     * Get PERSON_BIRTH_DATE value
     * @return string|null
     */
    public function getPERSON_BIRTH_DATE()
    {
        return $this->PERSON_BIRTH_DATE;
    }
    /**
     * Set PERSON_BIRTH_DATE value
     * @param string $pERSON_BIRTH_DATE
     * @return \StructType\TDoubleCheck
     */
    public function setPERSON_BIRTH_DATE($pERSON_BIRTH_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($pERSON_BIRTH_DATE) && !is_string($pERSON_BIRTH_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERSON_BIRTH_DATE)), __LINE__);
        }
        $this->PERSON_BIRTH_DATE = $pERSON_BIRTH_DATE;
        return $this;
    }
    /**
     * Get POSTCODE value
     * @return string|null
     */
    public function getPOSTCODE()
    {
        return $this->POSTCODE;
    }
    /**
     * Set POSTCODE value
     * @param string $pOSTCODE
     * @return \StructType\TDoubleCheck
     */
    public function setPOSTCODE($pOSTCODE = null)
    {
        // validation for constraint: string
        if (!is_null($pOSTCODE) && !is_string($pOSTCODE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pOSTCODE)), __LINE__);
        }
        $this->POSTCODE = $pOSTCODE;
        return $this;
    }
    /**
     * Get SURNAME value
     * @return string|null
     */
    public function getSURNAME()
    {
        return $this->SURNAME;
    }
    /**
     * Set SURNAME value
     * @param string $sURNAME
     * @return \StructType\TDoubleCheck
     */
    public function setSURNAME($sURNAME = null)
    {
        // validation for constraint: string
        if (!is_null($sURNAME) && !is_string($sURNAME)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($sURNAME)), __LINE__);
        }
        $this->SURNAME = $sURNAME;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TDoubleCheck
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
