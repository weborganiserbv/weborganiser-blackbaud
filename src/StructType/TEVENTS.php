<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TEVENTS StructType
 * @subpackage Structs
 */
class TEVENTS extends AbstractStructBase
{
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * The EVENTS
     * Meta informations extracted from the WSDL
     * - arrayType: ns1:TEVENT[]
     * - ref: soapenc:arrayType
     * @var \StructType\TEVENT[]
     */
    public $EVENTS;
    /**
     * Constructor method for TEVENTS
     * @uses TEVENTS::setERROR_ID()
     * @uses TEVENTS::setEVENTS()
     * @param int $eRROR_ID
     * @param \StructType\TEVENT[] $eVENTS
     */
    public function __construct($eRROR_ID = null, array $eVENTS = array())
    {
        $this
            ->setERROR_ID($eRROR_ID)
            ->setEVENTS($eVENTS);
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TEVENTS
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Get EVENTS value
     * @return \StructType\TEVENT[]|null
     */
    public function getEVENTS()
    {
        return $this->EVENTS;
    }
    /**
     * Set EVENTS value
     * @throws \InvalidArgumentException
     * @param \StructType\TEVENT[] $eVENTS
     * @return \StructType\TEVENTS
     */
    public function setEVENTS(array $eVENTS = array())
    {
        foreach ($eVENTS as $tEVENTSEVENTSItem) {
            // validation for constraint: itemType
            if (!$tEVENTSEVENTSItem instanceof \StructType\TEVENT) {
                throw new \InvalidArgumentException(sprintf('The EVENTS property can only contain items of \StructType\TEVENT, "%s" given', is_object($tEVENTSEVENTSItem) ? get_class($tEVENTSEVENTSItem) : gettype($tEVENTSEVENTSItem)), __LINE__);
            }
        }
        $this->EVENTS = $eVENTS;
        return $this;
    }
    /**
     * Add item to EVENTS value
     * @throws \InvalidArgumentException
     * @param \StructType\TEVENT $item
     * @return \StructType\TEVENTS
     */
    public function addToEVENTS(\StructType\TEVENT $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\TEVENT) {
            throw new \InvalidArgumentException(sprintf('The EVENTS property can only contain items of \StructType\TEVENT, "%s" given', is_object($item) ? get_class($item) : gettype($item)), __LINE__);
        }
        $this->EVENTS[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TEVENTS
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
