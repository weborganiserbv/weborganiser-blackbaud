<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TPERIODICAL StructType
 * @subpackage Structs
 */
class TPERIODICAL extends AbstractStructBase
{
    /**
     * The PERIODICAL_DESCR
     * @var string
     */
    public $PERIODICAL_DESCR;
    /**
     * The PERIODICAL_ID
     * @var string
     */
    public $PERIODICAL_ID;
    /**
     * Constructor method for TPERIODICAL
     * @uses TPERIODICAL::setPERIODICAL_DESCR()
     * @uses TPERIODICAL::setPERIODICAL_ID()
     * @param string $pERIODICAL_DESCR
     * @param string $pERIODICAL_ID
     */
    public function __construct($pERIODICAL_DESCR = null, $pERIODICAL_ID = null)
    {
        $this
            ->setPERIODICAL_DESCR($pERIODICAL_DESCR)
            ->setPERIODICAL_ID($pERIODICAL_ID);
    }
    /**
     * Get PERIODICAL_DESCR value
     * @return string|null
     */
    public function getPERIODICAL_DESCR()
    {
        return $this->PERIODICAL_DESCR;
    }
    /**
     * Set PERIODICAL_DESCR value
     * @param string $pERIODICAL_DESCR
     * @return \StructType\TPERIODICAL
     */
    public function setPERIODICAL_DESCR($pERIODICAL_DESCR = null)
    {
        // validation for constraint: string
        if (!is_null($pERIODICAL_DESCR) && !is_string($pERIODICAL_DESCR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERIODICAL_DESCR)), __LINE__);
        }
        $this->PERIODICAL_DESCR = $pERIODICAL_DESCR;
        return $this;
    }
    /**
     * Get PERIODICAL_ID value
     * @return string|null
     */
    public function getPERIODICAL_ID()
    {
        return $this->PERIODICAL_ID;
    }
    /**
     * Set PERIODICAL_ID value
     * @param string $pERIODICAL_ID
     * @return \StructType\TPERIODICAL
     */
    public function setPERIODICAL_ID($pERIODICAL_ID = null)
    {
        // validation for constraint: string
        if (!is_null($pERIODICAL_ID) && !is_string($pERIODICAL_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($pERIODICAL_ID)), __LINE__);
        }
        $this->PERIODICAL_ID = $pERIODICAL_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TPERIODICAL
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
