<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TColAreaCR StructType
 * @subpackage Structs
 */
class TColAreaCR extends AbstractStructBase
{
    /**
     * The ColArea
     * @var \StructType\TColArea
     */
    public $ColArea;
    /**
     * The ERROR_ID
     * @var int
     */
    public $ERROR_ID;
    /**
     * Constructor method for TColAreaCR
     * @uses TColAreaCR::setColArea()
     * @uses TColAreaCR::setERROR_ID()
     * @param \StructType\TColArea $colArea
     * @param int $eRROR_ID
     */
    public function __construct(\StructType\TColArea $colArea = null, $eRROR_ID = null)
    {
        $this
            ->setColArea($colArea)
            ->setERROR_ID($eRROR_ID);
    }
    /**
     * Get ColArea value
     * @return \StructType\TColArea|null
     */
    public function getColArea()
    {
        return $this->ColArea;
    }
    /**
     * Set ColArea value
     * @param \StructType\TColArea $colArea
     * @return \StructType\TColAreaCR
     */
    public function setColArea(\StructType\TColArea $colArea = null)
    {
        $this->ColArea = $colArea;
        return $this;
    }
    /**
     * Get ERROR_ID value
     * @return int|null
     */
    public function getERROR_ID()
    {
        return $this->ERROR_ID;
    }
    /**
     * Set ERROR_ID value
     * @param int $eRROR_ID
     * @return \StructType\TColAreaCR
     */
    public function setERROR_ID($eRROR_ID = null)
    {
        // validation for constraint: int
        if (!is_null($eRROR_ID) && !is_numeric($eRROR_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($eRROR_ID)), __LINE__);
        }
        $this->ERROR_ID = $eRROR_ID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TColAreaCR
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
