<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TNEWADDRESS StructType
 * @subpackage Structs
 */
class TNEWADDRESS extends AbstractStructBase
{
    /**
     * The NEWADDRESS_CITY
     * @var string
     */
    public $NEWADDRESS_CITY;
    /**
     * The NEWADDRESS_CONTACT_ID
     * @var int
     */
    public $NEWADDRESS_CONTACT_ID;
    /**
     * The NEWADDRESS_COUNTRY_ID
     * @var string
     */
    public $NEWADDRESS_COUNTRY_ID;
    /**
     * The NEWADDRESS_HOUSE_NO
     * @var string
     */
    public $NEWADDRESS_HOUSE_NO;
    /**
     * The NEWADDRESS_HOUSE_NO_ADD
     * @var string
     */
    public $NEWADDRESS_HOUSE_NO_ADD;
    /**
     * The NEWADDRESS_ID
     * @var int
     */
    public $NEWADDRESS_ID;
    /**
     * The NEWADDRESS_NEWADDRESSTYPE_ID
     * @var string
     */
    public $NEWADDRESS_NEWADDRESSTYPE_ID;
    /**
     * The NEWADDRESS_POSTCODE
     * @var string
     */
    public $NEWADDRESS_POSTCODE;
    /**
     * The NEWADDRESS_START_DATE
     * @var string
     */
    public $NEWADDRESS_START_DATE;
    /**
     * The NEWADDRESS_STREET
     * @var string
     */
    public $NEWADDRESS_STREET;
    /**
     * Constructor method for TNEWADDRESS
     * @uses TNEWADDRESS::setNEWADDRESS_CITY()
     * @uses TNEWADDRESS::setNEWADDRESS_CONTACT_ID()
     * @uses TNEWADDRESS::setNEWADDRESS_COUNTRY_ID()
     * @uses TNEWADDRESS::setNEWADDRESS_HOUSE_NO()
     * @uses TNEWADDRESS::setNEWADDRESS_HOUSE_NO_ADD()
     * @uses TNEWADDRESS::setNEWADDRESS_ID()
     * @uses TNEWADDRESS::setNEWADDRESS_NEWADDRESSTYPE_ID()
     * @uses TNEWADDRESS::setNEWADDRESS_POSTCODE()
     * @uses TNEWADDRESS::setNEWADDRESS_START_DATE()
     * @uses TNEWADDRESS::setNEWADDRESS_STREET()
     * @param string $nEWADDRESS_CITY
     * @param int $nEWADDRESS_CONTACT_ID
     * @param string $nEWADDRESS_COUNTRY_ID
     * @param string $nEWADDRESS_HOUSE_NO
     * @param string $nEWADDRESS_HOUSE_NO_ADD
     * @param int $nEWADDRESS_ID
     * @param string $nEWADDRESS_NEWADDRESSTYPE_ID
     * @param string $nEWADDRESS_POSTCODE
     * @param string $nEWADDRESS_START_DATE
     * @param string $nEWADDRESS_STREET
     */
    public function __construct($nEWADDRESS_CITY = null, $nEWADDRESS_CONTACT_ID = null, $nEWADDRESS_COUNTRY_ID = null, $nEWADDRESS_HOUSE_NO = null, $nEWADDRESS_HOUSE_NO_ADD = null, $nEWADDRESS_ID = null, $nEWADDRESS_NEWADDRESSTYPE_ID = null, $nEWADDRESS_POSTCODE = null, $nEWADDRESS_START_DATE = null, $nEWADDRESS_STREET = null)
    {
        $this
            ->setNEWADDRESS_CITY($nEWADDRESS_CITY)
            ->setNEWADDRESS_CONTACT_ID($nEWADDRESS_CONTACT_ID)
            ->setNEWADDRESS_COUNTRY_ID($nEWADDRESS_COUNTRY_ID)
            ->setNEWADDRESS_HOUSE_NO($nEWADDRESS_HOUSE_NO)
            ->setNEWADDRESS_HOUSE_NO_ADD($nEWADDRESS_HOUSE_NO_ADD)
            ->setNEWADDRESS_ID($nEWADDRESS_ID)
            ->setNEWADDRESS_NEWADDRESSTYPE_ID($nEWADDRESS_NEWADDRESSTYPE_ID)
            ->setNEWADDRESS_POSTCODE($nEWADDRESS_POSTCODE)
            ->setNEWADDRESS_START_DATE($nEWADDRESS_START_DATE)
            ->setNEWADDRESS_STREET($nEWADDRESS_STREET);
    }
    /**
     * Get NEWADDRESS_CITY value
     * @return string|null
     */
    public function getNEWADDRESS_CITY()
    {
        return $this->NEWADDRESS_CITY;
    }
    /**
     * Set NEWADDRESS_CITY value
     * @param string $nEWADDRESS_CITY
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_CITY($nEWADDRESS_CITY = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_CITY) && !is_string($nEWADDRESS_CITY)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_CITY)), __LINE__);
        }
        $this->NEWADDRESS_CITY = $nEWADDRESS_CITY;
        return $this;
    }
    /**
     * Get NEWADDRESS_CONTACT_ID value
     * @return int|null
     */
    public function getNEWADDRESS_CONTACT_ID()
    {
        return $this->NEWADDRESS_CONTACT_ID;
    }
    /**
     * Set NEWADDRESS_CONTACT_ID value
     * @param int $nEWADDRESS_CONTACT_ID
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_CONTACT_ID($nEWADDRESS_CONTACT_ID = null)
    {
        // validation for constraint: int
        if (!is_null($nEWADDRESS_CONTACT_ID) && !is_numeric($nEWADDRESS_CONTACT_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($nEWADDRESS_CONTACT_ID)), __LINE__);
        }
        $this->NEWADDRESS_CONTACT_ID = $nEWADDRESS_CONTACT_ID;
        return $this;
    }
    /**
     * Get NEWADDRESS_COUNTRY_ID value
     * @return string|null
     */
    public function getNEWADDRESS_COUNTRY_ID()
    {
        return $this->NEWADDRESS_COUNTRY_ID;
    }
    /**
     * Set NEWADDRESS_COUNTRY_ID value
     * @param string $nEWADDRESS_COUNTRY_ID
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_COUNTRY_ID($nEWADDRESS_COUNTRY_ID = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_COUNTRY_ID) && !is_string($nEWADDRESS_COUNTRY_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_COUNTRY_ID)), __LINE__);
        }
        $this->NEWADDRESS_COUNTRY_ID = $nEWADDRESS_COUNTRY_ID;
        return $this;
    }
    /**
     * Get NEWADDRESS_HOUSE_NO value
     * @return string|null
     */
    public function getNEWADDRESS_HOUSE_NO()
    {
        return $this->NEWADDRESS_HOUSE_NO;
    }
    /**
     * Set NEWADDRESS_HOUSE_NO value
     * @param string $nEWADDRESS_HOUSE_NO
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_HOUSE_NO($nEWADDRESS_HOUSE_NO = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_HOUSE_NO) && !is_string($nEWADDRESS_HOUSE_NO)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_HOUSE_NO)), __LINE__);
        }
        $this->NEWADDRESS_HOUSE_NO = $nEWADDRESS_HOUSE_NO;
        return $this;
    }
    /**
     * Get NEWADDRESS_HOUSE_NO_ADD value
     * @return string|null
     */
    public function getNEWADDRESS_HOUSE_NO_ADD()
    {
        return $this->NEWADDRESS_HOUSE_NO_ADD;
    }
    /**
     * Set NEWADDRESS_HOUSE_NO_ADD value
     * @param string $nEWADDRESS_HOUSE_NO_ADD
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_HOUSE_NO_ADD($nEWADDRESS_HOUSE_NO_ADD = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_HOUSE_NO_ADD) && !is_string($nEWADDRESS_HOUSE_NO_ADD)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_HOUSE_NO_ADD)), __LINE__);
        }
        $this->NEWADDRESS_HOUSE_NO_ADD = $nEWADDRESS_HOUSE_NO_ADD;
        return $this;
    }
    /**
     * Get NEWADDRESS_ID value
     * @return int|null
     */
    public function getNEWADDRESS_ID()
    {
        return $this->NEWADDRESS_ID;
    }
    /**
     * Set NEWADDRESS_ID value
     * @param int $nEWADDRESS_ID
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_ID($nEWADDRESS_ID = null)
    {
        // validation for constraint: int
        if (!is_null($nEWADDRESS_ID) && !is_numeric($nEWADDRESS_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a numeric value, "%s" given', gettype($nEWADDRESS_ID)), __LINE__);
        }
        $this->NEWADDRESS_ID = $nEWADDRESS_ID;
        return $this;
    }
    /**
     * Get NEWADDRESS_NEWADDRESSTYPE_ID value
     * @return string|null
     */
    public function getNEWADDRESS_NEWADDRESSTYPE_ID()
    {
        return $this->NEWADDRESS_NEWADDRESSTYPE_ID;
    }
    /**
     * Set NEWADDRESS_NEWADDRESSTYPE_ID value
     * @param string $nEWADDRESS_NEWADDRESSTYPE_ID
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_NEWADDRESSTYPE_ID($nEWADDRESS_NEWADDRESSTYPE_ID = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_NEWADDRESSTYPE_ID) && !is_string($nEWADDRESS_NEWADDRESSTYPE_ID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_NEWADDRESSTYPE_ID)), __LINE__);
        }
        $this->NEWADDRESS_NEWADDRESSTYPE_ID = $nEWADDRESS_NEWADDRESSTYPE_ID;
        return $this;
    }
    /**
     * Get NEWADDRESS_POSTCODE value
     * @return string|null
     */
    public function getNEWADDRESS_POSTCODE()
    {
        return $this->NEWADDRESS_POSTCODE;
    }
    /**
     * Set NEWADDRESS_POSTCODE value
     * @param string $nEWADDRESS_POSTCODE
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_POSTCODE($nEWADDRESS_POSTCODE = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_POSTCODE) && !is_string($nEWADDRESS_POSTCODE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_POSTCODE)), __LINE__);
        }
        $this->NEWADDRESS_POSTCODE = $nEWADDRESS_POSTCODE;
        return $this;
    }
    /**
     * Get NEWADDRESS_START_DATE value
     * @return string|null
     */
    public function getNEWADDRESS_START_DATE()
    {
        return $this->NEWADDRESS_START_DATE;
    }
    /**
     * Set NEWADDRESS_START_DATE value
     * @param string $nEWADDRESS_START_DATE
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_START_DATE($nEWADDRESS_START_DATE = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_START_DATE) && !is_string($nEWADDRESS_START_DATE)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_START_DATE)), __LINE__);
        }
        $this->NEWADDRESS_START_DATE = $nEWADDRESS_START_DATE;
        return $this;
    }
    /**
     * Get NEWADDRESS_STREET value
     * @return string|null
     */
    public function getNEWADDRESS_STREET()
    {
        return $this->NEWADDRESS_STREET;
    }
    /**
     * Set NEWADDRESS_STREET value
     * @param string $nEWADDRESS_STREET
     * @return \StructType\TNEWADDRESS
     */
    public function setNEWADDRESS_STREET($nEWADDRESS_STREET = null)
    {
        // validation for constraint: string
        if (!is_null($nEWADDRESS_STREET) && !is_string($nEWADDRESS_STREET)) {
            throw new \InvalidArgumentException(sprintf('Invalid value, please provide a string, "%s" given', gettype($nEWADDRESS_STREET)), __LINE__);
        }
        $this->NEWADDRESS_STREET = $nEWADDRESS_STREET;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TNEWADDRESS
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
