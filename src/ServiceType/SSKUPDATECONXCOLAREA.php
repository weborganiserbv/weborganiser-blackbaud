<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKUPDATECONXCOLAREA ServiceType
 * @subpackage Services
 */
class SSKUPDATECONXCOLAREA extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_UPDATE_CONXCOLAREA
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param \StructType\TConXColArea $cONXCOLAREA
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_UPDATE_CONXCOLAREA($iN_CHECK_ONLY, \StructType\TConXColArea $cONXCOLAREA)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_UPDATE_CONXCOLAREA($iN_CHECK_ONLY, $cONXCOLAREA));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
