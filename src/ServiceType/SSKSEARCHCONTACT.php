<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKSEARCHCONTACT ServiceType
 * @subpackage Services
 */
class SSKSEARCHCONTACT extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_SEARCH_CONTACT
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\TRelationSearch $relationSearch
     * @return \StructType\TRelationSearchCR|bool
     */
    public function SSK_SEARCH_CONTACT(\StructType\TRelationSearch $relationSearch)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_SEARCH_CONTACT($relationSearch));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TRelationSearchCR
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
