<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTCONTACT ServiceType
 * @subpackage Services
 */
class SSKINSERTCONTACT extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_CONTACT
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $iN_RELATION_SOURCE_ID
     * @param string $iN_PERSON_TITLE_ID
     * @param string $iN_PERSON_INITIALS
     * @param string $iN_PERSON_FIRST_NAME
     * @param string $iN_PERSON_MIDDLE_NAME
     * @param string $iN_PERSON_PREFIX
     * @param string $iN_PERSON_SURNAME_1
     * @param string $iN_PERSON_TELEPHONE
     * @param string $iN_PERSON_TELEPHONE_GSM
     * @param string $iN_PERSON_EMAIL
     * @param string $iN_PERSON_BIRTH_DATE
     * @param string $iN_ADDRESS_POSTCODE
     * @param string $iN_ADDRESS_STREET
     * @param string $iN_ADDRESS_HOUSE_NO
     * @param string $iN_ADDRESS_HOUSE_NO_ADD
     * @param string $iN_ADDRESS_COUNTRY_ID
     * @param string $iN_ADDRESS_CITY
     * @param string $iN_RELATION_PAYACCOUNT_ID
     * @param string $iN_PERSON_USERNAME
     * @param string $iN_PERSON_PASSWORD
     * @param string $iN_RELATION_NAME_1
     * @param string $iN_RELATION_NAME_2
     * @param string $iN_RELATION_UDPROFILE_ID
     * @param string $iN_RELATION_SUBPROFILE_ID
     * @param string $iN_CONTACT_TELEPHONE
     * @param string $iN_CONTACT_EMAIL
     * @param string $iN_CONTACT_ENTRY_SALESMAN_ID
     * @param string $iN_FREE_X100_1
     * @param string $iN_FREE_X100_2
     * @param string $iN_FREE_X100_3
     * @param string $iN_FREE_X100_4
     * @param string $iN_FREE_X100_5
     * @param string $iN_FREE_X100_6
     * @param string $iN_FREE_X100_7
     * @param string $iN_FREE_X100_8
     * @param string $iN_FREE_X100_9
     * @param string $iN_FREE_X1000_10
     * @return \StructType\TCONTACTID|bool
     */
    public function SSK_INSERT_CONTACT($iN_CHECK_ONLY, $iN_RELATION_SOURCE_ID, $iN_PERSON_TITLE_ID, $iN_PERSON_INITIALS, $iN_PERSON_FIRST_NAME, $iN_PERSON_MIDDLE_NAME, $iN_PERSON_PREFIX, $iN_PERSON_SURNAME_1, $iN_PERSON_TELEPHONE, $iN_PERSON_TELEPHONE_GSM, $iN_PERSON_EMAIL, $iN_PERSON_BIRTH_DATE, $iN_ADDRESS_POSTCODE, $iN_ADDRESS_STREET, $iN_ADDRESS_HOUSE_NO, $iN_ADDRESS_HOUSE_NO_ADD, $iN_ADDRESS_COUNTRY_ID, $iN_ADDRESS_CITY, $iN_RELATION_PAYACCOUNT_ID, $iN_PERSON_USERNAME, $iN_PERSON_PASSWORD, $iN_RELATION_NAME_1, $iN_RELATION_NAME_2, $iN_RELATION_UDPROFILE_ID, $iN_RELATION_SUBPROFILE_ID, $iN_CONTACT_TELEPHONE, $iN_CONTACT_EMAIL, $iN_CONTACT_ENTRY_SALESMAN_ID, $iN_FREE_X100_1, $iN_FREE_X100_2, $iN_FREE_X100_3, $iN_FREE_X100_4, $iN_FREE_X100_5, $iN_FREE_X100_6, $iN_FREE_X100_7, $iN_FREE_X100_8, $iN_FREE_X100_9, $iN_FREE_X1000_10)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_CONTACT($iN_CHECK_ONLY, $iN_RELATION_SOURCE_ID, $iN_PERSON_TITLE_ID, $iN_PERSON_INITIALS, $iN_PERSON_FIRST_NAME, $iN_PERSON_MIDDLE_NAME, $iN_PERSON_PREFIX, $iN_PERSON_SURNAME_1, $iN_PERSON_TELEPHONE, $iN_PERSON_TELEPHONE_GSM, $iN_PERSON_EMAIL, $iN_PERSON_BIRTH_DATE, $iN_ADDRESS_POSTCODE, $iN_ADDRESS_STREET, $iN_ADDRESS_HOUSE_NO, $iN_ADDRESS_HOUSE_NO_ADD, $iN_ADDRESS_COUNTRY_ID, $iN_ADDRESS_CITY, $iN_RELATION_PAYACCOUNT_ID, $iN_PERSON_USERNAME, $iN_PERSON_PASSWORD, $iN_RELATION_NAME_1, $iN_RELATION_NAME_2, $iN_RELATION_UDPROFILE_ID, $iN_RELATION_SUBPROFILE_ID, $iN_CONTACT_TELEPHONE, $iN_CONTACT_EMAIL, $iN_CONTACT_ENTRY_SALESMAN_ID, $iN_FREE_X100_1, $iN_FREE_X100_2, $iN_FREE_X100_3, $iN_FREE_X100_4, $iN_FREE_X100_5, $iN_FREE_X100_6, $iN_FREE_X100_7, $iN_FREE_X100_8, $iN_FREE_X100_9, $iN_FREE_X1000_10));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCONTACTID
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
