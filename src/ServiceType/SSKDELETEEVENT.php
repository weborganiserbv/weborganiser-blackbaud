<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKDELETEEVENT ServiceType
 * @subpackage Services
 */
class SSKDELETEEVENT extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_DELETE_EVENT
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $eVCONXROLE_EVENT_ID
     * @param string $eVCONXROLE_CONTACT_ID
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_DELETE_EVENT($iN_CHECK_ONLY, $eVCONXROLE_EVENT_ID, $eVCONXROLE_CONTACT_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_DELETE_EVENT($iN_CHECK_ONLY, $eVCONXROLE_EVENT_ID, $eVCONXROLE_CONTACT_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
