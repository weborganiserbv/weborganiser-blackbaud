<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTPERIODICAL ServiceType
 * @subpackage Services
 */
class SSKINSERTPERIODICAL extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_PERIODICAL
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CONTACT_ID
     * @param string $iN_PERIODICAL_ID
     * @param string $iN_START_DATE
     * @param string $iN_END_DATE
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_INSERT_PERIODICAL($iN_CONTACT_ID, $iN_PERIODICAL_ID, $iN_START_DATE, $iN_END_DATE)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_PERIODICAL($iN_CONTACT_ID, $iN_PERIODICAL_ID, $iN_START_DATE, $iN_END_DATE));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
