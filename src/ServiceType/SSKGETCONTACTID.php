<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKGETCONTACTID ServiceType
 * @subpackage Services
 */
class SSKGETCONTACTID extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_GET_CONTACT_ID
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_TITLE_ID
     * @param string $iN_INITIALS
     * @param string $iN_FIRST_NAME
     * @param string $iN_SURNAME_1
     * @param string $iN_STREET
     * @param string $iN_HOUSE_NO
     * @param string $iN_HOUSE_NO_ADD
     * @param string $iN_COUNTRY_ID
     * @param string $iN_POSTCODE
     * @param string $iN_CITY
     * @param string $iN_PAYACCOUNT_ID
     * @param string $iN_BIRTH_DATE
     * @param string $iN_EMAIL
     * @param string $iN_TELEPHONE
     * @param string $iN_USERNAME
     * @param string $iN_PASSWORD
     * @param string $iN_FREE_X100_1
     * @param string $iN_FREE_X100_2
     * @param string $iN_FREE_X100_3
     * @param string $iN_FREE_X100_4
     * @param string $iN_FREE_X100_5
     * @return \StructType\TCONTACTID|bool
     */
    public function SSK_GET_CONTACT_ID($iN_TITLE_ID, $iN_INITIALS, $iN_FIRST_NAME, $iN_SURNAME_1, $iN_STREET, $iN_HOUSE_NO, $iN_HOUSE_NO_ADD, $iN_COUNTRY_ID, $iN_POSTCODE, $iN_CITY, $iN_PAYACCOUNT_ID, $iN_BIRTH_DATE, $iN_EMAIL, $iN_TELEPHONE, $iN_USERNAME, $iN_PASSWORD, $iN_FREE_X100_1, $iN_FREE_X100_2, $iN_FREE_X100_3, $iN_FREE_X100_4, $iN_FREE_X100_5)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_GET_CONTACT_ID($iN_TITLE_ID, $iN_INITIALS, $iN_FIRST_NAME, $iN_SURNAME_1, $iN_STREET, $iN_HOUSE_NO, $iN_HOUSE_NO_ADD, $iN_COUNTRY_ID, $iN_POSTCODE, $iN_CITY, $iN_PAYACCOUNT_ID, $iN_BIRTH_DATE, $iN_EMAIL, $iN_TELEPHONE, $iN_USERNAME, $iN_PASSWORD, $iN_FREE_X100_1, $iN_FREE_X100_2, $iN_FREE_X100_3, $iN_FREE_X100_4, $iN_FREE_X100_5));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCONTACTID
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
