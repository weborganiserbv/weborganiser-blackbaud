<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTPERSONATRELATION ServiceType
 * @subpackage Services
 */
class SSKINSERTPERSONATRELATION extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_PERSON_AT_RELATION
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $check_Only
     * @param \StructType\TPersonContact $contactPerson
     * @return \StructType\TPersonContactCR|bool
     */
    public function SSK_INSERT_PERSON_AT_RELATION($check_Only, \StructType\TPersonContact $contactPerson)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_PERSON_AT_RELATION($check_Only, $contactPerson));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TPersonContactCR
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
