<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKUPDATECOLAREA ServiceType
 * @subpackage Services
 */
class SSKUPDATECOLAREA extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_UPDATE_COLAREA
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param \StructType\TColArea $cOLAREA
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_UPDATE_COLAREA($iN_CHECK_ONLY, \StructType\TColArea $cOLAREA)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_UPDATE_COLAREA($iN_CHECK_ONLY, $cOLAREA));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
