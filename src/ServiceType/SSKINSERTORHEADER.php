<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTORHEADER ServiceType
 * @subpackage Services
 */
class SSKINSERTORHEADER extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_ORHEADER
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $oRHEADER_ID
     * @param string $oRHEADER_CONTACT_ID
     * @param string $oRHEADER_ENTRY_SALESMAN_ID
     * @param string $oRHEADER_DELIVERY_CONTACT_ID
     * @param string $oRHEADER_SHIPCODE_ID
     * @param string $oRHEADER_PREPAYM_REQ_FLAG
     * @param string $oRHEADER_MIN_DELIVERY_DATE
     * @param string $oRHEADER_MAX_DELIVERY_DATE
     * @param string $oRHEADER_MAX_BO_DELAY
     * @param string $oRHEADER_MEMO
     * @param string $oRHEADER_VAT_INCL_FLAG
     * @param string $oRHEADER_SOURCE_ID
     * @param string $oRHEADER_PAYACCOUNT_ID
     * @param string $oRHEADER_AUTOPAYCODE_ID
     * @param string $oRHEADER_FIXEDPAYCODE_ID
     * @param string $oRHEADER_COLAREA_ID
     * @param string $oRHEADER_ORDERKIND_ID
     * @param string $pAYMENT_AMOUNT
     * @param string $pAYMENT_PAYCODE_ID
     * @param string $iN_FREE_X100_1
     * @param string $iN_FREE_X100_2
     * @param string $iN_FREE_X100_3
     * @param string $iN_FREE_X100_4
     * @param string $iN_FREE_X100_5
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_INSERT_ORHEADER($iN_CHECK_ONLY, $oRHEADER_ID, $oRHEADER_CONTACT_ID, $oRHEADER_ENTRY_SALESMAN_ID, $oRHEADER_DELIVERY_CONTACT_ID, $oRHEADER_SHIPCODE_ID, $oRHEADER_PREPAYM_REQ_FLAG, $oRHEADER_MIN_DELIVERY_DATE, $oRHEADER_MAX_DELIVERY_DATE, $oRHEADER_MAX_BO_DELAY, $oRHEADER_MEMO, $oRHEADER_VAT_INCL_FLAG, $oRHEADER_SOURCE_ID, $oRHEADER_PAYACCOUNT_ID, $oRHEADER_AUTOPAYCODE_ID, $oRHEADER_FIXEDPAYCODE_ID, $oRHEADER_COLAREA_ID, $oRHEADER_ORDERKIND_ID, $pAYMENT_AMOUNT, $pAYMENT_PAYCODE_ID, $iN_FREE_X100_1, $iN_FREE_X100_2, $iN_FREE_X100_3, $iN_FREE_X100_4, $iN_FREE_X100_5)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_ORHEADER($iN_CHECK_ONLY, $oRHEADER_ID, $oRHEADER_CONTACT_ID, $oRHEADER_ENTRY_SALESMAN_ID, $oRHEADER_DELIVERY_CONTACT_ID, $oRHEADER_SHIPCODE_ID, $oRHEADER_PREPAYM_REQ_FLAG, $oRHEADER_MIN_DELIVERY_DATE, $oRHEADER_MAX_DELIVERY_DATE, $oRHEADER_MAX_BO_DELAY, $oRHEADER_MEMO, $oRHEADER_VAT_INCL_FLAG, $oRHEADER_SOURCE_ID, $oRHEADER_PAYACCOUNT_ID, $oRHEADER_AUTOPAYCODE_ID, $oRHEADER_FIXEDPAYCODE_ID, $oRHEADER_COLAREA_ID, $oRHEADER_ORDERKIND_ID, $pAYMENT_AMOUNT, $pAYMENT_PAYCODE_ID, $iN_FREE_X100_1, $iN_FREE_X100_2, $iN_FREE_X100_3, $iN_FREE_X100_4, $iN_FREE_X100_5));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
