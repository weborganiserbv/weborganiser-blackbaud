<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTNEWADDRESS ServiceType
 * @subpackage Services
 */
class SSKINSERTNEWADDRESS extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_NEWADDRESS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $nEWADDRESS_CONTACT_ID
     * @param string $nEWADDRESS_START_DATE
     * @param string $nEWADDRESS_NEWADDRESSTYPE_ID
     * @param string $nEWADDRESS_COUNTRY_ID
     * @param string $nEWADDRESS_STREET
     * @param string $nEWADDRESS_HOUSE_NO
     * @param string $nEWADDRESS_HOUSE_NO_ADD
     * @param string $nEWADDRESS_POSTCODE
     * @param string $nEWADDRESS_CITY
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_INSERT_NEWADDRESS($nEWADDRESS_CONTACT_ID, $nEWADDRESS_START_DATE, $nEWADDRESS_NEWADDRESSTYPE_ID, $nEWADDRESS_COUNTRY_ID, $nEWADDRESS_STREET, $nEWADDRESS_HOUSE_NO, $nEWADDRESS_HOUSE_NO_ADD, $nEWADDRESS_POSTCODE, $nEWADDRESS_CITY)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_NEWADDRESS($nEWADDRESS_CONTACT_ID, $nEWADDRESS_START_DATE, $nEWADDRESS_NEWADDRESSTYPE_ID, $nEWADDRESS_COUNTRY_ID, $nEWADDRESS_STREET, $nEWADDRESS_HOUSE_NO, $nEWADDRESS_HOUSE_NO_ADD, $nEWADDRESS_POSTCODE, $nEWADDRESS_CITY));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
