<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKUPDATESUBSCRIPTIONEND ServiceType
 * @subpackage Services
 */
class SSKUPDATESUBSCRIPTIONEND extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_UPDATE_SUBSCRIPTION_END
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $iN_CONTACT_ID
     * @param string $iN_SUBSCRIPTION_SUBCODE_ID
     * @param string $iN_SUBSCRIPTION_SUBQUITREASON_ID
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_UPDATE_SUBSCRIPTION_END($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_SUBSCRIPTION_SUBCODE_ID, $iN_SUBSCRIPTION_SUBQUITREASON_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_UPDATE_SUBSCRIPTION_END($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_SUBSCRIPTION_SUBCODE_ID, $iN_SUBSCRIPTION_SUBQUITREASON_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
