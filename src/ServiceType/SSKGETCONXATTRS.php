<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKGETCONXATTRS ServiceType
 * @subpackage Services
 */
class SSKGETCONXATTRS extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_GET_CONXATTRS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $cONTACT_ID
     * @return \StructType\TConxAttrsCR|bool
     */
    public function SSK_GET_CONXATTRS($cONTACT_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_GET_CONXATTRS($cONTACT_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TConxAttrsCR
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
