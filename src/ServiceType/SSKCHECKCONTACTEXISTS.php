<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKCHECKCONTACTEXISTS ServiceType
 * @subpackage Services
 */
class SSKCHECKCONTACTEXISTS extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_CHECK_CONTACT_EXISTS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\TDoubleCheck $doubleCheck
     * @return \StructType\TCONTACTID|bool
     */
    public function SSK_CHECK_CONTACT_EXISTS(\StructType\TDoubleCheck $doubleCheck)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_CHECK_CONTACT_EXISTS($doubleCheck));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCONTACTID
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
