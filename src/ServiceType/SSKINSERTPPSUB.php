<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTPPSUB ServiceType
 * @subpackage Services
 */
class SSKINSERTPPSUB extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_PPSUB
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $iN_CONTACT_ID
     * @param string $iN_SOURCE_ID
     * @param string $iN_TERM_ID
     * @param string $iN_AMOUNT
     * @param string $iN_START_DATE
     * @param string $iN_END_DATE
     * @param string $iN_PAYACCOUNT_ID
     * @param string $iN_PROJECT_ID
     * @param string $iN_SALESMAN_ID
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_INSERT_PPSUB($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_SOURCE_ID, $iN_TERM_ID, $iN_AMOUNT, $iN_START_DATE, $iN_END_DATE, $iN_PAYACCOUNT_ID, $iN_PROJECT_ID, $iN_SALESMAN_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_PPSUB($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_SOURCE_ID, $iN_TERM_ID, $iN_AMOUNT, $iN_START_DATE, $iN_END_DATE, $iN_PAYACCOUNT_ID, $iN_PROJECT_ID, $iN_SALESMAN_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
