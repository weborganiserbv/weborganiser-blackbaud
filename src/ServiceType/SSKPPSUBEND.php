<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKPPSUBEND ServiceType
 * @subpackage Services
 */
class SSKPPSUBEND extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_PPSUB_END
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $iN_CONTACT_ID
     * @param string $iN_PPSUB_PPTERM_ID
     * @param string $iN_PPSUB_PPENDREASON_ID
     * @param string $iN_PPSUB_END_DATE
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_PPSUB_END($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_PPSUB_PPTERM_ID, $iN_PPSUB_PPENDREASON_ID, $iN_PPSUB_END_DATE)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_PPSUB_END($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_PPSUB_PPTERM_ID, $iN_PPSUB_PPENDREASON_ID, $iN_PPSUB_END_DATE));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
