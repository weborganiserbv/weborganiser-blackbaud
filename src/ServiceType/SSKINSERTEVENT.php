<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTEVENT ServiceType
 * @subpackage Services
 */
class SSKINSERTEVENT extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_EVENT
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $eVCONXROLE_SOURCE_ID
     * @param string $eVCONXROLE_EVENT_ID
     * @param string $eVCONXROLE_PAYACCOUNT_ID
     * @param string $eVCONXROLE_CONTACT_ID
     * @param string $eVCONXROLE_EVROLE_ID
     * @param string $eVCONXROLE_EVSTAT_ID
     * @param string $eVCONXROLE_EVSTAT_DATE
     * @param string $eVCONXROLE_MEMO
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_INSERT_EVENT($iN_CHECK_ONLY, $eVCONXROLE_SOURCE_ID, $eVCONXROLE_EVENT_ID, $eVCONXROLE_PAYACCOUNT_ID, $eVCONXROLE_CONTACT_ID, $eVCONXROLE_EVROLE_ID, $eVCONXROLE_EVSTAT_ID, $eVCONXROLE_EVSTAT_DATE, $eVCONXROLE_MEMO)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_EVENT($iN_CHECK_ONLY, $eVCONXROLE_SOURCE_ID, $eVCONXROLE_EVENT_ID, $eVCONXROLE_PAYACCOUNT_ID, $eVCONXROLE_CONTACT_ID, $eVCONXROLE_EVROLE_ID, $eVCONXROLE_EVSTAT_ID, $eVCONXROLE_EVSTAT_DATE, $eVCONXROLE_MEMO));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
