<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKGETPPSUBS ServiceType
 * @subpackage Services
 */
class SSKGETPPSUBS extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_GET_PPSUBS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CONTACT_ID
     * @return \StructType\TPPSUBS|bool
     */
    public function SSK_GET_PPSUBS($iN_CONTACT_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_GET_PPSUBS($iN_CONTACT_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TPPSUBS
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
