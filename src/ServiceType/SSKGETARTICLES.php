<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKGETARTICLES ServiceType
 * @subpackage Services
 */
class SSKGETARTICLES extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_GET_ARTICLES
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $aRTGROUP_ID
     * @return \StructType\TArticlesCR|bool
     */
    public function SSK_GET_ARTICLES($aRTGROUP_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_GET_ARTICLES($aRTGROUP_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TArticlesCR
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
