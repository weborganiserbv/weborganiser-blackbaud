<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTSSLINE ServiceType
 * @subpackage Services
 */
class SSKINSERTSSLINE extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_SSLINE
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $iN_CONTACT_ID
     * @param string $iN_SSLINE_SSLETTER_ID
     * @param string $iN_SSLINE_SOURCE_ID
     * @param string $sSLINE_SSSUBJECT_ID
     * @param string $sSLINE_SSSUBSUBJECT_ID
     * @param string $sSLINE_MEMO
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_INSERT_SSLINE($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_SSLINE_SSLETTER_ID, $iN_SSLINE_SOURCE_ID, $sSLINE_SSSUBJECT_ID, $sSLINE_SSSUBSUBJECT_ID, $sSLINE_MEMO)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_SSLINE($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_SSLINE_SSLETTER_ID, $iN_SSLINE_SOURCE_ID, $sSLINE_SSSUBJECT_ID, $sSLINE_SSSUBSUBJECT_ID, $sSLINE_MEMO));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
