<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKDELETECONXATTR ServiceType
 * @subpackage Services
 */
class SSKDELETECONXATTR extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_DELETE_CONXATTR
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $check_Only
     * @param \StructType\TConxAttr $conXAttr
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_DELETE_CONXATTR($check_Only, \StructType\TConxAttr $conXAttr)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_DELETE_CONXATTR($check_Only, $conXAttr));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
