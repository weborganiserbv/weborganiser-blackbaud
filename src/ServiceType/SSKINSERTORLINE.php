<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKINSERTORLINE ServiceType
 * @subpackage Services
 */
class SSKINSERTORLINE extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_INSERT_ORLINE
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $oRHEADER_ID
     * @param string $oRLINE_ARTICLE_ID
     * @param string $oRLINE_ARTICLE_DESCR
     * @param string $oRLINE_QTY
     * @param string $oRLINE_PERC_DISCOUNT
     * @param string $oRLINE_ARTICLE_PRICE
     * @param string $oRLINE_VAT_ID
     * @param string $oRLINE_MEMO
     * @param string $x100_1
     * @param string $x100_2
     * @param string $x100_3
     * @param string $x100_4
     * @param string $x100_5
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_INSERT_ORLINE($iN_CHECK_ONLY, $oRHEADER_ID, $oRLINE_ARTICLE_ID, $oRLINE_ARTICLE_DESCR, $oRLINE_QTY, $oRLINE_PERC_DISCOUNT, $oRLINE_ARTICLE_PRICE, $oRLINE_VAT_ID, $oRLINE_MEMO, $x100_1, $x100_2, $x100_3, $x100_4, $x100_5)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_INSERT_ORLINE($iN_CHECK_ONLY, $oRHEADER_ID, $oRLINE_ARTICLE_ID, $oRLINE_ARTICLE_DESCR, $oRLINE_QTY, $oRLINE_PERC_DISCOUNT, $oRLINE_ARTICLE_PRICE, $oRLINE_VAT_ID, $oRLINE_MEMO, $x100_1, $x100_2, $x100_3, $x100_4, $x100_5));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
