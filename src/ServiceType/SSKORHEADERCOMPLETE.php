<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKORHEADERCOMPLETE ServiceType
 * @subpackage Services
 */
class SSKORHEADERCOMPLETE extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_ORHEADER_COMPLETE
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $oRHEADER_ID
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_ORHEADER_COMPLETE($oRHEADER_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_ORHEADER_COMPLETE($oRHEADER_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
