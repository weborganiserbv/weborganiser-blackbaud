<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKGETCOLAREA ServiceType
 * @subpackage Services
 */
class SSKGETCOLAREA extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_GET_COLAREA
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $cOLAREA_ID
     * @return \StructType\TColAreaCR|bool
     */
    public function SSK_GET_COLAREA($cOLAREA_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_GET_COLAREA($cOLAREA_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TColAreaCR
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
