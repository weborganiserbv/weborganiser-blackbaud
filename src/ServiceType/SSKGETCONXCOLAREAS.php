<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKGETCONXCOLAREAS ServiceType
 * @subpackage Services
 */
class SSKGETCONXCOLAREAS extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_GET_CONXCOLAREAS
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $cOLAREA_ID
     * @return \StructType\TConXColAreasCR|bool
     */
    public function SSK_GET_CONXCOLAREAS($cOLAREA_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_GET_CONXCOLAREAS($cOLAREA_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TConXColAreasCR
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
