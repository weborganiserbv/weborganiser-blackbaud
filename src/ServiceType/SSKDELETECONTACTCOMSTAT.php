<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKDELETECONTACTCOMSTAT ServiceType
 * @subpackage Services
 */
class SSKDELETECONTACTCOMSTAT extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_DELETE_CONTACT_COMSTAT
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $iN_CONTACT_ID
     * @param string $iN_COMSTAT_ID
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_DELETE_CONTACT_COMSTAT($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_COMSTAT_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_DELETE_CONTACT_COMSTAT($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_COMSTAT_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
