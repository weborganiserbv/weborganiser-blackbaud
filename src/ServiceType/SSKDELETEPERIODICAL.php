<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for SSKDELETEPERIODICAL ServiceType
 * @subpackage Services
 */
class SSKDELETEPERIODICAL extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SSK_DELETE_PERIODICAL
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $iN_CHECK_ONLY
     * @param string $iN_CONTACT_ID
     * @param string $iN_PERIODICAL_ID
     * @return \StructType\TCALLRESULT|bool
     */
    public function SSK_DELETE_PERIODICAL($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_PERIODICAL_ID)
    {
        try {
            $this->setResult(self::getSoapClient()->SSK_DELETE_PERIODICAL($iN_CHECK_ONLY, $iN_CONTACT_ID, $iN_PERIODICAL_ID));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\TCALLRESULT
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
